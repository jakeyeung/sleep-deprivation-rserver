# Jake Yeung
# Date of Creation: 2017-08-17
# File: ~/projects/sleep_deprivation/scripts/penalized_sleep_model/adjust_logL_of_sleep_models.R
# logL is currently the negative of the optimization output. However with penalization, we are underestimating the log likelihood. Fix this knowing lambda

rm(list=ls())
start <- Sys.time()

# Inits -------------------------------------------------------------------

library(dplyr)
library(ggplot2)
library(reshape2)
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/GetClockGenes.R")

# speed up with mclapply and environments
source("/home/yeung/projects/sleep_deprivation/scripts/functions/EnvironmentsFunctions.R")
library(parallel)

indir <- "/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit"
# # jprefix <- "fits_Rcpp_bugfix.model."  # probably won't run because we add .model. into the loading of each Robj
# jprefix <- "fits_Rcpp_bugfix."  # should work
jprefix <- "fits_Rcpp_bugfix.maxiter.600."
jmodels <- c("sleep", "mix", "mixedaf")
jlambda <- 1000

robjsdir <- "/home/yeung/projects/sleep_deprivation/Robjs/combat"
outname <- paste0(jprefix, "LogLBicCorrected.model.sleep_mix_mixedaf.lambda.", jlambda, ".dolinear.FALSE.minhl.0.33.Robj")
# robjsf <- file.path(robjsdir, "fits_Rcpp_bugfix.LogLBicCorrected.model.sleep_mix_mixedaf.lambda.1000.dolinear.FALSE.minhl.0.33.Robj")
robjsf <- file.path(robjsdir, outname)

if (file.exists(robjsf)){
  stop(paste("Output file exists, not overwriting...", robjsf))
}

# Load data ---------------------------------------------------------------

do.linear <- FALSE

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}

dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)

# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=4/3600, max.time=78, filt.time=filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)


tstep <- 4/3600  # 4seconds in units of hour
low.pass.filt.time <- rep(0.1, length = 780) * 1:780

wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)


# Load fit ----------------------------------------------------------------

fits.penal <- LoadTwoFitsAndMerge()  # initialize
# fits.penal <- subset(fits.penal, select = gene)  # keep only gene, so we recalculate the log likelihoods


for (m in jmodels){
  print(m)
  outf <- file.path(indir, paste0(jprefix, "model.", m, ".lambda.", jlambda, ".dolinear.FALSE.minhl.0.33.Robj"))
  # mstr <- ifelse(m == "mixed", "mix", m)
  print(outf)
  load(outf, v=T)
  if ("bic" %in% colnames(fits)){
    fits$bic <- NULL
    fits <- AddBICToDat(fits)
  }
  # replace columns with new fits
  cnames.to.replace <- colnames(fits)[-1]  # ignore first column "gene"
  if (identical(fits$gene, fits.penal$gene)){
    for (cname in cnames.to.replace){
      print(paste("Replacing", cname))
      fits.penal[[cname]] <- fits[[cname]]
    }
  }
  else {
    # need to do it smartly, sometimes we run fits on subsets of genes
    for (cname in cnames.to.replace){
      print("Overwriting fits in subset of fits")
      rows.i <- which(fits.penal[["gene"]] %in% fits[["gene"]])
      fits.penal[[cname]][rows.i] <- fits[[cname]]
    }
  }
}

rm(fits)


# Plot Dbp ----------------------------------------------------------------

jgene <- "Dbp"
PlotFit(jgene, dat.long.shift, fits.penal, wake.collapsed, dat.eeg.plot, filt.time, low.pass.filter.times = low.pass.filt.time)


# Recalculate LogL and BIC ------------------------------------------------

# remove bic.* we recalculate
remove.i <- grepl("^bic.", colnames(fits.penal))
fits.penal <- fits.penal[, !remove.i]


dat.env <- DatLongToEnvironment(dat.long.shift)
fits.env <- DatLongToEnvironment(fits.penal)

  jgenes <- as.character(unique(dat.long.shift$gene))
  fits.penal.fixed <- mclapply(jgenes, function(jgene){
    return(FixLogLBic.3models(fits.env[[jgene]], dat.env[[jgene]], wake.collapsed, low.pass.filt.time))
  }, mc.cores = 24)
  fits.penal.fixed <- bind_rows(fits.penal.fixed)
print(Sys.time() - start)



# Check recalculated logL -------------------------------------------------

fits.penal.fixed <- AddBICToDat(fits.penal.fixed)
fits.penal <- AddBICToDat(fits.penal)

qplot(x = fits.penal$bic.sleep - fits.penal.fixed$bic.sleep) + geom_histogram(bins = 100)
qplot(x = unlist(lapply(fits.penal$fit.sleep, function(x) x[["logL"]])), y = unlist(lapply(fits.penal.fixed$fit.sleep, function(x) x[["logL"]]))) + geom_point()
qplot(x = unlist(lapply(fits.penal$fit.sleep, function(x) x[["logL"]])) - unlist(lapply(fits.penal.fixed$fit.sleep, function(x) x[["logL"]]))) + geom_histogram(bins = 100) + theme_bw() 



# Save no fits to output --------------------------------------------------


fits.penal.fixed$model <- apply(fits.penal.fixed, 1, function(row) SelectBestModel(row, colnames(fits.penal.fixed)))

if (!file.exists(robjsf)){
  save(fits.penal.fixed, file = robjsf) 
} else {
  warning(paste(robjsf, "exists, add .tmp to output"))
  save(paste0(robjsf, ".tmp", fits.penal.fixed))
}

# fits.orig <- LoadTwoFitsAndMerge()


