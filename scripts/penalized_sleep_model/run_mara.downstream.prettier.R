# Jake Yeung
# Date of Creation: 2017-08-28
# File: ~/projects/sleep_deprivation/scripts/penalized_sleep_model/run_mara.downstream.prettier.R
# Prettier plots, close to primetime

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

start <- Sys.time()

merge.models <- TRUE
use.atacseq.peaks <- TRUE  # 15kb from promoter
merge.all.sleep <- TRUE
normalize.sitecounts <- TRUE
center.sitecounts <- FALSE  # center the sitecounts across columns??
weight.cutoff <- 0  # 0.6 was default for a while, try changing it and record the change

marasuffix <- paste0(".maxiterfix.multieeg_lowpass_logscale_merge_models.", merge.models, ".useATACseq.", use.atacseq.peaks)
if (merge.all.sleep){
  marasuffix <- paste0(marasuffix, ".sleepmerge.", merge.all.sleep)
}
if (normalize.sitecounts){
  marasuffix <- paste0(marasuffix, ".normalizeNsd.", normalize.sitecounts)
}
if (center.sitecounts){
  marasuffix <- paste0(marasuffix, ".centerN.", center.sitecounts)
}
if (weight.cutoff != 0.6){
  marasuffix <- paste0(marasuffix, ".weightCutoff.", weight.cutoff)
}

print("Mara suffix:")
print(marasuffix)

top.n.motifs <- 50
setwd("/home/yeung/projects/sleep_deprivation")

library(methods)
library(ggplot2)
library(reshape2)
library(dplyr)
# library(preprocessCore)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/GetTFs.R")

# Load exprs data -------------------------------------------------------------------

# load data
inf.dat <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"
load(inf.dat, v=T); dat.orig <- subset(dat.long.cleaned, time <= 78)

fits <- MergeLowPassFits()

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

# Load EEG ----------------------------------------------------------------

## WAKE
min.time <- 0
max.time <- 78

# load EEG data (wake.collapsed, dat.eeg.plot)
max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# filt.time <- unique(dat.orig$time)
# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=tstep, max.time=max.time, filt.time=filt.time)
# dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=max.time, downsamp = downsamp)

tstep <- 4/3600
wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)



# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
weight.cutoff <- 0.6
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels

genes.lst[["genes_all"]] <- genes.all

if (merge.models){
  if (!merge.all.sleep){
    # copied from run_mara_on_models..combine_models script
    genes.lst <- MergeLists(genes.lst, "ampfree.step", "mixedaf")
    genes.lst <- MergeLists(genes.lst, "circadian", "mix")
    genes.lst <- RemoveLists(genes.lst, "ampfree.step")
    genes.lst <- RemoveLists(genes.lst, "mixedaf")
    genes.lst <- RemoveLists(genes.lst, "circadian")
    genes.lst <- RemoveLists(genes.lst, "mix")
  } else {
    # copied from run_mara.R script
    # add sleep first 
    # genes.lst <- MergeLists(genes.lst, "mix", "mixedaf")
    # genes.lst <- MergeLists(genes.lst, "mix_mixedaf", "sleep")
    # genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep", "ampfree.step")
    # genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep_ampfree.step", "circadian")
    # genes.lst <- RemoveLists(genes.lst, "mix")
    # genes.lst <- RemoveLists(genes.lst, "mixedaf")
    # genes.lst <- RemoveLists(genes.lst, "sleep")
    # genes.lst <- RemoveLists(genes.lst, "ampfree.step")
    # genes.lst <- RemoveLists(genes.lst, "circadian")
    
    # add sleep last 2018-10-04
    genes.lst <- MergeLists(genes.lst, "mix", "mixedaf")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf", "ampfree.step")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf_ampfree.step", "circadian")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf_ampfree.step_circadian", "sleep")
    genes.lst <- RemoveLists(genes.lst, "mix")
    genes.lst <- RemoveLists(genes.lst, "mixedaf")
    genes.lst <- RemoveLists(genes.lst, "sleep")
    genes.lst <- RemoveLists(genes.lst, "ampfree.step")
    genes.lst <- RemoveLists(genes.lst, "circadian")
    
  }
} 

# # Downstream --------------------------------------------------------------

# match TFs 
tfs <- GetTFs(split.commas = FALSE, get.mat.only = TRUE)

do.mean <- FALSE
do.center <- TRUE
# gene.lab <- "genes_sleep"
gene.labs <- names(genes.lst)[which(names(genes.lst) != "flat")]

outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", marasuffix)
# dir.create(outmain)

gene.lab <- "mix_mixedaf_sleep_ampfree.step_circadian"

print(gene.lab)
outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))

suffix <- paste0("mean.", do.mean,  ".centered.", do.center,  marasuffix, "/sleep_deprivation_gene_exprs_all")
act.dir <- file.path(outdir, suffix)

act.zscore.lst <- LoadMaraOutput(act.dir)
act.long <- act.zscore.lst$act.long
zscores <- act.zscore.lst$zscores


# Propagate errors across replicates --------------------------------------

act.means <- act.long %>%
  group_by(gene, time) %>%
  summarise(exprs = mean(exprs), sem = sqrt(sum(sem ^ 2)))

# PlotMara(subset(act.means, gene == "SRF.p3"))
# PlotMara.withEEG(subset(act.means, gene == "SRF.p3"), dat.eeg.plot, jtitle = "SRF.p3")

# # plot SRF.p3 at time 0 for sanity check
# jsub <- subset(act.long, gene == "SRF.p3"& time == 0)
# jsub.mean <- subset(act.means, gene == "SRF.p3"& time == 0)
# ggplot(jsub, aes(x = time, y = exprs)) + geom_point() + geom_errorbar(aes(ymin = exprs - sem, ymax = exprs + sem))
# ggplot(jsub.mean, aes(x = time, y = exprs)) + geom_point() + geom_errorbar(aes(ymin = exprs - sem, ymax = exprs + sem))

# plot top genes
top.genes <- as.character(zscores$motif[1:top.n.motifs])

plotdir <- paste0("plots/mara_outputs", marasuffix)
dir.create(plotdir, showWarnings = FALSE)
pdf(file.path(plotdir, paste0(gene.lab, "_motif_activities_pretty.pdf")))
for (jgene in top.genes){
  PlotMara.withEEG(subset(act.means, gene == jgene), dat.eeg.plot, jtitle = jgene)
  jgene.tf <- GetGenesFromMotifs(jgene, tfs)
  # print(m)
  for (g in jgene.tf){
    # PlotFit(c(g), dat.orig, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
    PlotFit(c(g), dat.orig, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
  }
}
print(Sys.time() - start)
dev.off()

