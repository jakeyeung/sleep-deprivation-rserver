# Jake Yeung
# Date of Creation: 2017-08-23
# File: ~/projects/sleep_deprivation/scripts/penalized_sleep_model/run_mara.downstream.troubleshoot.R
# Investigate target genes associated with motifs

rm(list=ls())

start <- Sys.time()

merge.models <- TRUE
use.atacseq.peaks <- TRUE

marasuffix <- paste0(".maxiterfix.multieeg_lowpass_logscale_merge_models.", merge.models, ".useATACseq.", use.atacseq.peaks)

print("Mara suffix:")
print(marasuffix)

top.n <- 50
setwd("/home/yeung/projects/sleep_deprivation")

library(methods)
library(ggplot2)
library(reshape2)
library(dplyr)
# library(preprocessCore)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/GetTFs.R")

# Load exprs data -------------------------------------------------------------------

# load data
inf.dat <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"
load(inf.dat, v=T); dat.long.shift <- subset(dat.long.cleaned, time <= 78)

fits <- MergeLowPassFits()

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

# Load EEG ----------------------------------------------------------------

## WAKE
min.time <- 0
max.time <- 78

# load EEG data (wake.collapsed, dat.eeg.plot)
max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# filt.time <- unique(dat.orig$time)
# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=tstep, max.time=max.time, filt.time=filt.time)
# dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=max.time, downsamp = downsamp)

tstep <- 4/3600
wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)



# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
weight.cutoff <- 0.6
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels

genes.lst[["genes_all"]] <- genes.all

if (merge.models){
  # copied from run_mara_on_models..combine_models script
  genes.lst <- MergeLists(genes.lst, "ampfree.step", "mixedaf")
  genes.lst <- MergeLists(genes.lst, "circadian", "mix")
  genes.lst <- RemoveLists(genes.lst, "ampfree.step")
  genes.lst <- RemoveLists(genes.lst, "mixedaf")
  genes.lst <- RemoveLists(genes.lst, "circadian")
  genes.lst <- RemoveLists(genes.lst, "mix")
} 


# Load sitecounts ---------------------------------------------------------



# # Downstream --------------------------------------------------------------

# match TFs 
tfs <- GetTFs(split.commas = FALSE, get.mat.only = TRUE)

do.mean <- FALSE
do.center <- TRUE
gene.lab <- "genes_sleep"
gene.labs <- names(genes.lst)[which(names(genes.lst) != "flat")]


# Find target genes -------------------------------------------------------

# look at sleep genes
gene.lab <- "sleep"
gene.lab <- "circadian_mix"
gene.lab <- "ampfree.step_mixedaf"

# Load N matrix
N.dir <- paste0("/home/yeung/data/sleep_deprivation/sitecount_matrices_for_mara", marasuffix)
N.name <- paste0("sitecounts_swiss_regulon_promoters_only.", gene.lab, marasuffix, ".mat")
N.out <- file.path(N.dir, N.name)
N <- read.table(N.out, header = TRUE)
N.long <- melt(N, id.vars = "Gene.ID", variable.name = "motif", value.name = "sitecount")
N.long <- dplyr::rename(N.long, "gene" = Gene.ID)

# Load Mara output
outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", marasuffix)
outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))
suffix <- paste0("mean.", do.mean,  ".centered.", do.center,  marasuffix, "/sleep_deprivation_gene_exprs_all")
act.dir <- file.path(outdir, suffix)
act.f <- file.path(act.dir, "Activities")
zscores.f <- file.path(act.dir, "Zscores")
act.mat <- read.table(act.f, header = FALSE, sep = "\t")
zscores <- read.table(zscores.f, header = FALSE, sep = "\t", col.names = c("motif", "zscore"))
print(head(zscores))
zscores <- zscores[order(zscores$zscore, decreasing = TRUE), ]

jmotif <- as.character(zscores$motif[[1]])  # take top motif  e.g., SRF

# jmotif <- "HSF1.2.p2"

# jmotif <- "NR3C1.p2"
# jmotif <- "EGR1..3.p2"
# jmotif <- "SRF.p3"
# jmotif <- "KLF4.p3"
# jmotif <- "ZNF143.p2"
# jmotif <- "SPI1.p2"

# Find target genes -------------------------------------------------------

N.sub <- subset(N.long, gene %in% genes.lst[[gene.lab]] & motif == jmotif) %>%
  arrange(desc(sitecount)) %>%
  mutate(sitecount = as.numeric(scale(sitecount, center = FALSE, scale = FALSE)))
print(jmotif)
print(head(N.sub))


# Plot expression of top 10 target genes ----------------------------------

jtop <- 20
target.genes <- as.character(N.sub$gene[1:min(jtop, nrow(N.sub))])

dat.sub <- subset(dat.long.shift, gene %in% target.genes)

dat.norm <- dat.sub %>%
  group_by(gene, time) %>%
  summarise(exprs = mean(exprs)) %>%
  mutate(exprs.norm = as.numeric(scale(exprs, center = TRUE, scale = FALSE)))
# assign sitecount to gene
sc.hash <- hash(as.character(N.sub$gene), as.numeric(N.sub$sitecount))
dat.norm$sitecount <- sapply(as.character(dat.norm$gene), function(x) sc.hash[[x]])

ggplot(dat.norm, aes(x = time, y = exprs.norm, group = gene, alpha = sitecount)) + 
  geom_line() + ggtitle(paste(gene.lab, jmotif, jtop, sep = "\n"))

jgene <- "Aanat"
jgene <- "Snat"
PlotFit(c(jgene), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
