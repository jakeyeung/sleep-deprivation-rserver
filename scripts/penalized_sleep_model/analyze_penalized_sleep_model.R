# Jake Yeung
# Date of Creation: 2017-08-15
# File: ~/projects/sleep_deprivation/scripts/penalized_sleep_model/analyze_penalized_sleep_model.R
# Run new models for Process S, Mix and Mixedaf. Assess whether we did a good job compared to before


# Inits -------------------------------------------------------------------

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(ggplot2)
library(reshape2)
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/GetClockGenes.R")


do.linear <- FALSE

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
low.pass.filt.time <- c(0, low.pass.filt.time)
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]


# HALF LIFE LIMITS
min.hl <- 1/3
max.hl <- 24
min.hl.mrna <- 1/3
max.hl.mrna <- 24

# inits -------------------------------------------------------------------

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}

dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)

# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=4/3600, max.time=78, filt.time=filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)


tstep <- 4/3600  # 4seconds in units of hour
low.pass.filt.time <- rep(0.1, length = 780) * 1:780

wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)


# Load new fits -----------------------------------------------------------

# load original, then replace
fits.penal <- LoadTwoFitsAndMerge()
jlambda <- 1000

indir <- "/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit"

# jprefix <- "fits.model."
# jmodels <- c("sleep", "mixed", "mixedaf")

jprefix <- "fits_Rcpp_bugfix.model."
jmodels <- c("sleep", "mix", "mixedaf")

for (m in jmodels){
  print(m)
  outf <- file.path(indir, paste0(jprefix, m, ".lambda.", jlambda, ".dolinear.FALSE.minhl.0.33.Robj"))
  # mstr <- ifelse(m == "mixed", "mix", m)
  load(outf, v=T)
  if ("bic" %in% colnames(fits)){
    fits$bic <- NULL
    fits <- AddBICToDat(fits)
  }
  # replace columns with new fits
  if (identical(fits$gene, fits.penal$gene)){
    cnames.to.replace <- colnames(fits)[-1]  # ignore first column "gene"
    for (cname in cnames.to.replace){
      print(paste("Replacing", cname))
      fits.penal[[cname]] <- fits[[cname]]
    }
  }
}
fits.penal$model <- apply(fits.penal, 1, function(row) SelectBestModel(row, colnames(fits.penal)))


# Load old fits -----------------------------------------------------------

fits.orig <- LoadTwoFitsAndMerge()
fits.orig$model <- apply(fits.orig, 1, function(row) SelectBestModel(row, colnames(fits.orig)))

rm(fits)


# Look at sleep models ----------------------------------------------------


fits.sleep <- subset(fits.orig, model == "sleep")


# Summarize sleep models --------------------------------------------------


params.sleep <- BindParameters(fits.orig, modelname = "sleep", fit.cname = "fit.sleep")

params.sleep$min.tau <- as.numeric(apply(params.sleep, 1, function(row) min(row[[3]], row[[5]])))
params.sleep$UL.delta <- params.sleep$U - params.sleep$L

UL.delta.i <- which(colnames(params.sleep) == "UL.delta")
gene.delta.i <- which(colnames(params.sleep) == "gene")
params.sleep$genelab <- apply(params.sleep, 1, function(row, UL.delta.i, gene.delta.i){
  UL.delt <- as.numeric(row[[UL.delta.i]])
  ifelse(abs(UL.delt) > 1.5, yes = row[[gene.delta.i]], no = NA)
}, UL.delta.i, gene.delta.i)

ggplot(params.sleep, aes(x = UL.delta, y = log(2) * min.tau, label = genelab)) + 
  geom_point(size = 0.1) + geom_text_repel(size = 5) + 
  theme_bw(24) + 
  xlab("Max - Min [log2 abundance]") + 
  ylab("Minimum Half-Life [h]") + 
  xlim(c(-5, 5))


# Genome wide -------------------------------------------------------------

# predict at 0 and 24
jpreds <- subset(fits.orig, model == "sleep") %>%
  group_by(gene) %>%
  do(PredictSleep(., filt.time, wake.collapsed, low.pass.filt.time))
jpreds.sub <- subset(jpreds, time %in% c(0, 24)) %>%
  group_by(gene) %>%
  summarise(exprs.diff = diff(exprs)) %>%
  arrange(desc(abs(exprs.diff)))
plot(density(jpreds.sub$exprs.diff))

jpreds.penal <- subset(fits.penal, model == "sleep") %>%
  group_by(gene) %>%
  do(PredictSleep(., filt.time, wake.collapsed, low.pass.filt.time))
jpreds.penal.sub <- subset(jpreds.penal, time %in% c(0, 24)) %>%
  group_by(gene) %>%
  summarise(exprs.diff = diff(exprs)) %>%
  arrange(desc(abs(exprs.diff)))
plot(density(jpreds.penal.sub$exprs.diff))

jpreds.penal.sub$ds <- "Penalized"; jpreds.sub$ds <- "Orig"
jpreds.merge.long <- bind_rows(jpreds.penal.sub, jpreds.sub)

jpreds.penal.sub$ds <- NULL; jpreds.sub$ds <- NULL

jpreds.penal.sub$exprs.diff.penal <- jpreds.penal.sub$exprs.diff
jpreds.penal.sub$exprs.diff <- NULL
jpreds.merge <- dplyr::inner_join(jpreds.penal.sub, jpreds.sub)

jlim <- 0.5
ggplot(jpreds.merge, aes(x = exprs.diff, y = exprs.diff.penal)) + geom_point(alpha=0.1) + xlim(c(-jlim, jlim)) + ylim(c(-jlim, jlim))

# Plot fit.sleeps side by side -------------------------------------------

jgene <- "Arc"
jgene <- "Fos"
jgene <- "Homer1"
jgene <- "Baiap2l2"

pred.orig <- PredictSleep(subset(fits.orig, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.orig$model <- "Sleep.Orig"
pred.penal <- PredictSleep(subset(fits.penal, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.penal$model <- "Sleep.Penal"


pred.merged <- bind_rows(pred.orig, pred.penal)
dat.sub <- subset(dat.long.shift, gene == jgene)
fits.sub <- subset(fits.orig, gene == jgene)

PlotBestFit(dat.sub, fits.sub, filt.time, jgene, dat.pred = pred.merged)
