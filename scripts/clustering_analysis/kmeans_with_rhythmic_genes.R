# Jake Yeung
# Date of Creation: 2017-09-08
# File: ~/projects/sleep_deprivation/scripts/clustering_analysis/kmeans_with_rhythmic_genes.R

rm(list=ls())

# inits -------------------------------------------------------------------

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(ggplot2)
library(reshape2)
library(hash)
# library(cowplot)
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/GetClockGenes.R")

do.linear <- FALSE

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
low.pass.filt.time <- c(0, low.pass.filt.time)
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# inits -------------------------------------------------------------------

plotdir <- "/home/yeung/projects/sleep_deprivation/plots/clustering_sleuth"
# plotf <- file.path(plotdir, "clustering_of_DE_genes_sleuth.pdf")
n.centers <- 10

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}

dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)

# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# Fit rhythmic ------------------------------------------------------------

dat.rhyth.78 <- dat.long.shift %>%
  group_by(gene) %>%
  do(FitRhythmic(.))
dat.rhyth.78 <- dat.rhyth.78 %>%
  arrange(pval)
outf <- "Robjs/dat.rhyth.78.rhythmic_0_to_78.Robj"
if (!file.exists(outf)){
  save(dat.rhyth.78, file = outf)
}

jgene <- "Net1"
jgene <- "Arntl"
m.gene2 <- PlotPoints(subset(dat.long.shift, gene == jgene & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "New Data"))
print(m.gene2)

