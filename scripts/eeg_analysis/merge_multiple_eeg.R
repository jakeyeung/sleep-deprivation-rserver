# 2016-09-04
# Jake Yeung
# Merge EEG from multiple mice. 12 mice have 72 hours. 6 mice have 78 hours. 
# Ties are broken arbitrarily 

rm(list=ls())

setwd("~/projects/sleep_deprivation")
library(dplyr)
library(ggplot2)
library(reshape2)
library(PhaseHSV)
library(stringr)
library(hash)
library(reshape2)
library(gridExtra)


source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/StringFunctions.R")
source("scripts/functions/PhaseFunctions.R")
source("scripts/functions/GetClockGenes.R")


# Functions ---------------------------------------------------------------




# Load --------------------------------------------------------------------

# from sleep_wake_pattern.12_mice_and_6_mice_78_hrs.R
# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/eeg_data_12_mice/dat.eeg.12mice.Robj"
# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/eeg_data_merged_72_and_78_hr_mice/dat.eeg.72h.78h.merged.mice.nremiswake.FALSE.Robj"
# load(inf, v=T)
# dat.eeg.all.tmp <- ShiftEeg(dat.eeg.all)
# dat.eeg.all.tmp$rem.wake <- FALSE

rem.wake <- TRUE
inf <- paste0("/home/yeung/projects/sleep_deprivation/Robjs/eeg_data_merged_72_and_78_hr_mice/dat.eeg.72h.78h.merged.mice.remiswake.", rem.wake, ".Robj")
load(inf, v=T)
dat.eeg.all <- ShiftEeg(dat.eeg.all)
dat.eeg.all$rem.wake <- rem.wake
# dat.eeg.all <- rbind(dat.eeg.all, dat.eeg.all.tmp)

# dat.eeg.all <- subset(dat.eeg.all)


# # Plot each  --------------------------------------------------------------
# 
# 
# downsamp <- 0.01
# dat.eeg.all.plot <- dat.eeg.all %>%
#   group_by(mouse, rem.wake) %>%
#   do(DownSample(., downsamp = 0.05))

# dat.sub <- subset(dat.eeg.all.plot, mouse == "BL601")
dat.sub <- subset(dat.eeg.all.plot)
PlotEeg(dat.sub) + facet_wrap(~mouse, ncol = 2) + theme(aspect.ratio = 0.1) +
  # AddDarkPhases(dark.start = c(24), dark.end = c(30), alpha = 0.2) +
  AddDarkPhases(dark.start = c(12, 36, 60), alpha = 0.1)


# Save to file  -----------------------------------------------------------

jmethod <- "mode"
wake.df <- dat.eeg.all %>%
  group_by(time.shift) %>%
  do(TakeAvgWakeStatus(., method = jmethod))

# redo smoothing
wake.df <- SmoothWakefulness5Minutes(wake.df)

# save mean method
save(wake.df, file = paste0("Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.", jmethod, ".Robj"))



# Collapse into one -------------------------------------------------------

# dat.eeg.all.plot <- dat.eeg.all %>%
#   group_by(mouse, rem.wake) %>%
#   do(DownSample(., downsamp = 0.05))

# plotdir <- "plots/eeg_data_many_mice"
# dir.create(plotdir, showWarnings = TRUE)
# # pdf(file.path(plotdir, "avg_many_mice_different_methods.pdf"))
# 
# PlotEeg(dat.eeg.all.plot) + facet_wrap(~mouse, ncol = 2) + theme(aspect.ratio = 0.1) +
#   AddDarkPhases(dark.start = c(24), dark.end = c(30), alpha = 0.2) +
#   AddDarkPhases(dark.start = c(12, 36, 60), alpha = 0.1)
# 
# jmethods <- c("mode", "mean")
# for (jmethod in jmethods){
#   wake.df <- dat.eeg.all %>%
#     group_by(time.shift) %>%
#     do(TakeAvgWakeStatus(., method = jmethod))
#   
#   # redo smoothing
#   wake.df <- SmoothWakefulness5Minutes(wake.df)
#   
#   # Plot collapsed ----------------------------------------------------------
#   
#   dat.eeg.plot <- DownSample(wake.df, downsamp = 0.05)
#   m <- PlotEeg(dat.eeg.plot)  + theme(aspect.ratio = 0.1) + ggtitle(paste0("Avg method:", jmethod)) + 
#     AddDarkPhases(dark.start = c(24), dark.end = c(30), alpha = 0.2) +
#     AddDarkPhases(dark.start = c(12, 36, 60), alpha = 0.1)  
#   print(m)
# }
# dev.off()



