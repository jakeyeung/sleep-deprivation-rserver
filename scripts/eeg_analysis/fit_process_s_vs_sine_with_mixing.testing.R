# 2016-08-21
# Jake Yeung


rm(list=ls())

start <- Sys.time()

setwd("/home/yeung/projects/sleep_deprivation")

top.n <- 500
downsamp <- 0.005  # downsample fraction 

library(dplyr)
library(hash)
library(reshape2)
source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")


# Functions ---------------------------------------------------------------



# LOAD  -------------------------------------------------------------------


# fit on replicates
## GENE EXPRS
zt0tozt24 <- FALSE
load("Robjs/dat.long.htseq.0to78hrs.reps.Robj", v=T)
dat.long.shift <- dat.long.shift[order(dat.long.shift$time), ]
dat.long.shift$exprs <- dat.long.shift$log2exprs; dat.long.shift$log2exprs <- NULL
dat.long.shift <- subset(dat.long.shift, time != 78)
if (zt0tozt24){
  # change ZT0 to ZT24
  dat.long.shift$time <- sapply(dat.long.shift$time, function(tt){
    if (tt == 0){
      return(24)
    } else {
      return(tt)
    }
  })
}
## WAKE
load("Robjs/dat.eeg.smooth.min_awake_5_min_intervals.Robj", verbose = T)



dat.eeg$time.shift <- dat.eeg$time - 24
jxlim <- c(-24, 78)
dat.eeg <- subset(dat.eeg, time.shift >= jxlim[1] & time.shift <= jxlim[2])
# add 72 as -24
row.to.add <- subset(dat.eeg, time.shift == -24); row.to.add$time.shift <- 72
dat.eeg <- rbind(dat.eeg, row.to.add)

wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= 72)
# add 72 hours as 71.99889
wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
wake.df.dup$time.shift <- 72
wake.df <- rbind(wake.df, wake.df.dup)

tstep <- 4/3600  # 4seconds in units of hour

# filt.time <- unique(dat.long.shift$time)
filt.time <- seq(1, 72)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = filt.time)

# downsample for plotting
dat.eeg.plot <- dat.eeg[seq(1, nrow(dat.eeg), length.out = downsamp * nrow(dat.eeg)), ]


# LOAD FITS
load("Robjs/fits.sleep.circadian.flat.bic.stricterULlimits.Robj", v=T)
if (is.null(fits$bic.sleep)){
  fits$bic.sleep <- sapply(fits$fit.sleep, function(s) s[["bic"]])
  fits$bic.circadian <- sapply(fits$fit.circadian, function(s) s[["bic"]])
  fits$bic.flat <- sapply(fits$fit.flat, function(s) s[["bic"]])
  # fits$bic.delta <- mapply(function(sl, cir) sl - cir, fits$bic.sleep, fits$bic.circadian)
  # fits$bic.mean <- mapply(function(x, y) mean(c(x, y)), fits$bic.sleep, fits$bic.circadian)
}
cnames <- c("sleep", "circadian", "flat")
fits$model <- apply(fits, 1, function(row){
  bic.s <- as.numeric(row[[5]])
  bic.c <- as.numeric(row[[6]])
  bic.f <- as.numeric(row[[7]])
  bic.vec <- c(bic.s, bic.c, bic.f)
  return(cnames[[which.min(bic.vec)]])
})


# Try additive mixing -----------------------------------------------------

g <- "Arntl"
jsub <- subset(dat.long.shift, gene == g)
label.transcript <- FALSE

fits.sub <- subset(fits, gene == g)

params <- unlist(fits.sub$fit.sleep[[1]][1:5])
# add BIC to end of params
params <- c(params, signif(GetBICWeights("bic.sleep", fits.sub), digits = 2))
params.lab <- c("i", "U", "tau.w", "L", "tau.s", "model weight")
params.title <- paste0(paste(params.lab, signif(params, 2), sep = "="), collapse = ", ")
params.lab2 <- c("mu", "amp", "phase", "model weight")
params.sine <- unlist(fits.sub$fit.circadian[[1]][c(4, 1, 2)])
# add BIC to end of params
params.sine <- c(params.sine, signif(GetBICWeights("bic.circadian", fits.sub), digits = 2))
params.title2 <- paste0(paste(params.lab2, signif(params.sine, 2), sep = "="), collapse = ", ")
if (!label.transcript){
  glabel <- g
} else {
  tx <- unique(jsub$transcript)
  if (length(tx) == 1){
    glabel <- paste(g, tx, sep = ": ")
  } else {
    warning("Single transcript expected")
  }
}
jtitle <-  paste0(glabel, "\n", params.title, "\n", params.title2)

S.pred <- S.process.collapsed(params, wake.collapsed, filt.time)
C.pred <- CosSine(params.sine, time = filt.time, ampphase=TRUE)

params.S <- unlist(fits.sub$fit.sleep[[1]][1:5])
params.sine <- unlist(fits.sub$fit.circadian[[1]][c(4, 1, 2)])
weight <- 0.8
params.all <- c(params.S, params.sine, weight)
S.sine <- WeightedSCircadian(params.all, wake.collapsed, filt.time)
dat.pred.mix <- data.frame(time = filt.time, exprs = S.sine, model = "Mix")
dat.pred <- data.frame(time = filt.time, exprs = S.pred, model = "Process S")
dat.pred <- rbind(dat.pred, data.frame(time = filt.time, exprs = C.pred, model = "Circadian"))
dat.pred <- rbind(dat.pred, dat.pred.mix)

m <- ggplot() + geom_point(data = jsub, aes(x = time, y = exprs)) + 
  geom_line(data = dat.pred, aes(x = time, y = exprs, colour = model, linetype = model)) + 
  ggtitle(g) + theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), legend.position = c(0.9, 0.9)) + 
  scale_x_continuous(limits = jxlim, breaks = seq(jxlim[1], jxlim[2], 6)) +  
  # scale_y_continuous(limits = jylim) + 
  geom_vline(xintercept = seq(0, 78, 24), linetype = "dashed", colour = "black") + 
  # geom_rect(aes(xmin=24, xmax=30, ymin=-Inf, ymax=Inf), alpha = 0.1) +  
  AddDarkPhases(dark.start = c(24), dark.end = c(30), alpha = 0.1) + 
  AddDarkPhases(dark.start = c(12, 36, 60), alpha = 0.05) + 
  ylab("log2 mRNA accumulation")
print(m)


# Do some mixing ----------------------------------------------------------


g <- "Npas2"
jsub <- subset(dat.long.shift, gene == g)
label.transcript <- FALSE

fits.sub <- subset(fits, gene == g)

start <- Sys.time()
fit.mix <- FitWeightedSCircadian(jsub, wake.collapsed, condensed = FALSE, pseudo = 0, include.mix.weight = FALSE, include.intercept = FALSE)
print(Sys.time() - start)



params.S <- unlist(fits.sub$fit.sleep[[1]][1:5])
params.sine <- unlist(fits.sub$fit.circadian[[1]][c(4, 1, 2)])
params.mix <- fit.mix[1:9]
  
S.pred <- S.process.collapsed(params, wake.collapsed, filt.time)
C.pred <- CosSine(params.sine, time = filt.time, ampphase=TRUE)
Mix.pred <- WeightedSCircadian(params.mix, wake.collapsed, filt.time, ampphase = FALSE)

dat.pred.mix <- data.frame(time = filt.time, exprs = Mix.pred, model = "Mix")
dat.pred <- data.frame(time = filt.time, exprs = S.pred, model = "Process S")
dat.pred <- rbind(dat.pred, data.frame(time = filt.time, exprs = C.pred, model = "Circadian"))
dat.pred <- rbind(dat.pred, dat.pred.mix)

m <- ggplot() + geom_point(data = jsub, aes(x = time, y = exprs)) + 
  geom_line(data = dat.pred, aes(x = time, y = exprs, colour = model, linetype = model)) + 
  ggtitle(g) + theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), legend.position = c(0.9, 0.9)) + 
  scale_x_continuous(limits = jxlim, breaks = seq(jxlim[1], jxlim[2], 6)) +  
  # scale_y_continuous(limits = jylim) + 
  geom_vline(xintercept = seq(0, 78, 24), linetype = "dashed", colour = "black") + 
  # geom_rect(aes(xmin=24, xmax=30, ymin=-Inf, ymax=Inf), alpha = 0.1) +  
  AddDarkPhases(dark.start = c(24), dark.end = c(30), alpha = 0.1) + 
  AddDarkPhases(dark.start = c(12, 36, 60), alpha = 0.05) + 
  ylab("log2 mRNA accumulation")
print(m)

