# 2016-08-21
# Jake Yeung


rm(list=ls())

use.merged <- TRUE
if (!use.merged){
  max.time <- 72
} else {
  max.time <- 78
}
zt0tozt24 <- FALSE

starts <- Sys.time()

args <- commandArgs(trailingOnly = TRUE)
ncores <- as.numeric(args[[1]])

if (is.na(ncores)) stop("Ncores must be numeric")

setwd("/home/yeung/projects/sleep_deprivation")

# HALF LIFE LIMITS
min.hl <- 1/3
max.hl <- 24

library(dplyr)
library(parallel)

# Function ----------------------------------------------------------------

source("scripts/functions/FitFunctions.R")
source("scripts/functions/EegFunctions.R")

# Load exprs --------------------------------------------------------------

# fit on replicates
load("Robjs/dat.long.htseq.0to78hrs.reps.Robj", v=T)
dat.long.shift <- dat.long.shift[order(dat.long.shift$time), ]
dat.long.shift$exprs <- dat.long.shift$log2exprs; dat.long.shift$log2exprs <- NULL
if (!use.merged){
  dat.long.shift <- subset(dat.long.shift, time != 78)
} else {
  dat.long.shift <- dat.long.shift
}
if (zt0tozt24){
  # change ZT0 to ZT24
  dat.long.shift$time <- sapply(dat.long.shift$time, function(tt){
    if (tt == 0){
      return(24)
    } else {
      return(tt)
    }
  })
}

# Load EEG ----------------------------------------------------------------

if (!use.merged){
  load("Robjs/dat.eeg.smooth.min_awake_5_min_intervals.Robj", verbose = T)
  
  dat.eeg$time.shift <- dat.eeg$time - 24
  jxlim <- c(-24, 78)
  dat.eeg <- subset(dat.eeg, time.shift >= jxlim[1] & time.shift <= jxlim[2])
  # add 72 as -24
  row.to.add <- subset(dat.eeg, time.shift == -24); row.to.add$time.shift <- 72
  dat.eeg <- rbind(dat.eeg, row.to.add)
  
  wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= max.time)
  # add 72 hours as 71.99889
  wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
  wake.df.dup$time.shift <- max.time
  wake.df <- rbind(wake.df, wake.df.dup)  
} else {
  load("Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", verbose = T)
  dat.eeg <- wake.df
  wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= max.time)
  # add 78 hours as last timepoint
  wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
  wake.df.dup$time.shift <- max.time
  wake.df <- rbind(wake.df, wake.df.dup) 
}


tstep <- 4/3600  # 4seconds in units of hour

filt.time <- unique(dat.long.shift$time)
# filt.time <- seq(1, 72)
# filt.time <- seq(1, 78)  # for debugging purposes 
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = filt.time)


# Fit data ----------------------------------------------------------------

start <- Sys.time()
genes <- as.character(unique(dat.long.shift$gene))
# genes <- sample(as.character(unique(dat.long.shift$gene)), 2)
# g <- "1110054M08Rik"  # problematic gene

fits <- mclapply(genes, function(g){
  dat <- subset(dat.long.shift, gene == g)
  fit <- dat %>%
    group_by(gene) %>%
    do(fit.sleep = FitProcessS(., wake.collapsed, exprs.cname = "exprs", time.cname = "time", condensed=TRUE, pseudo = 0, min.hl = min.hl, max.hl = max.hl),
       fit.circadian = FitRhythmic(., T.period = 24, use.weights=FALSE, get.bic=TRUE, condensed=TRUE),
       fit.flat = FitFlat(., use.weights = FALSE, get.bic = TRUE, condensed = TRUE),
       fit.mix = FitWeightedSCircadian(., wake.collapsed, condensed = FALSE, pseudo = 0, min.hl = min.hl, max.hl = max.hl, include.mix.weight = FALSE, include.intercept = FALSE)) %>%
    mutate(bic.sleep = fit.sleep$bic, 
           bic.circadian = fit.circadian$bic, 
           bic.flat = fit.flat$bic,
           bic.mix = fit.mix$bic)
  return(fit)
}, mc.cores = ncores)
fits <- do.call(rbind, fits)
print(Sys.time() - start)

print(head(fits))
 
# Write to output ---------------------------------------------------------

outf <- paste0("Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.noIntnoWeight.pseudo0.usemerged.", use.merged, ".zt0tozt24.", zt0tozt24, ".Robj")
save(fits, file=outf)

print(Sys.time() - start)

