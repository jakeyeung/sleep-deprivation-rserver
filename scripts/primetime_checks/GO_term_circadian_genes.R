# Jake Yeung
# Date of Creation: 2018-08-14
# File: ~/projects/sleep_deprivation/scripts/primetime_checks/GO_term_circadian_genes.R
# Run GO term analysis on circadian genes


jstart <- Sys.time()

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(reshape2)
library(wordcloud)
library(hash)
library(ggplot2)
library(CyclicGO)

source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/SummarizeModels.R")
source("scripts/functions/GetTFs.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/SleuthFunctions.R")
source("scripts/functions/ClusteringFunctions.R")
source("scripts/functions/LoadData.R")
source("scripts/functions/TimeShiftFunctions.R")
source("scripts/functions/GetClockGenes.R")


# Load data ---------------------------------------------------------------

LoadPrimetimeObjs()

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic))) %>%
  filter(weight == max(weight))
fits.w.weight <- left_join(fits, subset(fits.bic, select = c(gene, model, weight)))

jmodel <- "circadian"
params.circ <- SummarizeParameters(fits, jmodel)




# Run GO ------------------------------------------------------------------



library(CyclicGO)
library(dplyr)
library(ggplot2)
library(parallel)
library(DBI)
library(topGO)
library(org.Mm.eg.db)
library(GenomicRanges)

params.circ$phase.avg <- params.circ$phase

fits.sub <- params.circ
genes.bg <- unique(as.character(fits.sub$gene))

print(head(fits.sub))
print(genes.bg)

enrichment.multi <- RunEnrichment(fits.sub, genes.bg, tstarts = seq(0, 23), tstep = 6, GOterms = FALSE, ncores = 1)
save(enrichment.multi, file = "Robjs/GO_terms/CyclicGO_circadian_model.Rdata")

print(Sys.time() - jstart)
