# Jake Yeung
# Date of Creation: 2018-08-03
# File: ~/projects/sleep_deprivation/scripts/primetime_checks/pca_removing_flat_genes.R
# PCA 2 is bugging me. Try removing flat genes and re-run.

rm(list=ls())

library(ggplot2)
library(reshape2)
library(dplyr)

source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/MergingAssigningSamples.R")
source("scripts/functions/LoadData.R")

weight.cutoff <- 0
no.flat.genes <- FALSE


# Functions ---------------------------------------------------------------

PcaToLong <- function(x, pc1 = 1, pc2 = 2){
  pca.long <- data.frame(pc1 = x[, pc1],
                         pc2 = x[, pc2],
                         lab = rownames(x),
                         color = cols, 
                         stringsAsFactors = FALSE)
  pca.long$sd.sd <- sapply(pca.long$lab, function(x) strsplit(x, "_")[[1]][[1]])
  pca.long$lab <- sapply(pca.long$lab, function(x) paste(strsplit(x, "_")[[1]][c(1, 2)], collapse = "_"))
  pca.long$lab2 <- sapply(pca.long$lab, function(x) paste0("T", strsplit(x, "_")[[1]][[2]]))
  pca.long$time <- sapply(pca.long$lab, function(x) as.numeric(strsplit(x, "_")[[1]][[2]]))
  pca.long$ZT <- sapply(pca.long$time, TimeToZT)
  return(pca.long)
}

PcaToLong.all <- function(x){
  # For plotting over time as if it is a gene
  # PlotPoints() and PlotPoints.withEEG() compatible
  pca.long <- data.frame(exprs = melt(x)$value, 
                         gene = rep(colnames(x), each = nrow(x)),
                         batch = "pca",
                         lab = rownames(x),
                         color = cols, 
                         stringsAsFactors = FALSE)
  pca.long$trtmnt <- sapply(pca.long$lab, function(x) strsplit(x, "_")[[1]][[1]])
  pca.long$lab <- sapply(pca.long$lab, function(x) paste(strsplit(x, "_")[[1]][c(1, 2)], collapse = "_"))
  pca.long$samp2 <- sapply(pca.long$lab, function(x) paste0("T", strsplit(x, "_")[[1]][[2]]))
  pca.long$time <- sapply(pca.long$lab, function(x) as.numeric(strsplit(x, "_")[[1]][[2]]))
  pca.long$ZT <- sapply(pca.long$time, TimeToZT)
  pca.long$rname <- rownames(x)
  # handle the rname = "NSD_0_1_5,NSD_0_2_5" case
  pca.long$samp <- sapply(pca.long$rname, function(z) strsplit(strsplit(z, ",")[[1]][[1]], split = "_")[[1]][[4]])
  return(pca.long)
}


# Load data ---------------------------------------------------------------

LoadPrimetimeObjs()

inf.dat <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"
load(inf.dat, v=T); dat.orig <- subset(dat.long.cleaned, time <= 78)

inf.fits <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/fits_Rcpp_bugfix.maxiter.2000.LogLBicCorrected.model.sleep_mix_mixedaf_ampfreestep.lambda.1000.dolinear.FALSE.minhl.0.33.Robj"
load(inf.fits); fits <- fits.penal.fixed;rm(fits.penal.fixed)

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)

# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels
genes.lst[["genes_all"]] <- genes.all


# Merge gene lists --------------------------------------------------------

genes.lst <- MergeLists(genes.lst, "mix", "mixedaf")
genes.lst <- MergeLists(genes.lst, "mix_mixedaf", "sleep")
genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep", "ampfree.step")
genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep_ampfree.step", "circadian")
genes.lst <- RemoveLists(genes.lst, "mix")
genes.lst <- RemoveLists(genes.lst, "mixedaf")
genes.lst <- RemoveLists(genes.lst, "sleep")
genes.lst <- RemoveLists(genes.lst, "ampfree.step")
genes.lst <- RemoveLists(genes.lst, "circadian")

jmodel <- "mix_mixedaf_sleep_ampfree.step_circadian"


# Plot PCA ----------------------------------------------------------------

if (no.flat.genes){
  jgenes <- genes.lst[[jmodel]]  # remove flat genes
} else {
  jgenes <- dat.after$gene  # all genes
}


dat.sub <- subset(dat.after, gene %in% jgenes)

dat.M <- dcast(dat.sub, formula = "gene ~ SampID", value.var = "exprs")

rownames(dat.M) <- dat.M$gene; dat.M$gene <- NULL

dat.centered <- t(scale(t(dat.M), center = TRUE, scale = FALSE))

dat.pca <- prcomp(dat.centered, center = FALSE, scale. = FALSE)


# Flip rotation and x by -1 -----------------------------------------------

dat.pca$rotation <- -dat.pca$rotation
dat.pca$x <- -dat.pca$x

screeplot(dat.pca)

ncomps <- 2
U <- dat.pca$x[, seq(ncomps)]
V <- dat.pca$rotation[, seq(ncomps)]

dat.proj.genes <- dat.centered %*% V
dat.proj.samps <- t(dat.centered) %*% U

plot(dat.pca$x[, 1], dat.pca$x[, 2])
text(dat.pca$x[, 1], dat.pca$x[, 2], labels = rownames(dat.pca$x))

qplot(dat.proj.samps[, 1], dat.proj.samps[, 2]) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank())

qplot(dat.proj.genes[, 1], dat.proj.genes[, 2], label = rownames(dat.proj.genes)) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  geom_text()

# plot with colors  -------------------------------------------------------

pc1 <- 2
pc2 <- 3
cols <- GetCols(rownames(dat.proj.samps))

pca.long.proj <- PcaToLong(dat.proj.samps)
pca.long <- PcaToLong(dat.pca$rotation)
pca.long.35 <- PcaToLong(dat.pca$rotation, pc1 = 3, pc2 = 5)
pca.long.34 <- PcaToLong(dat.pca$rotation, pc1 = 3, pc2 = 4)
pca.long.23 <- PcaToLong(dat.pca$rotation, pc1 = 2, pc2 = 3)
pca.long.all <- PcaToLong.all(dat.pca$rotation)

MakePCPlot <- function(pca.long, xlab = "X", ylab = "Y"){
  m.pca <- ggplot(pca.long, aes(x = pc1, y = pc2, colour = color, label = lab2, shape = sd.sd)) + 
    geom_point(size = 3) + 
    geom_text_repel(color = "grey50") + 
    scale_color_identity() + 
    theme_bw() + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1), aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
    xlab(pc1) + ylab(pc2)
}

(m.pca <- MakePCPlot(pca.long, xlab = 1, ylab = 2))

(m.pca.23 <- MakePCPlot(pca.long.23, xlab = 2, ylab = 3))

(m.pca.test <- MakePCPlot(PcaToLong(dat.pca$rotation, pc1 = 3, pc2 = 5)))

(m.pca.test <- MakePCPlot(PcaToLong(dat.pca$rotation, pc1 = 3, pc2 = 4)))

# Plot hits ---------------------------------------------------------------

# jgene <- "Socs7"
jgene <- "Slc23a3"

m.both <- PlotPoints(subset(dat.long.cleaned, gene == jgene & time < 100))
print(m.both)


# Plot PC1 over time ------------------------------------------------------

pca.long.exprs <- pca.long %>%
  mutate(exprs = pc1,
         gene = "pc1",
         samp = lab,
         batch = "all")
pca.long.exprs2 <- pca.long %>%
  mutate(exprs = pc2,
         gene = "pc2",
         samp = lab,
         batch = "all")
pca.long.exprs3 <- pca.long.34 %>%
  mutate(exprs = pc1,
         gene = "pc3",
         samp = lab,
         batch = "all")

m.pca1 <- PlotPoints(subset(pca.long.exprs, time < 100))
m.pca1.eeg <- PlotPoints.withEEG(subset(pca.long.exprs, time < 100), dat.eeg.plot)

print(m.pca1)
print(m.pca1.eeg)

m.pca2 <- PlotPoints(subset(pca.long.exprs2, time < 100))
print(m.pca2)

m.pca3 <- PlotPoints(subset(pca.long.exprs3, time < 100))
print(m.pca2)

ggplot(pca.long, aes(x = time, y = pc1)) + geom_point() 


# Plot many PCs  ----------------------------------------------------------

pca.long.all.clean <- AssignZT0ToZT24Batching(pca.long.all)




# Find circadian PCs ------------------------------------------------------

pcs.filt <- paste0("PC", seq(10))
# PlotPoints(subset(pca.long.all.clean, gene %in% "PC1" & time < 100) %>% ungroup() %>% mutate(exprs = -exprs), dat.eeg.plot) + facet_wrap(~gene)

outdir <- "/home/yeung/projects/sleep_deprivation/plots/PCA_analysis_further"
pdf(paste0(outdir, "/pca_along_time.noflatgenes.", no.flat.genes, ".", Sys.Date(), ".pdf"), useDingbats = FALSE)
print(m.pca)
for (p in pcs.filt){
  PlotPoints.withEEG(subset(pca.long.all.clean, gene == p & time < 100), dat.eeg.plot, colour.pts = TRUE, has.batch = FALSE)
}
for (pc1 in seq(9)){
  pc2 <- pc1 + 1
  print(MakePCPlot(PcaToLong(dat.pca$rotation, pc1 = pc1, pc2 = pc2), xlab = paste0("PC ", pc1), ylab = paste0("PC ", pc2)))
}
dev.off()

# write table for Maxime
write.table(pca.long.all.clean, file = file.path(outdir, paste0("pca.long.all.clean.", Sys.Date(), ".txt")), sep = "\t", quote = FALSE, row.names = FALSE, col.names = TRUE)
