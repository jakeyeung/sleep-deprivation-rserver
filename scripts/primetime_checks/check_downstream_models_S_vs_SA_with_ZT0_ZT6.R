# Jake Yeung
# Date of Creation: 2018-11-15
# File: ~/projects/sleep_deprivation/scripts/primetime_checks/check_downstream_models_S_vs_SA_with_ZT0_ZT6.R
# Use ZT0 and ZT6 over 5 days to assess the "recovery"


rm(list=ls())

jstart <- Sys.time()

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(reshape2)
library(wordcloud)
library(hash)
library(ggplot2)
library(ggrastr)

source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/SummarizeModels.R")
source("scripts/functions/GetTFs.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/SleuthFunctions.R")
source("scripts/functions/ClusteringFunctions.R")
source("scripts/functions/LoadData.R")
source("scripts/functions/TimeShiftFunctions.R")
source("scripts/functions/GetClockGenes.R")
source("scripts/functions/HashFunctions.R")
source("scripts/functions/MdlNames.R")


# Constants ---------------------------------------------------------------

outdir <- "/home/yeung/projects/sleep_deprivation/plots/model_SC_downstream"

# Functions ---------------------------------------------------------------

PlotSAGene <- function(fits, g, wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift){
  jdo.lowpass <- TRUE
  sleep.cnames <- c("init", "U", "tau.w", "L", "tau.s", "tau.mrna")
  A.cnames <- c("intercept", "amp", "phase", "amp.param")
  fits.sub <- subset(fits, gene == g)$fit.mixedaf[[1]]
  params.sleep.gene.frommix <- as.numeric(fits.sub[names(fits.sub) %in% sleep.cnames])
  names(params.sleep.gene.frommix) <- sleep.cnames
  params.A.gene.frommix <- as.numeric(fits.sub[names(fits.sub) %in% A.cnames])
  names(params.A.gene.frommix) <- A.cnames
  
  # set intercept manually
  params.A.gene.frommix["intercept"] <- params.sleep.gene.frommix["init"]
  
  params.A.gene <- GetParams.mixedaf(subset(fits, gene == g), cname = "fit.mixedaf")
  
  S.pred <- S.process.collapsed(params.sleep.gene.frommix, wake.collapsed, filter.times = filt.time, do.lowpass = jdo.lowpass, low.pass.filter.times = low.pass.filt.time)
  A.pred <- Rhythmic.FreeAmp(params.A.gene.frommix, filt.time, AmpFunc, T.period = 24, tswitch = 33, form = "switch", include.intercept = TRUE, amp.phase = TRUE)
  SSA.pred <- WeightedSFreeAmp(params.A.gene, wake.collapsed, filt.time, tswitch = 33, form = "switch", ampphase = TRUE,
                               include.intercept = FALSE, do.lowpass = jdo.lowpass, low.pass.filter.times = low.pass.filt.time)
  dat.actual <- dcast(subset(dat.long.shift, gene == g), formula = "gene ~ time + SampID", value.var = "exprs")
  dat.actual <- subset(dat.actual, select = -gene)
  y.actual <- as.numeric(dat.actual)
  tvec <- as.numeric(sapply(colnames(dat.actual), function(x) strsplit(x, "_")[[1]][[1]]))
  plot(filt.time, A.pred, ylim = range(S.pred, A.pred, y.actual), type = 'l', col = 'blue', main = paste("gene", g), lty = 3, xlab = "ZT [h]", ylab = "log2 mRNA abundance",xaxt="n")
  axis(1, at=seq(0, 78, 6), labels=seq(0, 78, 6))
  lines(filt.time, S.pred, type = 'l', col = 'red', lty = 3)
  lines(filt.time, SSA.pred, type = 'l', col = 'black', lwd = 2.5)
  legend("bottomright", inset=.05, 
         c("C", "S"), col = c("blue", "red"), lty = 3, horiz=TRUE)
  points(tvec, y.actual, pch = 16)
  
}

PolarHistogram <- function(dat){
  # make polar histogram. Need tiny tweaks to make smooth transition which is actually a bar plot
  # assume phase colname
  dat$phase.round <- floor(dat$phase) + 0.5
  dat.counts <- dat %>%
    group_by(phase.round, Upreg) %>%
    summarise(count = length(phase))
  m <- ggplot(dat.counts, aes(y = count, x = phase.round, fill = Upreg)) + facet_wrap(~Upreg) + geom_bar(stat = "identity") + coord_polar(theta = "x") + 
    theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
    xlim(0, 23) + scale_x_continuous(breaks = seq(0, 18, 6)) + coord_polar(theta = "x") + expand_limits(x = 0)
  return(m)
}


# Load --------------------------------------------------------------------


LoadPrimetimeObjs()


# Plot Dbp with S and A part ----------------------------------------------


g <- "Slc23a3"
g <- "Nfil3"
g <- "Dbp"
g <- "Per2"
g <- "Nptx2"

g <- "Hmgcr"

g <- "Isca2"
g <- "Leprot"
g <- "Ubn1"
g <- "Hipk1"

PlotSAGene(fits, g, wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic))) %>%
  filter(weight == max(weight))
fits.w.weight <- left_join(fits, subset(fits.bic, select = c(gene, model, weight)))


# Summarize over 5 days ---------------------------------------------------





# Summarize ---------------------------------------------------------------


jmodel <- "sleep"
params.sleep <- SummarizeParameters(fits, jmodel) %>%
  mutate(w_ge_s = tau.w > tau.s,
         sleep_upreg = as.factor(sign(UL.delta)))

# redo genelab
top.n <- 10
jgenes.i <- c(order(rank(params.sleep$UL.delta))[1:top.n], order(rank(-params.sleep$UL.delta))[1:top.n])
jgenes <- params.sleep$gene[jgenes.i]
params.sleep$genelab <- sapply(params.sleep$gene, function(g) ifelse(g %in% jgenes, g, NA))


jmodel <- "mix"
params.mixed <- SummarizeParameters(fits, jmodel) %>%
  mutate(Upreg = as.factor(sign(UL.delta)))

jmodel <- "mixedaf"
params.mixedaf <- SummarizeParameters(fits, jmodel) %>%
  mutate(Upreg = as.factor(sign(UL.delta)),
         AmpDamp = ifelse(amp.param < 1, TRUE, FALSE)) %>%
  filter(Upreg != 0)

# histogram of phases 

mhistpol <- PolarHistogram(params.mixed) + ggtitle("SC model")
mhistpol.af <- PolarHistogram(subset(params.mixedaf, Upreg != 0)) + ggtitle("SA model")

mhist1 <- ggplot(params.mixed, aes(x = phase, fill = Upreg)) + geom_histogram() + facet_wrap(~Upreg) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlim(0, 24) + scale_x_continuous(breaks = seq(0, 24, 6)) + ggtitle("SC model")
mhist1.af <- ggplot(params.mixedaf, aes(x = phase, fill = Upreg)) + geom_histogram() + facet_wrap(~Upreg) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlim(0, 24) + scale_x_continuous(breaks = seq(0, 24, 6)) + ggtitle("SA model")

mdens1 <- ggplot(params.mixed, aes(x = phase, fill = Upreg), group = Upreg) + geom_density(alpha=0.5) +
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlim(0, 24) + scale_x_continuous(breaks = seq(0, 24, 6))  + ggtitle("SC model")
mdens1.af <- ggplot(params.mixedaf, aes(x = phase, fill = Upreg), group = Upreg) + geom_density(alpha=0.5) +
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlim(0, 24) + scale_x_continuous(breaks = seq(0, 24, 6))  + ggtitle("SA model")



# Show the relative differences in 2D -------------------------------------------

dat.after$day <- TimeToDay(dat.after$time)
dat.after$zt <- TimeToZT(dat.after$time)

dat.06 <- dat.after %>%
  filter(zt %in% c(0, 6)) %>%
  group_by(gene, day, zt) %>%
  summarise(exprs = mean(exprs))
dat.06$day.zt <- paste(dat.06$day, dat.06$zt, sep = "_")

dat.06.fc <- dat.06 %>%
  group_by(gene, day) %>%
  summarise(exprs.fc = exprs[2] - exprs[1])

dat.06.fc.diff <- dat.06.fc %>%
  group_by(gene) %>%
  mutate(exprs.fc.diff = exprs.fc - exprs.fc[1],
         exprs.fc.diff.norm = sign(exprs.fc.diff[2]) * abs(exprs.fc.diff / exprs.fc[1]),
         exprs.fc.norm = exprs.fc / exprs.fc[1])
dat.06.fc.diff <- dplyr::left_join(subset(fits, select = c(gene, model)), dat.06.fc.diff, by = 'gene')

jgene <- "Egr2"
jgene <- "Dbp"
jgene <- "Egr2"
jgene <- "Fosb"
jgene <- "Fosb"
jgene <- "Lrrtm4"
jgene <- "Dbp"
jgene <- "Pla2g4e"
jgene <- "Arntl"
jgene <- "Egr2"
jgene <- "Nr1d1"
jgene <- "Gkn3"
jgene <- "Rasd2"
jgene <- "Hes1"
jgene <- "Egr2"
jgene <- "Hsp90b1"
jgene <- "Cdkn1a"
jgene <- "Plekhg4"


m1 <- ggplot(subset(dat.06, gene == jgene), aes(x = day.zt, y = exprs, group = day)) + geom_point() + geom_line() + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + ggtitle(jgene)
m2 <- ggplot(subset(dat.06.fc, gene == jgene), aes(x = as.character(day), y = exprs.fc, group = day)) + geom_bar(stat = "identity") + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + ggtitle(jgene)
m3 <- ggplot(subset(dat.06.fc.diff, gene == jgene), aes(x = as.character(day), y = exprs.fc.diff, group = day)) + geom_bar(stat = "identity") + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + ggtitle(jgene)
# m4 <- ggplot(subset(dat.06.fc.diff, gene == jgene), aes(x = as.character(day), y = exprs.fc.diff.norm, group = day)) + geom_bar(stat = "identity") + 
#   theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + ggtitle(jgene)
m4 <- ggplot(subset(dat.06.fc.diff, gene == jgene), aes(x = as.character(day), y = exprs.fc.norm, group = day)) + geom_bar(stat = "identity") + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + ggtitle(jgene)
multiplot(m1, m2, m3, m4, cols = 2)

print(PlotBestFit(subset(dat.long.shift, gene == jgene), subset(fits, gene == jgene), 
                  filt.time, jgene, wake.collapsed, low.pass.filt.time, dat.pred = NA, dat.eeg = 
                    dat.eeg.plot))
PlotSAGene(fits, jgene, wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)

# Downstream S, S+C, S+A --------------------------------------------------

ggplot(subset(dat.06.fc, day == 0), aes(x = exprs.fc)) + geom_density()

genes.filt <- subset(dat.06.fc, day == 0 & abs(exprs.fc) > 0.05)$gene
models.filt <- c("sleep", "mix", "mixedaf")
# models.filt <- c("sleep", "mix", "mixedaf", "circadian", "sleep", "flat")  # doesnt work

ggplot(subset(dat.06.fc.diff, model %in% models.filt & gene %in% genes.filt), 
              aes(x = as.character(day), y = exprs.fc.diff.norm)) + geom_boxplot() +
  facet_wrap(~model)
ggplot(subset(dat.06.fc.diff, model %in% models.filt & gene %in% genes.filt), 
              aes(x = as.character(day), y = exprs.fc.diff)) + geom_boxplot() +
  facet_wrap(~model)
ggplot(subset(dat.06.fc.diff, model %in% models.filt & gene %in% genes.filt), 
              aes(x = as.character(day), y = exprs.fc.norm)) + geom_boxplot() +
  facet_wrap(~model)


# Try clustering the fold changes -----------------------------------------

# do it once only 
dat.06.fc <- dplyr::left_join(dat.06.fc, subset(fits, select = c(gene, model)), by = 'gene')

dat.fc.mat <- reshape2::dcast(dat.06.fc %>% filter(model %in% models.filt), 'gene ~ day', value.var = 'exprs.fc')
dat.fc.long <- tidyr::gather(dat.fc.mat, day, foldchange, 2:6)
rownames(dat.fc.mat) <- dat.fc.mat$gene; dat.fc.mat$gene <- NULL
# normalize

dat.fc.mat <- t(scale(t(dat.fc.mat), center = FALSE, scale = FALSE))
# dat.fc.mat <- sweep(dat.fc.mat, MARGIN = 1, STATS = dat.fc.mat$`0`, FUN = "/")

# dat.fc.mat <- t(scale(t(dat.fc.mat), center = FALSE, scale = FALSE))

genes.S <- subset(fits, model == "sleep")$gene
genes.SC <- subset(fits, model == "mix")$gene
genes.SA <- subset(fits, model == "mixedaf")$gene

set.seed(0)

nc <- 6
kout.S <- kmeans(dat.fc.mat[genes.S, ], centers = nc)
kout.SC <- kmeans(dat.fc.mat[genes.SC, ], centers = nc)
kout.SA <- kmeans(dat.fc.mat[genes.SA, ], centers = nc)

kout.lst <- list(kout.S, kout.SC, kout.SA)
names(kout.lst) <- models.filt


# Annotate genes to cluster and model -------------------------------------

gene.clstr.model <- lapply(models.filt, function(m){
  kout <- kout.lst[[m]]
  dat.tmp <- data.frame(gene = names(kout$cluster),
                        clstr = kout$cluster,
                        model = m)
}) %>%
  bind_rows(.)

dat.fc.long.annot <- dat.fc.long %>%
  dplyr::left_join(., gene.clstr.model, by = 'gene')


# Plot output of SA -------------------------------------------------------

ggplot(dat.fc.long.annot, aes(x = day, y = foldchange, group = gene)) + 
  geom_line(alpha = 0.3) + facet_grid(model ~ clstr) + geom_hline(yintercept = 0) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank())

# plot 2D scatterplot
dat.fc.long.base <- subset(dat.fc.long.annot, day == 0) %>%
  dplyr::rename(foldchange0 = foldchange)
dat.fc.long.trt <- subset(dat.fc.long.annot, day > 0) %>%
  dplyr::rename(foldchange1 = foldchange)
dat.fc.long.2d <- dplyr::left_join(dat.fc.long.trt, subset(dat.fc.long.base, select = c(gene, clstr, model, foldchange0)))

# update Day and model names for pretty
mdl.hash <- MdlHash()
dat.fc.long.2d$model <- ConvertMdlNames(dat.fc.long.2d$model, mdl.hash)
dat.fc.long.2d$day <- paste("x=", dat.fc.long.2d$day, sep = "")

m.2d <- ggplot(dat.fc.long.2d, 
       aes(x = foldchange0, y = foldchange1)) + 
  geom_point(alpha = 0.3) + 
  theme_bw() + theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  facet_grid(model ~ day) + 
  geom_abline(slope = 1) + 
  geom_hline(yintercept = 0) + 
  xlab("Log Fold Change Baseline (ZT6 vs ZT0)") + 
  ylab("Log Fold Change at Day x (ZT6 vs ZT0)")

# how many genes are up and down for S+CA model
jmodel <- "mixedaf"
params.mixedaf <- SummarizeParameters(fits, jmodel)

upgenes <- sapply(params.mixedaf$amp.param, function(x) ifelse(x > 1, "up", "down")) 

dat.long.shift$day <- TimeToDay(dat.long.shift$time)

dat.long.avg <- dat.long.shift %>%
  group_by(gene, time, day) %>%
  summarise(exprs.sd = sd(exprs),
            exprs = mean(exprs))

dat.long.avg <- left_join(dat.long.avg, subset(fits, select = c(gene, model)))

# This gives you changes in amplitudes over days 
dat.day.fc <- dat.long.avg %>%
  group_by(gene, day, model) %>%
  summarise(exprs.fc = diff(range(exprs))) %>% 
  group_by(gene, model) %>%
  mutate(exprs.log.fc = log2(exprs.fc / exprs.fc[1]),
         exprs.fc.max = exprs.fc[[1]],
         exprs.fc.diff = exprs.fc - exprs.fc[[1]]) %>%
  filter(day < 3)  # ignore 3rd day its incomplete

# Get differences in mix and sleep
dat.day.fc.base <- subset(dat.long.avg, day %in% c(0)) %>%
  group_by(gene, day, model) %>%
  summarise(exprs.fc = diff(range(exprs)))

jdays <- c(0, 1, 2)
dat.day.fc.days.lst <- lapply(jdays, function(jday){
  dat.day.fc.dayi <- subset(dat.long.avg, day %in% c(0, jday)) %>%
    group_by(gene, model) %>%
    summarise(exprs.fc = diff(range(exprs))) %>%
    mutate(day = jday)
  return(dat.day.fc.dayi)
})
dat.day.fc.days <- bind_rows(dat.day.fc.days.lst)

# Get differences by comparing with mean of baseline
dat.long.avg.baseline <- dat.long.avg %>%
  filter(day == 0) %>%
  group_by(gene, model) %>%
  summarise(exprs.baseline = mean(exprs))
dat.long.avg <- left_join(dat.long.avg, dat.long.avg.baseline)
dat.long.fc.baseline <- dat.long.avg %>%
  group_by(gene, day, model) %>%
  summarise(exprs.amp = max( abs(max(exprs) - exprs.baseline), abs(min(exprs) - exprs.baseline) )) %>%
  group_by(gene, model) %>%
  mutate(exprs.amp.diff = exprs.amp - exprs.amp[1],
         exprs.amp.diff.rel = (exprs.amp - exprs.amp[1]) / exprs.amp[1]) %>%
  filter(day < 3)

# dat.day.fc2 <- left_join(dat.day.fc2, subset(fits.w.weight, select = c(gene, model, weight)))

dat.long.fc.baseline <- left_join(dat.long.fc.baseline, subset(fits.w.weight, select = c(gene, model, weight)))

# add weights
dat.day.fc <- left_join(dat.day.fc, subset(fits.w.weight, select = c(gene, model, weight)))



# plots of amplitudes relative to baseline day 0 mean

GetCnamesHash <- function(){
  cnames.hash <- hash(c("flat", "circadian", "ampfree.step", "mix", "mixedaf", "sleep"), 
                      c("F", "C", "A", "S+C", "S+A", "S"))
}

cnames.hash <- GetCnamesHash()
# rename to final model names
dat.long.fc.baseline$model.new <- sapply(dat.long.fc.baseline$model, AddFromHash, cnames.hash)
dat.long.fc.baseline$model.new <- factor(dat.long.fc.baseline$model.new, levels = c("F", "S", "C", "A", "S+C", "S+A"))
dat.long.fc.baseline$day.str <- paste("Day", dat.long.fc.baseline$day)


# Plot to check -----------------------------------------------------------
m.nice <- ggplot(subset(dat.long.fc.baseline, model.new %in% c("S+C", "S+A", "S") & day != 0), 
                 aes(x = model.new, y = exprs.amp.diff.rel, group = model.new)) + 
  geom_boxplot_jitter() + 
  facet_wrap(~day.str) + 
  theme_bw(24) + 
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlab("Model") + ylab("Relative Amp Difference (amp0 - amp1)/amp0") + 
  geom_hline(yintercept = 0, linetype = "dotted")

m.nice2 <- ggplot(subset(dat.long.fc.baseline, model.new %in% c("S+C", "S+A", "S") & day != 0), 
                 aes(x = model.new, y = exprs.amp.diff, group = model.new)) + 
  geom_boxplot_jitter() + 
  facet_wrap(~day.str) + 
  theme_bw(24) + 
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  xlab("Model") + ylab("Amp Difference (amp0 - amp1)") + 
  geom_hline(yintercept = 0, linetype = "dotted")

# plot in 2D
dat.mat <- reshape2::dcast(subset(dat.long.fc.baseline, model.new %in% c("S+C", "S+A", "S")) %>%
                             mutate(day.tmp = gsub(" ", "_", day.str)), 
                           formula = "gene + model.new ~ day.tmp", value.var = "exprs.amp")

dat.smat <- tidyr::gather(dat.mat, key = Day_trt, value = exprs.amp, c(Day_1, Day_2))

m.nice3 <- ggplot(dat.smat, aes(x = Day_0, y = exprs.amp)) + 
  geom_point(alpha = 0.125) + theme_bw() + 
  facet_grid(model.new~Day_trt) +  
  theme_bw() + 
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + 
  geom_abline(slope = 1)

# Check individual genes and parameters -----------------------------------


# for sleep model, do the S model depend on whether it is "UP" vs "DOWN"

params.sleep.w.relFC <- left_join(params.sleep, subset(dat.long.fc.baseline, day == 1, select = c(gene, model, exprs.amp.diff.rel, exprs.amp.diff))) %>%
  arrange(desc(exprs.amp.diff.rel))

params.mixed.w.relFC <- left_join(params.mixed, subset(dat.long.fc.baseline, day == 1, select = c(gene, model, exprs.amp.diff.rel, exprs.amp.diff))) %>%
  arrange(desc(exprs.amp.diff.rel))

ggplot(params.sleep.w.relFC, aes(x = exprs.amp.diff.rel, y = UL.delta)) + geom_point()  + theme_bw() 

ggplot(params.mixed.w.relFC, aes(x = exprs.amp.diff.rel, y = UL.delta)) + geom_point()  + theme_bw() 




pdf(file.path(outdir, "S_vs_SSA_downstream_with_amp_plots.pdf"), useDingbats = FALSE)
  PlotSAGene(fits, g = "Dbp", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  PlotSAGene(fits, g = "Nfil3", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  PlotSAGene(fits, g = "Per2", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  PlotSAGene(fits, g = "Ubn1", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  PlotSAGene(fits, g = "Leprot", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  PlotSAGene(fits, g = "Isca2", wake.collapsed, filt.time, low.pass.filt.time, dat.long.shift)
  print(mhistpol)
  print(mhistpol.af)
  print(mdens1)
  print(mdens1.af)
  print(m.nice)
  print(m.nice2)
  print(m.nice3)
  print(m.2d)
dev.off()

