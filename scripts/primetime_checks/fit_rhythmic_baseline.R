# Jake Yeung
# Date of Creation: 2018-08-16
# File: ~/projects/sleep_deprivation/scripts/primetime_checks/fit_rhythmic_baseline.R
# Take baseline time as circadian experiment and find rhythmic genes using F24


rm(list=ls())

jstart <- Sys.time()

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(reshape2)
library(wordcloud)
library(hash)
library(ggplot2)

source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/SummarizeModels.R")
source("scripts/functions/GetTFs.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/SleuthFunctions.R")
source("scripts/functions/ClusteringFunctions.R")
source("scripts/functions/LoadData.R")
source("scripts/functions/TimeShiftFunctions.R")
source("scripts/functions/GetClockGenes.R")


# Load data ---------------------------------------------------------------

LoadPrimetimeObjs()


col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic))) %>%
  filter(weight == max(weight))
fits.w.weight <- left_join(fits, subset(fits.bic, select = c(gene, model, weight)))

# Filter baseline filter rhythmic -----------------------------------------

dat.base <- subset(dat.long.shift, time <= 24)

fit.rhyth.base <- dat.base %>%
  group_by(gene) %>%
  do(FitRhythmic(., T.period = 24, use.weights = FALSE, get.bic = FALSE, condensed = FALSE))

fit.rhyth.base <- fit.rhyth.base %>%
  arrange(pval)

fit.rhyth.base.annot <- left_join(fit.rhyth.base, subset(fits.w.weight, select = c(gene, model, weight)))

g <- "Arntl"  # circadian in both experiments?
PlotBestFit(subset(dat.long.shift, gene == g), subset(fits, gene == g), filt.time, g, wake.collapsed, low.pass.filt.time, dat.eeg = dat.eeg.plot)

# Write table -------------------------------------------------------------

dir.create("tables/tables_for_charlotte.2018-08-16")
write.table(fit.rhyth.base.annot, file = "tables/tables_for_charlotte.2018-08-16/fit.rhyth.base.annot.txt", sep = "\t", quote = FALSE, row.names = FALSE, col.names = TRUE)


