# 2017-03-29
# Add low-pass filter to S process signal

rm(list=ls())


# Functions ---------------------------------------------------------------


# https://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
LowPass <- function(s, dt, gamma.m){
  # s: synthesis function
  # dt: time step
  # gamma.m: degradation rate of m
  # 
  
  # define smoothing factor
  alpha <- (dt * gamma.m) / (dt * gamma.m + 1)
  m <- rep(NA, length(s))
  m[[1]] <- alpha * s[[1]] / gamma.m
  for (i in seq(2, length(s))){
    m[[i]] <- m[i - 1] + alpha * (s[[i]] / gamma.m - m[i - 1])
  }
  return(m)
}

# https://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
LowPassRC <- function(x, dt, RC){
  # x: input
  # dt: time step
  # RC: time to charge capacitor
  
  # define smoothing factor
  alpha <- dt / (dt + RC)
  m <- rep(NA, length(x))
  m[[1]] <- alpha * x[[1]]
  for (i in seq(2, length(x))){
    m[[i]] <- m[i - 1] + alpha * (x[[i]] - m[i - 1])
  }
  return(m)
}



# Load fits ---------------------------------------------------------------

library(dplyr)
library(ggplot2)
library(wordcloud)
library(reshape2)

source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/FitFunctions.R")

load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.kallisto.with_batch_effect.Robj"); dat.orig <- dat.long.shift
load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.kallisto.combat.mean_only.FALSE.exprs_cutoff.2.5.Robj"); dat.after <- dat.long.c
load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"); dat.long.shift <- subset(dat.long.cleaned, time <= 78)
load("/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.circadian.flat.mix.step.maxAmpInf.tswitch.33.Robj")  # fits
fits$model <- apply(fits, 1, function(row) SelectBestModel(row, colnames(fits)))

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]
wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=tstep, max.time=max.time, filt.time=filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=max.time, downsamp = downsamp)


# Plot Rbm3 wiggles -------------------------------------------------------

jgene <- "Rbm3"
jgene <- "Homer1"
jgene <- "Dbp"
PlotFit(jgene, dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time=FALSE)

params <- GetParams.sleep(subset(fits, gene == jgene))
S.pred <- S.process.collapsed(params, wake.collapsed, filt.time)

dat.pred <- data.frame(time = filt.time, exprs = S.pred, model = "Process S")

ggplot(dat.pred, aes(x = time, y = exprs)) + geom_line() + theme_bw()

# pass through filter
m.hl <- 2  # hours
tau.m <- m.hl / log(2)
# m.ss <- max(S.pred) * 1.2
dat.test <- c(rep(0, 15), rep(1, 20), rep(0, length(S.pred) - (15 + 20)))
dat.pred.test <- data.frame(time = filt.time, exprs = dat.test, model = "Test")
# m.0 <- S.pred[[1]]
# m.s <- S.pred * tau.m
m.0 <- dat.test[[1]]
m.s <- dat.test * tau.m

S.pred.damped <- m.s - (m.s - m.0) * exp(-dat.pred$time / tau.m)

tstep <- 1
m.prev <- dat.test[[1]]
S.pred.damped <- rep(NA, length(S.pred))
S.pred.damped[[1]] <- m.prev
for (i in seq(2, length(S.pred))){
  m.s <- dat.test[[i]] * tau.m
  m.prev <- dat.test[i - 1]
  S.pred.damped[[i]] <- m.s - (m.s - m.prev) * exp(-tstep / tau.m)
}

dat.pred.damped <- data.frame(time = filt.time, exprs = S.pred.damped, model = "Process S Damped")

dat.pred.merged <- rbind(dat.pred.test, dat.pred.damped)

m <- ggplot(dat.pred.merged, aes(x = time, y = exprs, linetype = model)) + geom_line() + theme_bw()
print(m)



# Use integrating factor method to get m(t) -------------------------------

m.hl <- 7  # hours
tau.m <- m.hl / log(2)
gamma.m <- 1 / tau.m


# on Square Wave
tvec <- seq(length(S.pred))
dat.test <- c(rep(0, 15), rep(1, 20), rep(0, length(S.pred) - (15 + 20)))

dat.passed <- LowPass(s = dat.test, dt = 1, gamma.m = gamma.m)
dat.passed2 <- LowPass(s = dat.passed, dt = 1, gamma.m = gamma.m)
dat.passed3 <- LowPass(s = dat.passed2, dt = 1, gamma.m = gamma.m)

plot(tvec, dat.test / gamma.m, "l", ylim = c(0, max(dat.passed, dat.test / gamma.m)))
lines(tvec, dat.passed, lty = 2)
lines(tvec, gamma.m * dat.passed2, lty = 3)
lines(tvec, gamma.m^2 * dat.passed3, lty = 4)

# On Sine wave

tvec <- seq(0, 48, length.out = 1000)
w <- 2 * pi / 24
m.hl <- 24
tau.m <- m.hl / log(2)
gamma.m <- 1 / tau.m
x <- cos(w * tvec - 0)
y <- LowPass(s = x, dt = 1, gamma.m = gamma.m)
y2 <- LowPass(s = y, dt = 1, gamma.m = gamma.m)
y3 <- LowPass(s = y2, dt = 1, gamma.m = gamma.m)
y4 <- LowPass(s = y3, dt = 1, gamma.m = gamma.m)

plot(tvec, x / gamma.m, "l", ylim = c(min(x / gamma.m, y), max(x / gamma.m, y)))
lines(tvec, y, lty = 2)
lines(tvec, gamma.m * y2, lty = 3)
lines(tvec, gamma.m ^ 2 * y3, lty = 4)
lines(tvec, gamma.m ^ 3 * y4, lty = 5)



# Low pass from wiki ------------------------------------------------------

library(signal)

bf <- butter(2, 0.2, type="low")
b <- signal::filter(bf, S.pred)
plot(filt.time, S.pred, col="black", pch=20, "l", ylim = c(0, max(b, S.pred)))
points(filt.time, b, "o")

# with self-made low pass function
S.passed <- LowPassRC(S.pred, 1, RC = 5)
plot(filt.time, S.pred, col = "black", pch=20, "o", ylim = c(0, max(S.pred, S.passed)))
lines(filt.time, S.passed, lty = 2)

m.hl <- 10
tau.m <- m.hl / log(2)
gamma.m <- 1 / tau.m
S.in <- scale(S.pred, center = TRUE, scale = TRUE)
S.out <- LowPass(S.in, 1, gamma.m = log(2) / 2)
plot(filt.time, S.in / gamma.m, col = "black", pch=20, "o", ylim = c(min(S.in / gamma.m, S.passed), max(S.in / gamma.m, S.passed)))
lines(filt.time, S.out, lty = 2)
