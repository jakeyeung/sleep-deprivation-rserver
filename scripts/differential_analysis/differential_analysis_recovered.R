# 2016-10-12
# Jake Yeung
# Have genes recovered after 78 hours?

rm(list=ls())

library(dplyr)
library(ggplot2)
library(reshape2)
library(stringr)
library(hash)
library(reshape2)
library(gridExtra)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/StringFunctions.R")
source("scripts/functions/PhaseFunctions.R")
source("scripts/functions/GetClockGenes.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/GetTFs.R")

# Load --------------------------------------------------------------------

## LOAD GENE EXPRS
load("Robjs/dat.long.htseq.0to78hrs.reps.Robj", v=T)
dat.orig <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)
dat.long.shift <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)

times <- c(6, 30, 54, 78)
times <- c(0, 24, 48, 72)
dat.sub <- subset(dat.long.shift, time %in% times)
dat.sub$time[which(dat.sub$time == 24)] <- 0

# find ones different between ZT6 and ZT78
ttest.trt <- subset(dat.sub) %>%
  group_by(gene) %>%
  do(DoTtest(., "exprs ~ trtmnt")) %>%
  filter(pval < 1e-2) %>%
  arrange(desc(abs(sd.minus.nsd)))

print(head(ttest.trt, n = 50))

top.n <- 100
jgenes <- as.character(ttest.trt$gene)[1:top.n]

pdf("plots/differential_analysis/top_deviations_from_zt0.pdf")
for (jgene in jgenes){
  m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
    AddDarkPhases(alpha = 0.1) + 
    ggtitle(jgene)
  print(m)
}
dev.off()