# Jake Yeung
# Date of Creation: 2017-08-13
# File: ~/projects/sleep_deprivation/scripts/differential_analysis/sleuth_analysis.R
# Do differential analysis using Sleuth (better than t-test on log scale)

rm(list=ls())

# # # install sleuth if necessary
# source("http://bioconductor.org/biocLite.R")
# biocLite("rhdf5")
# 
# biocLite("BiocUpgrade")
# 
# library(devtools)
# devtools::install_github("pachterlab/sleuth")

library(sleuth)
# library(dplyr)

library(dplyr)
library(hash)
library(biomaRt)

source("scripts/functions/SleuthFunctions.R")

# Downstream of Sleuth (run things on vitalit) ----------------------------

infs <- list.files("/home/yeung/data/sleep_deprivation/sleuth_outputs", pattern = "*.Rdata", full.names = TRUE)
sleuths_and_fulls <- lapply(infs, LoadSleuthOutput)

sleuth_tables <- bind_rows(lapply(sleuths_and_fulls, function(l) l[[1]]))
full_models <- bind_rows(lapply(sleuths_and_fulls, function(l) l[[2]]))
effect_sizes <- dplyr::select(dplyr::filter(full_models, grepl("conditionUntreated", term)),
                target_id, gene, time.nsd, full_estimate = estimate, gene, time.nsd)

# ijsf <- inner_join(sleuth_tables, effect_sizes)
# fjsf <- full_join(sleuth_tables, effect_sizes)  # nothing missing


# inf <- "/home/yeung/data/sleep_deprivation/sleuth_outputs/sleuth_T3N_T3S.Rdata"
# load(inf, v=T)
# 
# zscores <- full_model %>%
#   group_by(target_id) %>%
#   summarise(zscore = diff(estimate) / sqrt(sum(std_error^2))) %>%
#   arrange(desc(abs(zscore)))
# 
# zscores.hash <- hash(zscores$target_id, zscores$zscore)
# 
# sleuth_table$zscore <- sapply(sleuth_table$target_id, function(x) zscores.hash[[x]])


# Assign to gene ----------------------------------------------------------

mart <- biomaRt::useMart(biomart = "ENSEMBL_MART_ENSEMBL",
                         dataset = "mmusculus_gene_ensembl",
                         host = "dec2015.archive.ensembl.org")
ttg <- biomaRt::getBM(
  attributes = c("ensembl_transcript_id", "transcript_version",
                 "ensembl_gene_id", "external_gene_name", "description",
                 "transcript_biotype"),
  mart = mart)
ttg <- dplyr::rename(ttg, target_id = ensembl_transcript_id,
                     ens_gene = ensembl_gene_id, ext_gene = external_gene_name)
head(ttg)

tx.gene.hash <- hash(ttg$target_id, ttg$ext_gene)
sleuth_table$gene <- sapply(sleuth_table$target_id, function(x){
  gene <- tx.gene.hash[[x]]
  if (!is.null(gene)){
    return(gene)
  } else {
    return(NA)
  }
})


# Find differentially expressed genes  ------------------------------------

sleuth_table.hits <- sleuth_table %>%
  dplyr::filter(qval < 0.2)
  

# Take those genes and plot the shapes ------------------------------------------

# will do this in a separate table!
