# Jake Yeung
# Date of Creation: 2018-08-18
# File: ~/projects/sleep_deprivation/scripts/primetime/primetime_motif_run_models.R
# Run models on motif activity and write to table

rm(list=ls())

jstart <- Sys.time()

setwd("/home/yeung/projects/sleep_deprivation")

library(ggplot2)
library(ggrepel)
library(dplyr)

source("scripts/functions/MaraDownstream.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/LoadData.R")

# Load EEG data ---------------------------------------------------------------

do.linear <- FALSE
log.eps <- 1

do.test <- TRUE

if (do.test){
  print(paste("Testing mode:", do.test))
}

jtswitch <- 33
jform <- "switch"

use.merged <- TRUE
if (!use.merged){
  max.time <- 72
} else {
  max.time <- 78
}
zt0tozt24 <- FALSE

# HALF LIFE LIMITS
min.hl <- 1/3
max.hl <- 24
min.hl.mrna <- 1/3
max.hl.mrna <- 24

lambda <- 1000
jmaxiter <- 3000

LoadPrimetimeObjs()


# Plot motifs -------------------------------------------------------------

top.n.motifs <- 45
plot.merged.models <- TRUE

if (plot.merged.models){
  ### OPTION A: plot merged models
  merge.models <- TRUE
  use.atacseq.peaks <- TRUE  # 15kb from promoter
  merge.all.sleep <- TRUE
  normalize.sitecounts <- TRUE
  center.sitecounts <- FALSE  # center the sitecounts across columns??
  weight.cutoff <- 0
} else {
  ### OPTION B: plot models separately
  merge.models <- FALSE
  use.atacseq.peaks <- TRUE  # 15kb from promoter
  merge.all.sleep <- FALSE
  normalize.sitecounts <- TRUE
  center.sitecounts <- FALSE  # center the sitecounts across columns??
  weight.cutoff <- 0.6
}
marasuffix <- paste0(".maxiterfix.multieeg_lowpass_logscale_merge_models.", merge.models, ".useATACseq.", use.atacseq.peaks)
if (merge.all.sleep){
  marasuffix <- paste0(marasuffix, ".sleepmerge.", merge.all.sleep)
}
if (normalize.sitecounts){
  # marasuffix <- paste0(marasuffix, ".normalizeN.", normalize.sitecounts)
  marasuffix <- paste0(marasuffix, ".normalizeNsd.", normalize.sitecounts)  # fix variance 2018-06-18
}
if (center.sitecounts){
  marasuffix <- paste0(marasuffix, ".centerN.", center.sitecounts)
}
if (weight.cutoff != 0.6){
  marasuffix <- paste0(marasuffix, ".weightCutoff.", weight.cutoff)
}

plotsuffix <- paste0("mergemodels.", plot.merged.models, ".weightCutoff.", weight.cutoff, ".", Sys.Date())

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))
genes.all <- as.character(unique(fits$gene))
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels

genes.lst[["genes_all"]] <- genes.all

if (merge.models){
  if (!merge.all.sleep){
    # copied from run_mara_on_models..combine_models script
    genes.lst <- MergeLists(genes.lst, "ampfree.step", "mixedaf")
    genes.lst <- MergeLists(genes.lst, "circadian", "mix")
    genes.lst <- RemoveLists(genes.lst, "ampfree.step")
    genes.lst <- RemoveLists(genes.lst, "mixedaf")
    genes.lst <- RemoveLists(genes.lst, "circadian")
    genes.lst <- RemoveLists(genes.lst, "mix")
  } else {
    # copied from run_mara.R script
    genes.lst <- MergeLists(genes.lst, "mix", "mixedaf")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf", "sleep")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep", "ampfree.step")
    genes.lst <- MergeLists(genes.lst, "mix_mixedaf_sleep_ampfree.step", "circadian")
    genes.lst <- RemoveLists(genes.lst, "mix")
    genes.lst <- RemoveLists(genes.lst, "mixedaf")
    genes.lst <- RemoveLists(genes.lst, "sleep")
    genes.lst <- RemoveLists(genes.lst, "ampfree.step")
    genes.lst <- RemoveLists(genes.lst, "circadian")
  }
} 

# match TFs 
tfs <- GetTFs(split.commas = FALSE, get.mat.only = TRUE)

do.mean <- FALSE
do.center <- TRUE
# gene.lab <- "genes_sleep"

outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", marasuffix)
# dir.create(outmain)

if (plot.merged.models){
  gene.labs <- c("mix_mixedaf_sleep_ampfree.step_circadian")
} else {
  gene.labs <- names(genes.lst)[which(names(genes.lst) != "flat")]
}

gene.lab <- c("mix_mixedaf_sleep_ampfree.step_circadian")

  print(gene.lab)
  outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))
  
  suffix <- paste0("mean.", do.mean,  ".centered.", do.center,  marasuffix, "/sleep_deprivation_gene_exprs_all")
  act.dir <- file.path(outdir, suffix)
  
  assertthat::assert_that(dir.exists(act.dir))
  
  act.zscore.lst <- LoadMaraOutput(act.dir)
  act.long <- act.zscore.lst$act.long
  zscores <- act.zscore.lst$zscores
  zscores$motif <- factor(as.character(zscores$motif), levels = zscores$motif)
  # zscores$label <- mapply(function(m, z) ifelse(z > 1.68, m, NA), as.character(zscores$motif), zscores$zscore)
  zscores$label <- mapply(function(m, z) ifelse(z > 1.46, m, NA), as.character(zscores$motif), zscores$zscore)
  
  
  m.zscores <- ggplot(zscores, aes(x = motif, y = zscore, label = label)) + geom_point() + geom_text_repel(size = 7) + 
    theme_bw(24) + 
    theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.x = element_blank(), axis.ticks.x = element_blank()) + 
    xlab("Index") + 
    ylab("Zscore from Mean Activity")
  print(m.zscores)
 
  fit.motif <- act.long %>%
    group_by(gene) %>%
    do(fit.sleep = FitProcessS(., wake.collapsed, exprs.cname = "exprs", time.cname = "time",
                               condensed=TRUE, pseudo = 0, min.hl = min.hl, max.hl = max.hl,
                               do.lowpass=TRUE, min.hl.mrna = min.hl.mrna, max.hl.mrna = max.hl.mrna,
                               low.pass.filter.times = low.pass.filt.time, jlambda = lambda, jmaxit = jmaxiter), 
       fit.flat = FitFlat(., use.weights = FALSE, get.bic = TRUE, condensed = TRUE),
       fit.circadian = FitRhythmic(., T.period = 24, use.weights=FALSE, get.bic=TRUE, condensed=TRUE),
       fit.ampfree.step = FitRhythmic.FreeAmp(., AmpFunc = AmpFunc, T.period = 24, tswitch = jtswitch, form = jform, include.intercept = TRUE, jmaxit = jmaxiter),
       fit.mixedaf = FitWeightedSFreeAmp(., AmpFunc = AmpFunc, wake.collapsed = wake.collapsed,
                                         exprs.cname = "exprs", time.cname = "time", T.period = 24,
                                         tswitch=jtswitch, form=jform, pseudo = 0, min.hl = min.hl,
                                         max.hl = max.hl, include.intercept = FALSE, do.lowpass=TRUE,
                                         min.hl.mrna = min.hl.mrna, max.hl.mrna = max.hl.mrna,
                                         low.pass.filter.times = low.pass.filt.time, jlambda = lambda, jmaxit = jmaxiter),
       fit.mix = FitWeightedSCircadian(., wake.collapsed, condensed = TRUE, pseudo = 0,
                                       min.hl = min.hl, max.hl = max.hl, include.mix.weight = FALSE,
                                       include.intercept = FALSE, do.lowpass=TRUE, min.hl.mrna = min.hl.mrna,
                                       max.hl.mrna = max.hl.mrna, low.pass.filter.times = low.pass.filt.time, jlambda = lambda, jmaxit = jmaxiter))
  fit.motif <- AddBICToDat(fit.motif)
  save(fit.motif, file = "Robjs//model_selection_motif_activity.tmp.Robj")
  fit.motif$model <- SelectBestModel(fit.motif, colnames(fit.motif))  # SRF is sleep gene
  save(fit.motif, file = "Robjs//model_selection_motif_activity.Robj")
  
  
print(Sys.time() - jstart)
