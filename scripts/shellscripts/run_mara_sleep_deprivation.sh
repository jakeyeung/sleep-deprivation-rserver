#!/bin/sh
# Jake Yeung
# Run MARA on sleep deprivation (i.e., do not try to plot stuff)
# 2016-04-12

# Nmain="/home/yeung/projects/tissue-specificity/data/sitecounts/motevo/liver_kidney_sitecounts_tissuespecpeaks_cutofflow_limited_crossprods"
# outmain="/home/yeung/projects/tissue-specificity/results/MARA.liver_kidney.limitprod"
Nmat=$1
outdir=$2
geneexprsdir=$3

# if not supplied then assume liver and kidney
if [ -z $geneexprsdir ]; then
	geneexprsdir="/home/yeung/data/sleep_deprivation/gene_exprs_for_mara/sleep_deprivation_gene_exprs_all.mat"
fi

combinescript="/home/yeung/projects/ridge-regression/run_scripts/combine_activities_and_plot.one_dir.sh"
runscript="/home/yeung/projects/ridge-regression/run_scripts/run_mara_batch_promoters.sh"
[[ ! -e $runscript ]] && echo "$runscript not found, exiting" && exit 1

bname=$(basename $Nmat)
subdir=$(basename $geneexprsdir)
[[ -d $outdir ]] && echo "$outdir found, exiting" && exit 1  # overwriting mode does not work! need this to prevent bugs: TODO just remove safely the directory?
[[ ! -d $outdir ]] && mkdir $outdir
[[ ! -e $Nmat ]] && echo "$Nmat not found, exiting" && exit 1
[[ ! -d $geneexprsdir ]] && echo "$geneexprsdir not found, exiting" && exit 1
# run MARA here
bash $runscript $geneexprsdir $outdir $Nmat
ret=$?; [[ $ret -ne 0  ]] && echo "ERROR: script failed" && exit 1
# bash $combinescript $outdir/$subdir
# ret=$?; [[ $ret -ne 0  ]] && echo "ERROR: script failed" && exit 1
