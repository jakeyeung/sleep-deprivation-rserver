#!/bin/sh
# Jake Yeung
# run_quantif_reads.sh
# Run quantif reads from Gobet 
# 2018-02-13

# pscript="/Users/yeung/Downloads/jake_IE-1/Quantif_Reads_IE_Jake.pl"  # exonbed, intronbed, bamf, b1, b2
prscript="/home/yeung/projects/sleep_deprivation/scripts/count_introns_exons/from_cedric/Quantif_Reads_IE_Jake.pl"
exonbed="/home/yeung/projects/sleep_deprivation/scripts/count_introns_exons/from_cedric/mm10_ens_exons_nonoverlap_all.no_chr.bed"
intronbed="/home/yeung/projects/sleep_deprivation/scripts/count_introns_exons/from_cedric/mm10_ens_introns_all.no_chr.bed"

outdir="
outf="/bigdata/sleep_intron_exon_counts"
bamf="/bigdata/sleep_bams/ZT0A.bam"
b1=1
b2=1000

perl $prscript $exonbed $intronbed $bamf $b1 $b2 > 


