# 2016-05-02
# RBM3 and HOMER1 have alternating transcript levels. Confirm this with Kallisto?

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

start <- Sys.time()

library(hash)
library(dplyr)
library(ggplot2)
library(reshape2)
library(stringr)

# Functions ---------------------------------------------------------------

source("~/projects/tissue-specificity/scripts/functions/BiomartFunctions.R")  # Transcript2Gene
source("scripts/functions/StringFunctions.R")
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/FitFunctions.R")

# Load --------------------------------------------------------------------

dat.long <- LoadProcessData(ftype = "kallisto", has.transcript = TRUE, make.neg.hours = FALSE)

# save(dat.long, file = "Robjs/dat.long.kallisto.withTx.Robj")

eps <- 1  # for log2 transform
# sum over samples
dat.long.summed <- SumOverSamples(dat.long, eps = eps, remove.mt.genes = TRUE, has.transcript = TRUE)
# log transform though
dat.long$exprs <- log2(dat.long$exprs + eps)

# plot example
# m <- PlotGeneAcrossTime(subset(dat.long.summed, gene == "Rbm3"), jsize = 1)
(m <- PlotGeneAcrossTime(subset(dat.long.summed, gene == "Homer1"), jsize = 1) + facet_wrap(~transcript))
# m + facet_wrap(~transcript)


# Find rhythmic isoforms --------------------------------------------------

dat.sub <- subset(dat.long.summed, trtmnt == "NSD" & gene %in% c("Homer1", "Cirbp", "Rbm3"))

# print(dat.long)

dat.fits.tx <- subset(dat.long, trtmnt == "NSD") %>%
  group_by(gene, transcript) %>%
  do(FitRhythmic(.))

save(dat.fits.tx, file = "Robjs/dat.fits.tx.withsamps.log2.Robj")

print(Sys.time() - start)

