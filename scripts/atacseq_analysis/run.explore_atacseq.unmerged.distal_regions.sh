#!/bin/sh
# Jake Yeung
# run.explore_atacseq.unmerged.distal_regions.sh
# Run
# 2016-10-11

rscript="/home/yeung/projects/sleep_deprivation/scripts/atacseq_analysis/explore_atacseq.unmerged.distal_regions.R"
dolog2="FALSE"
dolog2="TRUE"
for time1 in 3 6; do
	time2=$((time1 + 24))
	# echo $time1 $time2
	Rscript $rscript $time1 $time2 $dolog2&
done
wait
