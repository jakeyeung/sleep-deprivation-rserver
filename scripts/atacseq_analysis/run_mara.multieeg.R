# 2016-08-04
# Jake Yeung
# Run MARA

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(methods)
library(ggplot2)
library(reshape2)
library(dplyr)
library(preprocessCore)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")


do.center <- TRUE
# center
do.mean <- FALSE
suffix <- ".multieeg"

# Load -------------------------------------------------------------------


load("Robjs/dat.long.htseq.0to78hrs.reps.Robj")
dat.orig <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)
dat.long.shift <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)

inf.fits <- "/home/yeung/projects/sleep_deprivation/Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.noIntnoWeight.pseudo0.usemerged.TRUE.zt0tozt24.FALSE.Robj"
load(inf.fits, v=T)
cnames <- c("sleep", "circadian", "flat", "mixed")
s.i <- which(colnames(fits) == "bic.sleep")
c.i <- which(colnames(fits) == "bic.circadian")
f.i <- which(colnames(fits) == "bic.flat")
m.i <- which(colnames(fits) == "bic.mix")
fits$model <- apply(fits, 1, function(row){
  bic.s <- as.numeric(row[[s.i]])
  bic.c <- as.numeric(row[[c.i]])
  bic.f <- as.numeric(row[[f.i]])
  bic.m <- as.numeric(row[[m.i]])
  bic.vec <- c(bic.s, bic.c, bic.f, bic.m)
  return(cnames[[which.min(bic.vec)]])
})

fits.bic <- melt(data = subset(fits, select = c(-fit.circadian, -fit.flat)), 
                 id.vars = c("gene", "model", "fit.sleep", "fit.mix"), 
                 measure.vars = c("bic.sleep", "bic.circadian", "bic.flat", "bic.mix"), 
                 variable.name = "bic.model", 
                 value.name = "bic")

fits.bic <- fits.bic %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))


# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
genes.sleep <- as.character(unique(subset(fits.bic, model == "sleep" & weight > 0.95)$gene))
genes.circadian <- as.character(unique(subset(fits.bic, model == "circadian" & weight > 0.95)$gene))
genes.mixed <- as.character(unique(subset(fits.bic, model == "mixed" & weight > 0.95)$gene))
genes.lst <- list("genes_all" = genes.all, 
                  "genes_sleep" = genes.sleep,
                  "genes_circadian" = genes.circadian,
                  "genes_mixed" = genes.mixed)

# Write gene expression (ALL GENES) ---------------------------------------

dat.orig$sampname <- apply(dat.orig, 1, function(row) paste("ZT", sprintf("%02d", as.numeric(row[4])), sep = "_"))

if (do.mean){
  dat.orig.mean <- dat.orig %>%
    group_by(gene, sampname) %>%
    summarise(exprs = mean(exprs))
} else {
  dat.orig.mean <- dat.orig
  dat.orig.mean$sampname <- paste(dat.orig$sampname, dat.orig$samp, sep = "_")
}


if (do.center){
  dat.orig.mean <- dat.orig.mean %>%
    group_by(gene) %>%
    mutate(exprs = exprs - mean(exprs))
}

M <- dcast(dat.orig.mean, gene ~ sampname, value.var = "exprs")

E.dir <- file.path("/home/yeung/data/sleep_deprivation/gene_exprs_for_mara", paste0("mean.", do.mean, ".centered.", do.center))
dir.create(E.dir, showWarnings = FALSE)
E.out <- file.path(E.dir, paste0("sleep_deprivation_gene_exprs_all", ".mean.", do.mean, ".centered.", do.center, ".mat"))

# rename gene to Gene.ID
colnames(M)[[1]] <- "Gene.ID"
if (!file.exists(E.out)){
  write.table(M, E.out, append = FALSE, quote = FALSE, row.names = FALSE, col.names = TRUE, sep = "\t")
} else {
  print("File exists, skipping writing of gene expression matrix")
}

# Write sitecounts matrix (subset of genes) -------------------------------

N.dir <- paste0("/home/yeung/data/sleep_deprivation/sitecount_matrices_for_mara", suffix)
dir.create(N.dir)
Npath <- "/home/yeung/projects/tissue-specificity/data/sitecounts/motevo/sitecount_matrix_geneids"
N <- read.table(Npath, header=TRUE)

# gene.lab <- "genes_all"
gene.labs <- names(genes.lst)

for (gene.lab in gene.labs){
  print(paste("Running mara for", gene.lab))
  N.sub <- subset(N, Gene.ID %in% genes.lst[[gene.lab]])
  N.out <- file.path(N.dir, paste0("sitecounts_swiss_regulon_promoters_only.", gene.lab, ".mat"))
  
  if (!file.exists(N.out)){
    write.table(N.sub, file = N.out, append = FALSE, quote = FALSE, sep = "\t", col.names = TRUE, row.names = FALSE)
  } else {
    print("Sitecounts exists, skipping")
  }
  
  
  # Run MARA ----------------------------------------------------------------
  
  marascript <- "/home/yeung/projects/sleep_deprivation/scripts/shellscripts/run_mara_sleep_deprivation.sh"
  outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", suffix)
  dir.create(outmain, showWarnings = FALSE)
  outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))
  # do not create outdir, because MARA will not run on an already-created directory 
  cmd <- paste("bash", marascript, N.out, outdir, E.dir)
  
  system(cmd)
}


