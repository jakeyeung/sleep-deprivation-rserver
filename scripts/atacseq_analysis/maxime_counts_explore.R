# Jake Yeung
# Date of Creation: 2018-01-19
# File: ~/projects/sleep_deprivation/scripts/atacseq_analysis/maxime_counts_explore.R
# Explore Maxime's counts

rm(list=ls())

library(dplyr)
library(ggplot2)

GetGenomicRegion <- function(jpeak_id, meta){
  jsub <- subset(meta, peak_id == jpeak_id)
  return(paste0(jsub$chromo, ":", jsub$start, "-", jsub$end))
}

inf <- "/home/yeung/data/sleep_deprivation/atacseq_from_maxime/ATAC_Seq_logCPM.txt"
metainf <- "/home/yeung/data/sleep_deprivation/atacseq_from_maxime/TimePointsMerged.gtf"
dat <- read.table(inf, header = TRUE)
meta <- read.table(metainf); colnames(meta) <- c("chromo", "Method", "Type", "start", "end", "blank1", "strand", "blank2", "id", "peak_id", "blank3")

# get hits

jhit <- "chr19_108507"

jhit <- "chr10_28307"

jhit <- "chr1_11888"


jhit <- "chr18_102442"

jhit <- "chr8_198446"

jhit <- "chr4_139662"  # most promising hit, why didnt I find this before??

print(GetGenomicRegion(jhit, meta))
range(dat[jhit, ])

# compare T27 versus T3
cnames.grep <- "^T3S|^T3N"
dat.sub <- dat[jhit, grepl(cnames.grep, colnames(dat))]

x <- c(1, 1, 1, 2, 2, 2)
plot(x, dat.sub)
