#!/bin/sh
# Jake Yeung
# run.mat_to_sql.sh
# Run mat to sql 
# 2016-07-29

mysqlscript="/home/yeung/projects/tissue-specificity/mysql/mat_to_sql.R"
[[ ! -e $mysqlscript ]] && echo "$mysqlscript not found, exiting" && exit 1

# inmat="/home/yeung/projects/sleep_deprivation/mysql/dhs_signal_windows500.long.mat"
inmat="/home/yeung/projects/sleep_deprivation/mysql/test"
outsql="/home/yeung/projects/sleep_deprivation/mysql/dhs_signal_windows500.long.sql"
metadat="/home/yeung/projects/sleep_deprivation/mysql/metadata.txt"
[[ ! -e $inmat ]] && echo "$inmat not found, exiting" && exit 1
[[ ! -e $metadat ]] && echo "$metadat not found, exiting" && exit 1

cnames=`cat $metadat | grep -Po '(?<=atacseq/).+?(?=.bam)' | tr '\n' ',' | sed 's/,$//g'`

tblname="sleep_deprivation_windows500_dhs_signal"

echo "Rscript $mysqlscript $inmat $outsql $cnames $tblname"

