#!/bin/sh
# Jake Yeung
# run.kallisto_explore_combat_transcripts.sh
# Run it 
# 2017-08-31

inf="/home/yeung/projects/sleep_deprivation/scripts/round_two_analysis_transcripts/kallisto_explore_combat_transcripts.R"
[[ ! -e $inf ]] && echo "$inf not found, exiting" && exit 1

# cutoff="1.25"

# for cutoff in "1.25" "2.5"; do
# for cutoff in "0.5" "1"; do
for cutoff in "2" "2.25"; do
  Rscript $inf $cutoff&
done

wait
