# Jake Yeung
# Date of Creation: 2017-08-31
# File: ~/projects/sleep_deprivation/scripts/round_two_analysis/kallisto_explore_combat_transcripts.R
# Can we do batch effect correction on transcripts?

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

jstart <- Sys.time()

library(stringr)
library(hash)
library(dplyr)
library(ggplot2)
library(reshape2)
library(PhaseHSV)
library(wordcloud)
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/StringFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/TimeShiftFunctions.R")
source("scripts/functions/IntegerFunctions.R")
source("~/projects/tissue-specificity/scripts/functions/BiomartFunctions.R")  # Transcript2Gene
source("scripts/functions/RoundTwoPCAScripts.R")




# Set constants that can be changed ---------------------------------------
eps <- 1
# cutoff <- 2.5  # original cutoff (first try)
# cutoff <- 2.25  # cutoff for lowly expressed genes.
save.plots <- FALSE  # debug
with.tx <- TRUE

# args <- commandArgs(trailingOnly = TRUE)
# cutoff <- as.numeric(args[[1]])
cutoff <- 1

print(paste("Cutoff:", cutoff))
# cutoff <- 1.25  # original cutoff (first try)

# Load --------------------------------------------------------------------

load("/home/yeung/projects/sleep_deprivation/Robjs/dat.long.kallisto.withTx.Robj", v=T)

dat.long.gene1 <- dat.long %>%
  group_by(gene, transcript, samp, time, trtmnt) %>%
  summarise(exprs = sum(exprs))
dat.long.gene1$exprs <- log2(dat.long.gene1$exprs + eps)
dat.long.shift1 <- MakeDatShift(dat.long.gene1)
  

inf <- "/home/yeung/data/sleep_deprivation/kallisto_round_two/SDrecovery_kallisto_tpm_round2.txt"
metaf <- "/home/yeung/data/sleep_deprivation/SDrecovery_design_formatted.round2.txt"

dat.long2 <- LoadProcessData(ftype = "kallisto2", has.transcript = TRUE, make.neg.hours = FALSE)

print(head(subset(dat.long2, gene == "Dbp" & transcript == "ENSMUST00000080885" & time == 12 & trtmnt == "NSD" & samp == 1)))

dat.long.gene <- dat.long2 %>%
  group_by(gene, transcript, samp, time, trtmnt, batch) %>%
  summarise(exprs = sum(exprs))

# make log2 exprs
dat.long.gene$exprs <- log2(dat.long.gene$exprs + eps)


# Shift SD times ----------------------------------------------------------

# Handle 168 and 174 timepoints in NSD
dat.long.gene <- RenameTime(dat.long.gene, 168, 0, trt = "NSD", increment.samp = 70)
dat.long.gene <- RenameTime(dat.long.gene, 174, 6, trt = "NSD", increment.samp = 70)
dat.long.shift <- MakeDatShift(dat.long.gene)

# Plot example genes ------------------------------------------------------

jgene <- "Dbp"
jtranscript <- "ENSMUST00000080885"
m.gene2 <- PlotPoints(subset(dat.long.shift, gene == jgene & transcript == jtranscript & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "New Data"))
m.gene1 <- PlotPoints(subset(dat.long.shift1, gene == jgene  & transcript == jtranscript & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "Old Data"))
multiplot(m.gene1, m.gene2, cols = 2)

m.gene.long <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "New Data"))
print(m.gene.long)

print(m.gene2)


# Do PCAs -----------------------------------------------------------------

dat.long.shift$ZT <- sapply(dat.long.shift$time, function(x){
  zt <- TimeToZT(x, period = 24)
  ztstr <- sprintf("ZT%02d", zt)
})
dat.long.shift$sampname <- paste(dat.long.shift$trtmnt, dat.long.shift$time, dat.long.shift$batch, dat.long.shift$samp, sep = "_")

# remove lowly expressed genes
if (save.plots){
  pdf(paste0("plots/combat_by_tx/cutoff_gene_expression", cutoff, ".transcript.", with.tx, ".pdf"))
  m <- ggplot(dat.long.shift, aes(x = exprs)) + geom_histogram(bins = 50) + geom_vline(xintercept=cutoff)
  print(m)
  dev.off()
}

# remove genes whose entire time course (75%) is below cutoff
dat.long.filt <- dat.long.shift %>%
  group_by(gene, transcript) %>%
  filter(quantile(exprs, probs = 1) >= cutoff)

if (save.plots){
  pdf(paste0("plots/combat_by_tx/1-gene_expression_cutoff.", cutoff, ".transcript.", with.tx, ".pdf"))
  m <- ggplot(dat.long.filt, aes(x = exprs)) + geom_histogram(bins = 50) + geom_vline(xintercept=cutoff)
  print(m)
  dev.off()
}

dat.pca <- DoPca(subset(dat.long.filt), left.formula = "transcript ~ ")
cols <- GetCols(rownames(dat.pca$rotation))
pchs <- GetPchs(rownames(dat.pca$rotation))

if (save.plots){
  pdf(paste0("plots/combat_by_tx/1-pca_original_data.min_exprs.", cutoff, ".transcript.", with.tx, ".pdf"))
}
pc1 <- 1
pc2 <- 2
# plot(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs, cex = 2)
# text(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], labels = rownames(dat.pca$rotation), col = cols, cex = 0.75)

textplot(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], words = rownames(dat.pca$rotation), 
         col = cols, cex = 0.85, xlab = paste0("PC", pc1), ylab = paste0("PC", pc2),
         main="PCA before batch effect correction. Batch 2 labeled with *")
points(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs, cex = 1.5)
screeplot(dat.pca)

# slight batch effect from PC1 and PC2, check which genes are affected
# label genes in top 75 in both PC1 and PC2
p <- 0.6
xmin <- quantile(abs(dat.pca$x[, pc1]), probs = p)
ymin <- quantile(abs(dat.pca$x[, pc2]), probs = p)

genes.signif <- abs(dat.pca$x[, pc1]) >= xmin & abs(dat.pca$x[, pc2]) >= ymin
genes.lab <- rownames(dat.pca$x)
genes.lab[!genes.signif] <- NA


plot(dat.pca$x[, pc1], dat.pca$x[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = ".",
     main="PCA (gene loadings) before batch correction")
text(dat.pca$x[, pc1], dat.pca$x[, pc2], labels = genes.lab, cex = 0.75)
abline(v = 0, h = 0)
# abline(v = c(-xmin, xmin), h = c(-ymin, ymin), lty = 3)

if (save.plots){
  dev.off()
}

# # problem genes in top left and bottom right 
# genes.tp <- genes.lab[dat.pca$x[, pc1] <= -xmin & dat.pca$x[, pc2] >= ymin]
# genes.br <- genes.lab[dat.pca$x[, pc1] >= xmin & dat.pca$x[, pc2] <= -ymin]
# genes.bad <- c(genes.tp, genes.br)
# 
# # "good" genes in top right and bottom left?
# ggenes.tp <- genes.lab[dat.pca$x[, pc1] <= -xmin & dat.pca$x[, pc2] <= -ymin]
# ggenes.br <- genes.lab[dat.pca$x[, pc1] >= xmin & dat.pca$x[, pc2] >= ymin]
# ggenes <- c(ggenes.tp, ggenes.br)

genes.bad <- c()
ggenes <- rownames(dat.pca$x)  # all genes are good


# Redo PCA with bad genes removed
# dat.pca.cleaner <- DoPca(subset(dat.long.filt, gene %in% ggenes), left.formula = "transcript ~ ")
dat.pca.cleaner <- DoPca(subset(dat.long.filt, !gene %in% genes.bad), left.formula = "transcript ~ ")
screeplot(dat.pca.cleaner)
cols.cleaner <- GetCols(rownames(dat.pca.cleaner$rotation))
pchs.cleaner <- GetPchs(rownames(dat.pca.cleaner$rotation))
pc1 <- 1
pc2 <- 2
plot(dat.pca.cleaner$rotation[, pc1], dat.pca.cleaner$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs.cleaner, cex = 2, main = paste("Genes in PCA:", nrow(dat.pca.cleaner$x)))
text(dat.pca.cleaner$rotation[, pc1], dat.pca.cleaner$rotation[, pc2] + 0.0075, labels = rownames(dat.pca.cleaner$rotation), col = cols.cleaner)


# Are bad genes lowly expressed -------------------------------------------

# label genes as bad or good
genes.hash <- hash(c(genes.bad, ggenes), c(rep("bad", length(genes.bad)), rep("good", length(ggenes))))

dat.long.filt$gene.lab <- sapply(as.character(dat.long.filt$gene), function(g){
  g.lab <- genes.hash[[g]]
  g.lab <- ifelse(is.null(g.lab), "other", g.lab)
})

ggplot(dat.long.filt, aes(x = sampname, y = exprs, fill = as.factor(batch))) + geom_boxplot() + facet_wrap(~gene.lab)


# Can we handle batch effects by ComBat? ----------------------------------

library(devtools)
dev_mode(TRUE)

jmean.only <- FALSE
# install_github("jtleek/sva-devel")
library(sva)

# keep track of gene to transcript mapping
# gene.tx.hash <- hash(as.character(dat.long.filt$gene), as.character(dat.long.filt$transcript))
tx.gene.hash <- hash(as.character(dat.long.filt$transcript), as.character(dat.long.filt$gene))

dat.lst <- DoPca(subset(dat.long.filt), return.mat.and.pca = TRUE, input.is.long = TRUE, jcenter = FALSE, jscale = FALSE, left.formula = "transcript ~ ")
edata <- dat.lst$mat
pheno <- data.frame(sampname = colnames(edata),
                    timept = sapply(colnames(edata), function(cname) strsplit(cname, split = "_")[[1]][[2]]),
                    batch = sapply(colnames(edata), function(cname) strsplit(cname, split = "_")[[1]][[3]]))
batch <- pheno$batch
modcombat <- model.matrix(~timept, data = pheno)
edata.combatd <- ComBat(dat=edata, batch=batch, mod=modcombat, par.prior = TRUE, prior.plots = FALSE, mean.only=jmean.only, ref.batch = 1)  # requires develop version
# check for identical column names
if(any(duplicated(colSums(edata.combatd.tx)))){
  stop("ERROR: Some columns are duplicated!")
}

par(mfrow=c(1,1))
plot(density(unlist(apply(edata.combatd, 1, var))), log="x")
plot(density(unlist(apply(edata.combatd, 1, mean))))

dev_mode(FALSE)



# Run PCA on Batch-effect-removed data ------------------------------------


dat.pca.c <- DoPca(edata.combatd, return.mat.and.pca = FALSE, input.is.long = FALSE, jcenter = TRUE, jscale = FALSE)
cols.c <- GetCols(rownames(dat.pca.c$rotation))
pchs.c <- GetPchs(rownames(dat.pca.c$rotation))

if (save.plots){
  pdf(paste0("plots/combat_by_tx/2-pca_after_combat.min_exprs.", cutoff, ".transcript.", with.tx, ".pdf"))
}
pc1 <- 1
pc2 <- 2
# plot(dat.pca.c$rotation[, pc1], dat.pca.c$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs.c, cex = 2)
# text(dat.pca.c$rotation[, pc1], dat.pca.c$rotation[, pc2], labels = rownames(dat.pca.c$rotation), col = cols.c, cex = 0.75)

textplot(dat.pca.c$rotation[, pc1], dat.pca.c$rotation[, pc2], words = rownames(dat.pca.c$rotation), 
         col = cols, cex = 0.85, xlab = paste0("PC", pc1), ylab = paste0("PC", pc2),
         main = "PCA after batch effect correction. Batch 2 labeled with *")
points(dat.pca.c$rotation[, pc1], dat.pca.c$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs, cex = 1.5)
screeplot(dat.pca.c)

# Plot gene loadings
p <- 0.6
xmin <- quantile(abs(dat.pca.c$x[, pc1]), probs = p)
ymin <- quantile(abs(dat.pca.c$x[, pc2]), probs = p)

genes.signif <- abs(dat.pca.c$x[, pc1]) >= xmin & abs(dat.pca.c$x[, pc2]) >= ymin
genes.lab <- rownames(dat.pca.c$x)
genes.lab[!genes.signif] <- NA

plot(dat.pca.c$x[, pc1], dat.pca.c$x[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = ".", 
     main = "PCA (gene loadings) after batch effect correction")
text(dat.pca.c$x[, pc1], dat.pca.c$x[, pc2], labels = genes.lab, cex = 0.75)
abline(v = 0, h = 0)

if (save.plots){
  dev.off()
}

# Plot genes after ComBat -------------------------------------------------

dat.long.c <- melt(edata.combatd, varnames = c("transcript", "SampID"), value.name = "exprs")
dat.long.c$transcript <- as.character(dat.long.c$transcript)
dat.long.c$gene <- sapply(as.character(dat.long.c$transcript), function(g) tx.gene.hash[[g]])
dat.long.c$SampID <- as.character(dat.long.c$SampID)
dat.long.c$trtmnt <- sapply(dat.long.c$SampID, function(s) strsplit(s, "_")[[1]][[1]])
dat.long.c$time <- as.numeric(sapply(dat.long.c$SampID, function(s) strsplit(s, "_")[[1]][[2]]))
dat.long.c$batch <- sapply(dat.long.c$SampID, function(s) strsplit(s, "_")[[1]][[3]])
dat.long.c$samp <- sapply(dat.long.c$SampID, function(s) strsplit(s, "_")[[1]][[4]])

jgene <- "Dbp"
jtranscript <- "ENSMUST00000080885"
m.gene.clean <- PlotPoints(subset(dat.long.c, gene == jgene & transcript == jtranscript & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "Batch Effect Removed"))
m.gene.orig <- PlotPoints(subset(dat.long.shift, gene == jgene & transcript == jtranscript & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene, "Original Data"))
multiplot(m.gene.clean, m.gene.orig, cols = 2)

# Write data to Robjs -----------------------------------------------------

fixed.fout <- paste0("Robjs/combat_by_tx/dat.long.kallisto.combat.mean_only.", jmean.only, ".exprs_cutoff.", cutoff, ".transcript.", with.tx, ".Robj")
if (!file.exists(fixed.fout)){
  save(dat.long.c, file = fixed.fout)
}

orig.fout <- paste0("Robjs/combat_by_tx/dat.long.kallisto.with_batch_effect.transcript.", with.tx, ".Robj")
if (!file.exists(orig.fout)){
  save(dat.long.shift, file = orig.fout)
}

print(Sys.time() - jstart)
