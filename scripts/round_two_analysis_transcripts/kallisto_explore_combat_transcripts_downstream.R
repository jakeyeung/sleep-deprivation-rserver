# Jake Yeung
# Date of Creation: 2017-08-31
# File: ~/projects/sleep_deprivation/scripts/round_two_analysis_transcripts/kallisto_explore_combat_transcripts_downstream.R
# Downstream analysis to check whether we see expected observations (e.g., from Homer1, can do t-test to find differential directions)

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)

source("scripts/functions/PlotFunctions.R")
source("scripts/functions/SleuthFunctions.R")

do.linear <- FALSE

# Load data ---------------------------------------------------------------

# LOAD RNASEQ GENE LEVEL
max.time <- 78
load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}
dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)
# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# LOAD TRANSCRIPT LEVEL 

# it appears a small cutoff is still sufficient to find a "good" PCA plot after combat. So we choose cutoff = 0.5

jcutoff <- 2.5

# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/combat_by_tx/dat.long.kallisto.combat.mean_only.FALSE.exprs_cutoff.0.5.transcript.TRUE.Robj"
# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/combat_by_tx/dat.long.kallisto.combat.mean_only.FALSE.exprs_cutoff.1.25.transcript.TRUE.Robj"
inf <- paste0("/home/yeung/projects/sleep_deprivation/Robjs/combat_by_tx/dat.long.kallisto.combat.mean_only.FALSE.exprs_cutoff.", jcutoff, ".transcript.TRUE.Robj")

load(inf, v=T)

jgene <- "Rbm3"
jgene <- "Homer1"
jgene <- "Dnm1l"
m.gene.clean <- PlotPoints(subset(dat.long.c, gene == jgene & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene)) + 
  facet_wrap(~transcript)
print(m.gene.clean)

transcripts.keep <- as.character(unique(dat.long.c$transcript))


# Do t-test for different transcripts ---------------------------------------------------------------

# Fancier diff analysis with shrinkage
infs <- list.files("/home/yeung/data/sleep_deprivation/sleuth_outputs", pattern = "*.Rdata", full.names = TRUE)
sleuths_and_fulls <- lapply(infs, LoadSleuthOutput)
sleuth_tables <- bind_rows(lapply(sleuths_and_fulls, function(l) l[[1]]))
full_models <- bind_rows(lapply(sleuths_and_fulls, function(l) l[[2]]))
full_models <- full_models %>%
  mutate(estimate = estimate / log(2),
         std_error = estimate / log(2))
effect_sizes <- dplyr::select(dplyr::filter(full_models, grepl("conditionUntreated", term)),
                              target_id, gene, time.nsd, full_estimate = estimate, gene, time.nsd)
sleuth_tables <- dplyr::inner_join(sleuth_tables, effect_sizes)
genes.hits <- unique(as.character(subset(sleuth_tables, pval < 1e-3 & abs(full_estimate) > 0.5)$gene))

sleuth_tables <- sleuth_tables %>%
  dplyr::filter(target_id %in% transcripts.keep)

jtime <- 3
jgene <- "Dnm1l"

print(subset(sleuth_tables, gene == jgene & time.nsd == jtime))



# Find significant differences with opposing fold changes -----------------


dat.long.mean <- dat.long.c %>%
  group_by(transcript, gene) %>%
  summarise(exprs.mean = mean(exprs), frac.zeros = length(which(exprs < 0.5)) / length(exprs))
dat.long.mean <- dplyr::rename(dat.long.mean, target_id = "transcript")

# Can play with filter here to get different genes

get.mode <- "diverge"
get.mode <- "signif"

if (get.mode == "diverge"){
  # misses Homer1
  st.duals <- sleuth_tables %>%
    rowwise() %>%
    dplyr::filter(pval < 0.1) %>%
    group_by(gene, time.nsd) %>%
    dplyr::filter(prod(range(full_estimate)) < 0)
} else {
  # try to capture Homer1
  st.duals <- sleuth_tables %>%
    rowwise() %>%
    dplyr::filter(pval < 0.01) %>%
    group_by(gene, time.nsd) %>%
    dplyr::filter(abs(diff(range(full_estimate))) > 1)
}

st.duals <- dplyr::inner_join(st.duals, dat.long.mean)

st.duals_sum <- st.duals %>%
  group_by(gene, time.nsd) %>%
  dplyr::filter(exprs.mean > 2 & frac.zeros < 0.1) %>%
  summarise(min.change = min(full_estimate), max.change = max(full_estimate), max.delta = diff(range(full_estimate)), max.prod = prod(range(full_estimate))) %>%
  arrange(desc(max.delta))
  
print(head(st.duals_sum))  # many are lowly expressed genes? Add that info into table
print(head(st.duals_sum %>% arrange(max.prod)))  # many are lowly expressed genes? Add that info into table


jgene <- "Tbrg4"
jgene <- "Hdhd2"
jgene <- "Ubtf"

jgene <- "Ablim2"
jgene <- "Dnm1l"

jgene <- "Gnas"

jgene <- "Csde1"
jgene <- "Celf3"
jgene <- "Agpat1"
jgene <- "Ktn1"

jgene <- "Hnrnpdl"
jgene <- "Dgkz"

jgene <- "Csde1"
jgene <- "Celf3"

jgene <- "Homer1"
jgene <- "Ktn1"
jgene <- "Fubp1"

jgene <- "Anks1b"

jgene <- "Agpat1"

jgene <- "Homer1"
jgene <- "Rbm3"
jgene <- "Dclk1"
jtime <- subset(st.duals_sum, gene == jgene)$time.nsd[[1]]; jtx <- subset(st.duals, gene == jgene & time.nsd == jtime)$target_id

m.tx.wrap <- PlotPoints(subset(dat.long.c, gene == jgene & transcript %in% jtx & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene)) + 
  facet_wrap(~transcript) + 
  theme(aspect.ratio = 1)
print(m.tx.wrap)

# show top two divergent targets?
jsub <- subset(st.duals, gene == jgene) %>%
  group_by(gene) %>%
  dplyr::filter(full_estimate == max(full_estimate) | full_estimate == min(full_estimate))
jtranscripts <- jsub$target_id
m.tx.merge <- PlotPoints(subset(dat.long.c, gene == jgene & time <= 78 & transcript %in% jtranscripts), 
                           expand.zero = TRUE, colour.pts = FALSE, dotsize = 3.5, has.batch = TRUE, group.tx = TRUE) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene))
print(m.tx.merge)

# plot the gene expression to see any differences
m.gene.clean <- PlotPoints(subset(dat.long.shift, gene == jgene & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste("RNASeq:", jgene)) + 
  theme(aspect.ratio = 1)
print(m.gene.clean)


# Plot outputs to pdf -----------------------------------------------------

top.n <- min(nrow(st.duals_sum), 50)
g.hits <- unique(st.duals_sum$gene)[1:top.n]

plotdir <- paste0("/home/yeung/projects/sleep_deprivation/plots/combat_by_tx_divergent_responses.cutoff.", jcutoff)
dir.create(plotdir)

# write table
fprefix <- paste0("isoform_differential_responses_getmode.", get.mode)
write.table(st.duals, file = file.path(plotdir, paste0(fprefix, ".txt")))

pdf(file.path(plotdir, paste0(fprefix, ".pdf")))

for (jgene in g.hits){
  
  jtime <- subset(st.duals_sum, gene == jgene)$time.nsd[[1]]; jtx <- subset(st.duals, gene == jgene & time.nsd == jtime)$target_id
  
  jtitle.tx <- paste("Transcript level:", jgene, "diff. exprs'd (NSD vs SD) at time:", jtime)
  jtitle.gene <- paste("Gene level:", jgene)
  m.tx.wrap <- PlotPoints(subset(dat.long.c, gene == jgene & transcript %in% jtx & time <= 78), expand.zero = TRUE, colour.pts = FALSE, dotsize = 1, has.batch = TRUE)  +  
    ylab("log2 mRNA accumulation") +
    AddDarkPhases(alpha = 0.1) +
    ggtitle(jtitle.tx) + 
    facet_wrap(~transcript) + 
    theme(aspect.ratio = 1)
  print(m.tx.wrap)
  
  # show top two divergent targets?
  jsub <- subset(st.duals, gene == jgene) %>%
    group_by(gene) %>%
    dplyr::filter(full_estimate == max(full_estimate) | full_estimate == min(full_estimate))
  jtranscripts <- jsub$target_id
  m.tx.merge <- PlotPoints(subset(dat.long.c, gene == jgene & time <= 78 & transcript %in% jtranscripts), 
                           expand.zero = TRUE, colour.pts = FALSE, dotsize = 2, has.batch = TRUE, group.tx = TRUE) +
    theme(aspect.ratio = 1, legend.position = "bottom") + 
    AddDarkPhases(alpha = 0.1) +
    ggtitle(jtitle.tx)
  print(m.tx.merge)
  
  # plot the gene expression to see any differences
  m.gene.clean <- PlotPoints(subset(dat.long.shift, gene == jgene & time <= 78), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3, has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
    AddDarkPhases(alpha = 0.1) +
    ggtitle(paste("Gene level:", jgene)) + 
    theme(aspect.ratio = 1)
  print(m.gene.clean)
}
dev.off()

# Plot some examples ------------------------------------------------------

library(sleuth)
load("/home/yeung/data/sleep_deprivation/sleuth_outputs/sleuth_T3N_T3S.Rdata", v=T)

jtx <- jtranscripts[[1]]
plot_bootstrap(so, jtx, units = "est_counts")
# plot_bootstrap(so, jtx, units = "tpm")
