#!/bin/sh
# Jake Yeung
# rerun_analysis_pipeline.sh
#  
# 2017-03-17

script1="/home/yeung/projects/sleep_deprivation/scripts/round_two_analysis/kallisto_explore.R"
script2="/home/yeung/projects/sleep_deprivation/scripts/round_two_analysis/handle_techreps_assign_zt0_zt24.R"

Rscript $script1
Rscript $script2

