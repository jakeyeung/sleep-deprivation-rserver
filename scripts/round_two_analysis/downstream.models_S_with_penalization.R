# Jake Yeung
# Date of Creation: 2017-08-11
# File: ~/projects/sleep_deprivation/scripts/round_two_analysis/downstream.models_S_with_penalization.R
# Initialize downstream analysis of Process S with penalty

rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(ggplot2)
library(reshape2)
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/PlotFunctions.auxiliary.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/FitFunctions_Downstream.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/GetClockGenes.R")

do.linear <- FALSE


low.pass.filt.time <- rep(0.1, length = 780) * 1:780
low.pass.filt.time <- c(0, low.pass.filt.time)
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]


# HALF LIFE LIMITS
min.hl <- 1/3
max.hl <- 24
min.hl.mrna <- 1/3
max.hl.mrna <- 24

# inits -------------------------------------------------------------------

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}

dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)

# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=4/3600, max.time=78, filt.time=filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)


tstep <- 4/3600  # 4seconds in units of hour
low.pass.filt.time <- rep(0.1, length = 780) * 1:780

wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)


# Load Robjs --------------------------------------------------------------

# new penalized sleep fits
inf <- "/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit/fits.sleep.lambda.1000.dolinear.FALSE.maxhl.24.Robj"
# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit/fits.model.mixedaf.lambda.1000.dolinear.FALSE.minhl.0.33.Robj"
# inf <- "/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit/fits.model.mixed.lambda.1000.dolinear.FALSE.minhl.0.33.Robj"
load(inf, v=T)
if ("bic" %in% colnames(fits)){
  # remove bic column name and redo it
  if ("bic" %in% colnames(fits)){
    print("Removing bic colname")
    fits$bic <- NULL
    fits <- AddBICToDat(fits)
  }
}
fits.penal <- fits
fits.penal$model <- apply(fits.penal, 1, function(row) SelectBestModel(row, colnames(fits.penal)))

# original fits
fits <- LoadTwoFitsAndMerge()
fits$model <- apply(fits, 1, function(row) SelectBestModel(row, colnames(fits)))

sleep.params <- bind_rows(fits$fit.sleep)


# Plot fit.sleeps side by side -------------------------------------------

jgene <- "Dbp"
jgene <- "Parp6"
jgene <- "Arc"
jgene <- "Fos"
jgene <- "Egr2"

pred.orig <- PredictSleep(subset(fits, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.orig$model <- "Sleep.Orig"
pred.penal <- PredictSleep(subset(fits.penal, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.penal$model <- "Sleep.Penal"

# pred.orig <- PredictMixedAF(subset(fits, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.orig$model <- "Mixedaf.Orig"
# pred.penal <- PredictMixedAF(subset(fits.penal, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.penal$model <- "Mixedaf.Penal"

# pred.orig <- PredictMix(subset(fits, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.orig$model <- "Mix.Orig"
# pred.penal <- PredictMix(subset(fits.penal, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time); pred.penal$model <- "Mix.Penal"

pred.merged <- bind_rows(pred.orig, pred.penal)
dat.sub <- subset(dat.long.shift, gene == jgene)
fits.sub <- subset(fits, gene == jgene)

m1 <- PlotBestFit(dat.sub, subset(fits.penal, gene==jgene), filt.time, jgene, dat.pred=NA)
m2 <- PlotBestFit(dat.sub, fits.sub, filt.time, jgene, dat.pred = pred.merged)

multiplot(m2, m2, cols = 1)


# if fits have multiple sleep across lambdas, plot all of them ------------

# load("/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.dolinear.FALSE.ProcessSPenalized.lambda.0_to_100000_bugfix2_mclapply.testmode.Rcpp.mclapply.Robj", v=T)
# load("/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.dolinear.FALSE.ProcessSPenalized.lambda.0_to_100000_bugfix2_mclapply.testmode.Rcpp.mclapply.Robj", v=T)
inf <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.dolinear.FALSE.ProcessSPenalized.lambda.0_to_100000_bugfix2_mclapply_bad_genes.testmode.Rcpp.mclapply.Robj"
load(inf, v=T)

jlambdas <- c("", ".10", ".100", ".1000", ".10000", ".100000")

pdf("/home/yeung/projects/sleep_deprivation/plots/homeostatic_sleep/homeostatic_many_genes.pdf")

jgene <- "Egr2"
jgene <- "Plekhg4"
jgene <- "Baiap2l2"
jgene <- "Arc"

n.max <- ifelse(length(fits$gene) >= 20, 20, length(fits$gene))
for (jgene in fits$gene[1:n.max]){
  pred.merged <- data.frame()
  for (jlambda in jlambdas){
    jgrep <- paste0("^fit.sleep", jlambda, "$")
    jname <- paste0("sleep.", jlambda)
    pred.tmp <- PredictSleep(subset(fits, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time, cname.grep = jgrep, jmodel = jname)
    pred.merged <- bind_rows(pred.merged, pred.tmp)
  }
  m.all <- PlotBestFit(subset(dat.long.shift, gene == jgene), fits, filt.time, jgene, dat.pred = pred.merged)
  print(m.all)
}
dev.off()


# Plot sleep genes side by side -------------------------------------------



# 
# # Test with long mRNA half-life -------------------------------------------
# 
# load("/home/yeung/projects/sleep_deprivation/Robjs/from_vitalit/fits.sleep.sleepLP.circadian.flat.mix.step.maxAmpInf.tswitch.33.dolinear.FALSE.MixedAFOnlyBugFix.FixInitLowPass.Robj", v=T)
# 
# jgene <- "Arc"
# pred.longhl <-PredictSleep(subset(fits, gene == jgene), filt.time, wake.collapsed, low.pass.filt.time)
# 
# fits$model <- "sleep"
# PlotBestFit(subset(dat.long.shift, gene == jgene), subset(fits, gene == jgene), filt.time, jgene)
