# 2017-14-03
# Run MARA on round 2 analysis: downstream analysis
# copy from atacseq_analysis folder


rm(list=ls())

marasuffix <- ".multieeg_lowpass_logscale"

top.n <- 50
setwd("/home/yeung/projects/sleep_deprivation")

library(methods)
library(ggplot2)
library(reshape2)
library(dplyr)
library(preprocessCore)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/GetTFs.R")

# Load exprs data -------------------------------------------------------------------

# load data
inf.dat <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"
load(inf.dat, v=T); dat.orig <- subset(dat.long.cleaned, time <= 78)

fits <- MergeLowPassFits()

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

# Load EEG ----------------------------------------------------------------

## WAKE
min.time <- 0
max.time <- 78

# load EEG data (wake.collapsed, dat.eeg.plot)
max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# filt.time <- unique(dat.orig$time)
# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=tstep, max.time=max.time, filt.time=filt.time)
# dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=max.time, downsamp = downsamp)

tstep <- 4/3600
wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)



# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
weight.cutoff <- 0.6
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels
genes.lst[["genes_all"]] <- genes.all

# # Downstream --------------------------------------------------------------

# match TFs 
tfs <- GetTFs(split.commas = FALSE, get.mat.only = TRUE)

do.mean <- FALSE
do.center <- TRUE
gene.lab <- "genes_sleep"
gene.labs <- names(genes.lst)[which(names(genes.lst) != "flat")]

outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", marasuffix)
dir.create(outmain)
for (gene.lab in gene.labs){
  print(gene.lab)
  outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))
  
  suffix <- paste0("mean.", do.mean,  ".centered.", do.center,  marasuffix, "/sleep_deprivation_gene_exprs_all")
  act.dir <- file.path(outdir, suffix)
  act.f <- file.path(act.dir, "Activities")
  zscores.f <- file.path(act.dir, "Zscores")
  act.mat <- read.table(act.f, header = FALSE, sep = "\t")
  zscores <- read.table(zscores.f, header = FALSE, sep = "\t", col.names = c("motif", "zscore"))
  print(head(zscores))
  zscores <- zscores[order(zscores$zscore, decreasing = TRUE), ]
  
  # get colnames
  row.i <- which(colnames(dat.orig) == "time")
  dat.orig$sampname <- apply(dat.orig, 1, function(row) paste("ZT", sprintf("%02d", as.numeric(row[row.i])), sep = "_"))
  if (do.mean){
    dat.orig.mean <- dat.orig %>%
      group_by(gene, sampname) %>%
      summarise(exprs = mean(exprs))
  } else {
    dat.orig.mean <- dat.orig
    dat.orig.mean$sampname <- paste(dat.orig$sampname, dat.orig$samp, sep = "_")
  }
  
  M <- dcast(dat.orig.mean, gene ~ sampname, value.var = "exprs")
  colnames(act.mat) <- colnames(M)
  
  # set up act long
  act.long <- melt(act.mat, id.vars = "gene", variable.name = "sample", value.name = "exprs")
  act.long$time <- sapply(as.character(act.long$sample), function(s) as.numeric(strsplit(s, "_")[[1]][[2]]))
  act.long$samp <- sapply(as.character(act.long$sample), function(s) as.numeric(strsplit(s, "_")[[1]][[3]]))
  
  # plot top genes
  top.genes <- as.character(zscores$motif[1:top.n])
  
  plotdir <- paste0("plots/mara_outputs", marasuffix)
  dir.create(plotdir, showWarnings = FALSE)
  pdf(file.path(plotdir, paste0(gene.lab, "_motif_activities.pdf")))
  for (jgene in top.genes){
    m <- PlotPoints(subset(act.long, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3)  +  ylab("TF motif activity") +
      # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
      AddDarkPhases(alpha = 0.1) + 
      ggtitle(jgene)
    # plot gene if possible
    jgene.tf <- GetGenesFromMotifs(jgene, tfs)
    print(m)
    for (g in jgene.tf){
      # PlotFit(c(g), dat.orig, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
      PlotFit(c(g), dat.orig, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
    }
  }
  dev.off()
}
