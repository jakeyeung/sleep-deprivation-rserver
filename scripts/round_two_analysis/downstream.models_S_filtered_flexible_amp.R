# 2017-04-21
# Jake Yeung
# Downstream analysis of models after rerunning ~20 hours.


rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(dplyr)
library(ggplot2)
library(reshape2)
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/EegFunctions.R")

do.linear <- TRUE

low.pass.filt.time <- rep(0.1, length = 780) * 1:780
low.pass.filt.time <- c(0, low.pass.filt.time)
min.time <- 0
max.time <- 78
filt.time <- seq(min.time, max.time)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# inits -------------------------------------------------------------------

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

load("Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj", v=T)
dat.long.shift <- dat.long.cleaned; rm(dat.long.cleaned)
# do linear!
if (do.linear){
  dat.long.shift$exprs <- 2^dat.long.shift$exprs - 1
}

dat.long.shift <- dat.long.shift %>%
  group_by(gene, trtmnt) %>%
  arrange(time)

# ignore timepoints: 192 and 198 SD???
dat.long.shift <- subset(dat.long.shift, time <= max.time)


# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]

# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=4/3600, max.time=78, filt.time=filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)

use.merged <- TRUE
if (!use.merged){
  load("Robjs/dat.eeg.smooth.min_awake_5_min_intervals.Robj", verbose = T)
  
  dat.eeg$time.shift <- dat.eeg$time - 24
  jxlim <- c(-24, 78)
  dat.eeg <- subset(dat.eeg, time.shift >= jxlim[1] & time.shift <= jxlim[2])
  # add 72 as -24
  row.to.add <- subset(dat.eeg, time.shift == -24); row.to.add$time.shift <- 72
  dat.eeg <- rbind(dat.eeg, row.to.add)
  
  wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= max.time)
  # add 72 hours as 71.99889
  wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
  wake.df.dup$time.shift <- max.time
  wake.df <- rbind(wake.df, wake.df.dup)  
} else {
  load("Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", verbose = T)
  dat.eeg <- wake.df
  wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= max.time)
  # add 78 hours as last timepoint
  wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
  wake.df.dup$time.shift <- max.time
  wake.df <- rbind(wake.df, wake.df.dup) 
}


tstep <- 4/3600  # 4seconds in units of hour

# filt.time <- unique(dat.long.shift$time)
# filt.time <- seq(1, 72)
# filt.time <- seq(0, 78)  # for debugging purposes 
# low.pass.filt.time2 <- seq(from = 0.1, to = 78, by = 0.1)
low.pass.filt.time <- rep(0.1, length = 780) * 1:780
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)


# wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = filt.time)
# dat.eeg.plot <- wake.df[seq(1, nrow(wake.df), length.out = downsamp * nrow(wake.df)), ]


# Load Robjs --------------------------------------------------------------

inf <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.sleepLP.circadian.flat.mix.step.maxAmpInf.tswitch.33.Linear.MixedAFOnlyBugFix.Robj"
load(inf, verbose = T)
fits.mixedafs <- fits
fits.mixedafs <- dplyr::rename(fits.mixedafs, "bic.mixedaf" = bic)
inf <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.sleepLP.circadian.flat.mix.step.maxAmpInf.tswitch.33.Linear.Robj"
load(inf, verbose=T)
if (identical(fits$gene, fits.mixedafs$gene)){
  # discard mixedaf in fits, replace it with fits.mixedafs
  print("Replacing fits.mixedaf with new fits")
  fits$fit.mixedaf <- fits.mixedafs$fit.mixedaf
  fits$bic.mixedaf <- fits.mixedafs$bic.mixedaf
}

# Add best model
fits$model <- apply(fits, 1, function(row) SelectBestModel(row, colnames(fits)))

# show fits with gene and bic only
col.i <- !grepl("^fit.", colnames(fits))
subset(fits, gene == "Tef")[col.i]
subset(fits, gene == "Arntl")[col.i]
subset(fits, gene == "Egr2")[col.i]
subset(fits, gene == "Per2")[col.i]
subset(fits, gene == "Per3")[col.i]
subset(fits, gene == "Fmo2")[col.i]
subset(fits, gene == "Srf")[col.i]


# Plot hits ---------------------------------------------------------------

PlotFit(c("Tef"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
PlotFit(c("Arntl"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)

PlotFit(c("Hlf"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
PlotFit(c("Egr2"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)


PlotFit(c("Fmo2"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
PlotFit(c("Arntl"), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)


# Explore models ----------------------------------------------------------

fits.sum <- fits %>%
  group_by(model) %>%
  summarise(count = length(gene)) %>%
  mutate(fraction = count / nrow(fits))


# Plot top models ---------------------------------------------------------

# top ampfree.step
subset(fits, model == "ampfree.step") %>% arrange(bic.ampfree.step)
subset(fits, model == "ampfree.step") %>% arrange(bic.ampfree.step)

bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))


fits.best <- fits.bic %>%
  group_by(gene) %>%
  filter(weight == max(weight))




# Init plotting -----------------------------------------------------------


top.n <- 75
maindir <- "/home/yeung/projects/sleep_deprivation/plots/model_selection_round2/model_selection_summary.LinearWithMrna"
dir.create(maindir)

# Show best sleep  --------------------------------------------------------

jmodel <- "sleep"
params.sleep <- BindParameters(fits, modelname = jmodel, fit.cname = "fit.sleep")

params.sleep$min.tau <- as.numeric(apply(params.sleep, 1, function(row) min(row[[3]], row[[5]])))
params.sleep$UL.delta <- params.sleep$U - params.sleep$L

UL.delta.i <- which(colnames(params.sleep) == "UL.delta")
gene.delta.i <- which(colnames(params.sleep) == "gene")
params.sleep$genelab <- apply(params.sleep, 1, function(row, UL.delta.i, gene.delta.i){
  UL.delt <- as.numeric(row[[UL.delta.i]])
  ifelse(abs(UL.delt) > 1.5, yes = row[[gene.delta.i]], no = NA)
}, UL.delta.i, gene.delta.i)

# sort by abs(max-min)
params.sleep <- params.sleep %>% 
  arrange(desc(abs(UL.delta)))

genes.sleep <- params.sleep$gene[1:top.n]

pdf(paste0(maindir, "/model_", jmodel, "_by_params.pdf"))
for (g in genes.sleep){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()

# Show best circadian -----------------------------------------------------

jmodel <- "circadian"
params.circ <- BindParameters(fits, modelname = jmodel, fit.cname = "fit.circadian")

# sort by amp
params.circ <- params.circ %>%
  arrange(desc(amp)) 

genes.circ <- params.circ$gene[1:top.n]

pdf(paste0(maindir, "/model_", jmodel, "_by_params.pdf"))
for (g in genes.circ){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()

# Show best circ-sleep mix ------------------------------------------------

jmodel <- "mix"
params.mixed <- BindParameters(fits, modelname = jmodel, fit.cname = "fit.mix")

tau.s.i <- which(colnames(params.mixed) == "tau.s")
tau.w.i <- which(colnames(params.mixed) == "tau.w")
params.mixed$min.tau <- as.numeric(apply(params.mixed, 1, function(row) min(row[[tau.s.i]], row[[tau.w.i]])))
params.mixed$UL.delta <- params.mixed$U - params.mixed$L

gene.delta.i.m <- which(colnames(params.mixed) == "gene")
UL.delta.i.m <- which(colnames(params.mixed) == "UL.delta")

params.mixed$genelab <- apply(params.mixed, 1, function(row, UL.delta.i.m, gene.delta.i.m){
  UL.delt <- as.numeric(row[[UL.delta.i.m]])
  ifelse(abs(UL.delt) > 1, yes = row[[gene.delta.i.m]], no = NA)
}, UL.delta.i.m, gene.delta.i.m)

# add amp and phase
params.mixed$amp = CosSineToAmpPhase(params.mixed$cos.part, params.mixed$sin.part, T.period = 24)$amp
params.mixed$phase = CosSineToAmpPhase(params.mixed$cos.part, params.mixed$sin.part, T.period = 24)$phase

# sort by abs min max
params.mixed <- params.mixed %>% arrange(desc(abs(UL.delta)))
genes.mixed <- params.mixed$gene[1:top.n]

pdf(paste0(maindir, "/model_", jmodel, "_by_params.pdf"))
for (g in genes.mixed){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()


# Show circadian free -----------------------------------------------------

jmodel <- "ampfree.step"
params.circstep <- BindParameters(fits, modelname = jmodel, fit.cname = "fit.ampfree.step")

params.circstep$amp.final <- params.circstep$amp * params.circstep$amp.param 

m1 <- PlotAmpPhase(subset(params.circstep), ampscale = 2, constant.amp = 5)

gene.i.m <- which(colnames(params.circstep) == "gene")
amp.i.m <- which(colnames(params.circstep) == "amp")
amp2.i.m <- which(colnames(params.circstep) == "amp.final")

params.circstep$genelab <- apply(params.circstep, 1, function(row, amp.i.m, amp2.i.m, gene.i.m){
  amp1 <- as.numeric(row[[amp.i.m]])
  amp2 <- as.numeric(row[[amp2.i.m]])
  gene <- row[[gene.i.m]]
  ifelse(amp1 > 0.45 | amp2 > 0.3, yes = gene, no = NA)
}, amp.i.m, amp2.i.m, gene.i.m)

# sort by change in amplitude
params.circstep <- params.circstep %>% arrange(desc(amp -amp.final))
genes.circstep <- params.circstep$gene[1:top.n]

pdf(paste0(maindir, "/model_", jmodel, "_by_params.pdf"))
for (g in genes.circstep){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()


# Show circfree-sleep mix -------------------------------------------------


jmodel <- "mixedaf"
params.mixedaf <- BindParameters(fits, modelname = jmodel, fit.cname = "fit.mixedaf")

params.mixedaf$amp.final <- params.mixedaf$amp * params.mixedaf$amp.param 

tau.s.i <- which(colnames(params.mixedaf) == "tau.s")
tau.w.i <- which(colnames(params.mixedaf) == "tau.w")
params.mixedaf$min.tau <- as.numeric(apply(params.mixedaf, 1, function(row) min(row[[tau.s.i]], row[[tau.w.i]])))
params.mixedaf$UL.delta <- params.mixedaf$U - params.mixedaf$L

gene.i.m <- which(colnames(params.mixedaf) == "gene")
amp.i.m <- which(colnames(params.mixedaf) == "amp")
amp2.i.m <- which(colnames(params.mixedaf) == "amp.final")

params.mixedaf$genelab <- apply(params.mixedaf, 1, function(row, amp.i.m, amp2.i.m, gene.i.m){
  amp1 <- as.numeric(row[[amp.i.m]])
  amp2 <- as.numeric(row[[amp2.i.m]])
  gene <- row[[gene.i.m]]
  ifelse(amp1 > 0.3 | amp2 > 0.2, yes = gene, no = NA)
}, amp.i.m, amp2.i.m, gene.i.m)

# sort by change in ampltiude 
params.mixedaf.byamp <- params.mixedaf %>% arrange(desc(abs(amp.final - amp)))
genes.mixedaf.byamp <- params.mixedaf.byamp$gene[1:top.n]

# sort by abs(delta UL)
params.mixedaf.byUL <- params.mixedaf %>% arrange(desc(abs(UL.delta)))
genes.mixedaf.byUL <- params.mixedaf.byUL$gene[1:top.n]

pdf(paste0(maindir, "/model_", jmodel, "_by_UL.pdf"))
for (g in genes.mixedaf.byUL){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()

pdf(paste0(maindir, "/model_", jmodel, "_by_ampdiff.pdf"))
for (g in genes.mixedaf.byamp){
  PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time)
}
dev.off()

