# 2017-14-03
# Run MARA on round 2 analysis
# copy from atacseq_analysis folder


rm(list=ls())

setwd("/home/yeung/projects/sleep_deprivation")

library(methods)
library(ggplot2)
library(reshape2)
library(dplyr)
library(preprocessCore)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")



do.center <- TRUE
# center
do.mean <- FALSE
suffix <- ".multieeg_lowpass_logscale_merge_models_distal_peaks"

# Load -------------------------------------------------------------------

# load data
inf.dat <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"
load(inf.dat, v=T); dat.orig <- subset(dat.long.cleaned, time <= 78)

# inf.fits <- "/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.circadian.flat.mix.step.maxAmpInf.tswitch.33.Robj"
# load(inf.fits, v=T)  # fits
# fits$model <- apply(fits, 1, function(row) SelectBestModel(row, colnames(fits)))
fits <- MergeLowPassFits()

col.i <- !grepl("^fit.", colnames(fits))
bic.cols <- colnames(fits)[grepl("^bic.", colnames(fits))]
fits.bic <- melt(fits[, col.i], id.vars = c("gene", "model"), measure.vars = bic.cols, variable.name = "model.names", value.name = "bic") %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

# Get gene list -----------------------------------------------------------

genes.all <- as.character(unique(fits$gene))
weight.cutoff <- 0.6
jmodels <- unique(fits.bic$model)
genes.lst <- lapply(jmodels, function(m){
  genes <- as.character(unique(subset(fits.bic, model == m & weight > weight.cutoff)$gene))
})
names(genes.lst) <- jmodels
genes.lst[["genes_all"]] <- genes.all

# Write gene expression (ALL GENES) ---------------------------------------

row.i <- which(colnames(dat.orig) == "time")
dat.orig$sampname <- apply(dat.orig, 1, function(row) paste("ZT", sprintf("%02d", as.numeric(row[row.i])), sep = "_"))

if (do.mean){
  dat.orig.mean <- dat.orig %>%
    group_by(gene, sampname) %>%
    summarise(exprs = mean(exprs))
} else {
  dat.orig.mean <- dat.orig
  dat.orig.mean$sampname <- paste(dat.orig$sampname, dat.orig$samp, sep = "_")
}


if (do.center){
  dat.orig.mean <- dat.orig.mean %>%
    group_by(gene) %>%
    mutate(exprs = exprs - mean(exprs))
}

M <- dcast(dat.orig.mean, gene ~ sampname, value.var = "exprs")

E.dir <- file.path("/home/yeung/data/sleep_deprivation/gene_exprs_for_mara", paste0("mean.", do.mean, ".centered.", do.center, suffix))
dir.create(E.dir, showWarnings = FALSE)
E.out <- file.path(E.dir, paste0("sleep_deprivation_gene_exprs_all", ".mean.", do.mean, ".centered.", do.center, ".mat"))

# rename gene to Gene.ID
colnames(M)[[1]] <- "Gene.ID"
if (!file.exists(E.out)){
  write.table(M, E.out, append = FALSE, quote = FALSE, row.names = FALSE, col.names = TRUE, sep = "\t")
} else {
  print("File exists, skipping writing of gene expression matrix")
}

# Write sitecounts matrix (subset of genes) -------------------------------

N.dir <- paste0("/home/yeung/data/sleep_deprivation/sitecount_matrices_for_mara", suffix)
dir.create(N.dir)
# Npath <- "/home/yeung/projects/tissue-specificity/data/sitecounts/motevo/sitecount_matrix_geneids"
# N <- read.table(Npath, header=TRUE)

load("/home/yeung/projects/sleep_deprivation/Robjs/N.long.filt.peaks.Robj", v=T)
# make N from long
atacseq.peaks <- unique(explore.cutoff$peak)
atacseq.genes <- unique(explore.cutoff$gene)
# merge across peaks 
N.sub <- subset(N.long.filt, gene %in% atacseq.genes & peak %in% atacseq.peaks) %>%
  group_by(gene, motif) %>%
  summarise(sitecount = sum(sitecount))
N <- dcast(N.sub, formula = "gene ~ motif", value.var = "sitecount", fill = 0)
N <- dplyr::rename(N, "Gene.ID" = gene)

# merge certain lists



genes.lst <- MergeLists(genes.lst, "ampfree.step", "mixedaf")
genes.lst <- MergeLists(genes.lst, "circadian", "mix")
genes.lst <- RemoveLists(genes.lst, "ampfree.step")
genes.lst <- RemoveLists(genes.lst, "mixedaf")
genes.lst <- RemoveLists(genes.lst, "circadian")
genes.lst <- RemoveLists(genes.lst, "mix")


# gene.lab <- "genes_all"
gene.labs <- names(genes.lst)

for (gene.lab in gene.labs){
  print(paste("Running mara for", gene.lab))
  N.sub <- subset(N, Gene.ID %in% genes.lst[[gene.lab]])
  N.out <- file.path(N.dir, paste0("sitecounts_swiss_regulon_promoters_only.", gene.lab, suffix, ".mat"))
  
  if (!file.exists(N.out)){
    write.table(N.sub, file = N.out, append = FALSE, quote = FALSE, sep = "\t", col.names = TRUE, row.names = FALSE)
  } else {
    print("Sitecounts exists, skipping")
  }
  
  
  # Run MARA ----------------------------------------------------------------
  
  marascript <- "/home/yeung/projects/sleep_deprivation/scripts/shellscripts/run_mara_sleep_deprivation.sh"
  outmain <- paste0("/home/yeung/data/sleep_deprivation/mara_outputs", suffix)
  dir.create(outmain, showWarnings = FALSE)
  outdir <- file.path(outmain, paste0(gene.lab, ".mean.", do.mean, ".center.", do.center))
  # do not create outdir, because MARA will not run on an already-created directory 
  cmd <- paste("bash", marascript, N.out, outdir, E.dir)
  
  system(cmd)
}
