---
title: 'Recovery and ATAC-Seq footprint analysis'
author: "Jake Yeung"
date: "2017-07-03"
output: 
  beamer_presentation:
    template: /home/yeung/.pandoc/template/rmd-template/default.tex
    toc: false
    theme: "Madrid"
    colortheme: "default"
    fonttheme: "default"
    incremental: false
    slide_level: 2
---

# Introduction
```{r init, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_chunk$set(fig.width=18, fig.height=10,
                      echo=FALSE, warning=FALSE, message=FALSE)
setwd("~/projects/sleep_deprivation")
library(dplyr)
library(ggplot2)
library(wordcloud)
library(reshape2)


source("scripts/functions/RoundTwoPCAScripts.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/FitFunctions.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/TimeShiftFunctions.R")
source("scripts/functions/WellingtonFunctions.R")

TtestOnMeans <- function(jsub, comp.days, ref.day, jzt){
  t.test.lst <- lapply(comp.days, function(x, jzt){
    days.sub <- c(ref.day, x)
    jj <- subset(jsub, day %in% days.sub)
    jtest <- t.test(exprs ~ day, data = jj)
    # make output in dataframe
    if (missing(jzt)){
      jzt <- unique(jsub$ZT)
    }
    if (length(jzt) != 1){
      warning(paste("ZT should be single time point", paste(jzt, collapse = ",")))
    }
    outdat <- data.frame(ref.day = ref.day, comp.day = x, ZT = jzt, p.value = jtest$p.value, ref.mean = jtest$estimate[[1]], comp.mean = jtest$estimate[[2]])
    return(outdat)
  }, jzt)
  return(bind_rows(t.test.lst))
}


load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.kallisto.with_batch_effect.Robj"); dat.orig <- dat.long.shift
load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.kallisto.combat.mean_only.FALSE.exprs_cutoff.2.5.Robj"); dat.after <- dat.long.c
load("/home/yeung/projects/sleep_deprivation/Robjs/combat/dat.long.cleaned.techmerged_zt24assigned.Robj"); dat.long.shift <- subset(dat.long.cleaned, time <= 78)
# load("/home/yeung/projects/sleep_deprivation/Robjs/combat/fits.sleep.circadian.flat.mix.step.maxAmpInf.tswitch.33.Robj")  # fits
# fits$model <- apply(fits, 1, function(row) SelectBestModel(row, colnames(fits)))
fits <- MergeLowPassFits()

max.time <- 78
downsamp <- 0.005
tstep <- 4/3600  # 4seconds in units of hour

# filt.time <- unique(dat.long.shift$time)
min.time <- 0
max.time <- 78
filt.time <- seq(0, 78)
filt.time <- filt.time[!filt.time %in% c(31, 32)]
low.pass.filt.time <- rep(0.1, length = 780) * 1:780
# wake.collapsed <- GetWakeCollapsed(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", tstep=tstep, max.time=max.time, filt.time=filt.time)
# dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=max.time, downsamp = downsamp)

wake.df <- GetWakeCollapsed(tstep = tstep, max.time = max.time, return.wake.df.only = TRUE)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = low.pass.filt.time)
dat.eeg.plot <- GetSubsampledEeg(inf="Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj", max.time=78)

load("/home/yeung/projects/sleep_deprivation/Robjs/atacseq.qnorm.Robj")

dat.long.cleaned$ZT <- sapply(dat.long.cleaned$time, TimeToZT)
dat.long.cleaned$day <- sapply(dat.long.cleaned$time, TimeToDay)

params.circstep <- BindParameters(fits, modelname = "ampfree.step", fit.cname = "fit.ampfree.step")
jgenes.ampfree <- params.circstep$gene

# genomewide recovery 

days.to.test <- unique(dat.long.cleaned$day)[which(unique(dat.long.cleaned$day) != 0)]

t.test.out.zt6 <- subset(dat.long.cleaned, ZT == 6 & gene %in% jgenes.ampfree) %>%
  group_by(gene) %>%
  do(TtestOnMeans(., days.to.test, ref.day = 0, jzt = 6)) %>%
  arrange(p.value)
# t.test.out <- TtestOnMeans(subset(dat.long.cleaned, ZT == 6 & gene == jgene), days.to.test, 0, 6)



jsub.shift <- subset(dat.long.cleaned, ZT == 0 & gene %in% jgenes.ampfree)
jsub.shift$time[jsub.shift$time == 24] <- 0  # set 0 to 24 to increase the size of t-test

t.test.out.zt0 <- jsub.shift %>%
  group_by(gene) %>%
  do(TtestOnMeans(., days.to.test, ref.day = 0, jzt = 0)) %>%
  arrange(p.value)

load("Robjs/wellington_output.Robj")

jsub <- subset(well.long, dist < 1500) %>%
  group_by(samp, gene, time, sleep) %>%
  summarise(exprs = -1 * mean(exprs, na.rm = TRUE))
jsub$samp <- 1
jsub$batch <- 1

# inf <- "/home/yeung/data/sleep_deprivation/wellington_analysis/all_ZTs_summary_rawWellington.sorted.wgenes.filt.long.mat"
# inf.header <- "/home/yeung/data/sleep_deprivation/wellington_analysis/all_ZTs_summary_rawWellington.sorted.wgenes.filt.txt.header"
# 
# cnames <- unlist(read.table(inf.header, stringsAsFactors = FALSE), use.names = FALSE)
# well.mat <- read.table(inf, header = FALSE, sep = "\t", col.names = cnames)
# 
# ids <- cnames[!grepl("ZT", cnames)]
# measures <- cnames[grepl("ZT", cnames)]
# 
# well.long <- melt(well.mat, id.vars = ids, measure.vars = measures, variable.name = "samp", value.name = "exprs")
# 
# well.long$time <- sapply(as.character(well.long$samp), TimeFromSamp)
# well.long$sleep <- sapply(as.character(well.long$samp), SleepFromSamp)


```

## Have damped oscillations recovered after 7 days?

```{r tef}
jgene <- "Tef"
PlotFit(c(jgene), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, low.pass.filter.times = low.pass.filt.time)
```


## Jund is a recovered gene

```{r jund}
jgene <- "Jund"
pval <- signif(subset(t.test.out.zt6, gene == jgene & comp.day == 8)$p.value, 2)
m.gene.clean2 <- PlotPoints(subset(dat.long.cleaned, gene == jgene), 
                            expand.zero = TRUE, 
                            colour.pts = TRUE, 
                            dotsize = 3.5, 
                            has.batch = TRUE,
                            shift.day.7.to.4 = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste(jgene, "Pval:", pval)) + 
  theme_bw(24)
print(m.gene.clean2)
```

## Nonrecovered gene: Fbxo3

```{r Klhl9}
# # jgene <- "Tef"
# jgene <- "Xpot"
# jgene <- "Tomm22"
# jgene <- "Slc25a12"
# jgene <- "Hsf2"
# jgene <- "Sox4"
# jgene <- "Klhl9"
jgene <- "Fbxo3"
pval <- signif(subset(t.test.out.zt6, gene == jgene & comp.day == 8)$p.value, 2)

m.gene.clean2 <- PlotPoints(subset(dat.long.cleaned, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE, shift.day.7.to.4 = TRUE)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) +
  ggtitle(paste(jgene, "Pval:", pval)) + 
  theme_bw(24) 
print(m.gene.clean2)

```

## Footprint Analysis: Wellington

Show some UCSC browser 


## Correlations between RNA-Seq and Wellington 

```{r rnaseq_well}

# jgene <- "Egr2"
# 
# well.sub <- subset(well.long, gene == jgene & dist < 1500) %>%
#   group_by(samp, gene, time, sleep) %>%
#   summarise(exprs = -1 * mean(exprs, na.rm = TRUE))
# well.sub$samp <- 1
# well.sub$batch <- 1
# 
# # plot points above
# gene.sub <- subset(dat.long.shift, gene == jgene)
# m.gene <- PlotPoints(gene.sub, 
#                             expand.zero = TRUE, 
#                             colour.pts = TRUE, 
#                             dotsize = 3.5, 
#                             has.batch = TRUE)  +  ylab("log2 mRNA accumulation") +
#   AddDarkPhases(alpha = 0.1) +
#   ggtitle(jgene)
# m.well <- PlotPoints(well.sub, expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, has.batch = TRUE)  +  ylab("-log10(p-value)") +
#   # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
#   AddDarkPhases(alpha = 0.1) +
#   ggtitle(paste("Wellington:", jgene))
# multiplot(m.gene, m.well)
```