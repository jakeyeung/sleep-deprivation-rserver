---
title: 'Transcriptional regulators underlying sleep-wake-driven gene expression'
author: "Jake Yeung"
date: "2016-10-10"
output: 
  beamer_presentation:
    template: /home/yeung/.pandoc/template/rmd-template/default.tex
    toc: false
    theme: "Madrid"
    colortheme: "default"
    fonttheme: "default"
    incremental: false
    slide_level: 2
---

# Introduction
```{r init, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_chunk$set(fig.width=18, fig.height=10,
                      echo=FALSE, warning=FALSE, message=FALSE)
setwd("~/projects/sleep_deprivation")

library(dplyr)
library(ggplot2)
library(reshape2)
library(stringr)
library(hash)
library(reshape2)
library(gridExtra)
library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/StringFunctions.R")
source("scripts/functions/PhaseFunctions.R")
source("scripts/functions/GetClockGenes.R")
source("scripts/functions/MaraDownstream.R")
source("scripts/functions/GetTFs.R")

## HARD CONSTANTS
downsamp <- 0.01  # for eeg

## LOAD GENE EXPRS
load("Robjs/dat.long.htseq.0to78hrs.reps.Robj")
dat.orig <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)
dat.long.shift <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = FALSE, removezt78 = FALSE)

## LOAD EEG: all mice
rem.wake <- TRUE
inf <- paste0("/home/yeung/projects/sleep_deprivation/Robjs/eeg_data_merged_72_and_78_hr_mice/dat.eeg.72h.78h.merged.mice.remiswake.", rem.wake, ".Robj")
load(inf, v=T)
dat.eeg.all <- ShiftEeg(dat.eeg.all)
dat.eeg.all$rem.wake <- rem.wake
dat.eeg.all.plot <- dat.eeg.all %>%
  group_by(mouse, rem.wake) %>%
  do(DownSample(., downsamp = downsamp))

## LOAD EEG: merged
min.time <- 0
max.time <- 78
inf.wake.df <- "/home/yeung/projects/sleep_deprivation/Robjs/eeg_data_merged_72_and_78_hr_mice/wake.df.method.mode.Robj"
load(inf.wake.df, v=T)
wake.df <- subset(wake.df, time.shift >= 0 & time.shift <= max.time)
# add 78 hours as last timepoint
wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
wake.df.dup$time.shift <- max.time
wake.df <- rbind(wake.df, wake.df.dup) 
tstep <- 4 / 3600
filt.time <- seq(min.time, max.time)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = filt.time)
dat.eeg.plot <- wake.df[seq(1, nrow(wake.df), length.out = downsamp * nrow(wake.df)), ]

## GET FITS
inf.fits <- "/home/yeung/projects/sleep_deprivation/Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.noIntnoWeight.pseudo0.usemerged.TRUE.zt0tozt24.FALSE.Robj"
load(inf.fits, v=T)
cnames <- c("sleep", "circadian", "flat", "mixed")
s.i <- which(colnames(fits) == "bic.sleep")
c.i <- which(colnames(fits) == "bic.circadian")
f.i <- which(colnames(fits) == "bic.flat")
m.i <- which(colnames(fits) == "bic.mix")
fits$model <- apply(fits, 1, function(row){
  bic.s <- as.numeric(row[[s.i]])
  bic.c <- as.numeric(row[[c.i]])
  bic.f <- as.numeric(row[[f.i]])
  bic.m <- as.numeric(row[[m.i]])
  bic.vec <- c(bic.s, bic.c, bic.f, bic.m)
  return(cnames[[which.min(bic.vec)]])
})
fits.bic <- melt(data = subset(fits, select = c(-fit.circadian, -fit.flat)), 
                 id.vars = c("gene", "model", "fit.sleep", "fit.mix"), 
                 measure.vars = c("bic.sleep", "bic.circadian", "bic.flat", "bic.mix"), 
                 variable.name = "bic.model", 
                 value.name = "bic")
fits.bic <- fits.bic %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

## Get circadian fits
load("/home/yeung/projects/sleep_deprivation/Robjs/dat.fits.weighted_and_nonweighted.Robj", v=T)

## Load circadian and sleep regulators (MARA)
suffix <- "mean.FALSE.centered.TRUE/sleep_deprivation_gene_exprs_all"
circ.maradir <- file.path("/home/yeung/data/sleep_deprivation/mara_outputs.multieeg/genes_circadian.mean.FALSE.center.TRUE", suffix)
sleep.maradir <- file.path("/home/yeung/data/sleep_deprivation/mara_outputs.multieeg/genes_sleep.mean.FALSE.center.TRUE", suffix)
circ.mara <- LoadMaraOutput(circ.maradir)
sleep.mara <- LoadMaraOutput(sleep.maradir)
tfs <- GetTFs(split.commas = FALSE, get.mat.only = TRUE)

## LOAD atacseq promoters
load("/home/yeung/projects/sleep_deprivation/Robjs/atacseq_ttests/atacseq_promoters.Robj")

## Define my genes
sleep.genes <- unique(as.character(subset(fits.bic, model == "sleep" & weight > 0.95)$gene))
sleep.genes.all <- unique(as.character(subset(fits.bic, model == "sleep")$gene))
circ.genes <- unique(as.character(subset(fits.bic, model == "circadian" & weight > 0.95)$gene))
circ.genes.all <- unique(as.character(subset(fits.bic, model == "circadian")$gene))
mixed.genes <- unique(as.character(subset(fits.bic, model == "mixed" & weight > 0)$gene))
flat.genes <- unique(as.character(subset(fits.bic, model == "flat")$gene))

## LOAD database
# inf <- "/home/shared/sql_dbs/atacseq_signal_windows500.long.motifindexed.sqlite3"
# atacseq.tbl <- LoadDatabase(inf)
inf.ex <- "/home/yeung/projects/sleep_deprivation/Robjs/atacseq_ttests/exploresub_maxdist.100.log2.TRUE.ttest_with_dist_zt.3_v_zt27.Robj"
load(inf.ex, v=T)
```

## Experimental data

- RNA-Seq (3-4 mice per time point, from ZT0 to ZT78, SD between ZT24-ZT30)
- ATAC-Seq (3-4 mice per time point, from ZT0 to ZT78, SD between ZT24-ZT30)
- EEG signal (12+6 mice from ZT0 to ZT78)

# Dynamics of mRNA accumulation 

## Classify genes into 4 models

- Flat (k=1) mRNA abundance
- Circadian (k=3): mRNA abundance oscillates in a circadian manner
- Sleep-wake driven (k=5): sleep-wake history can model dynamics of gene expression
- Mixed (k=7): Combination of sleep-wake driven and circadian signals models dynamics of gene expression

Penalize model complexity using Bayesian Information Criterion.

## Classify genes as sleep-wake-driven, diurnal, or flat

Process S (sleep): 5 parameters $E_0, U, L, \tau_w, \tau_s$
\[ 
\begin{aligned}
E_{t=0} &= E_0 \\ 
E_{t+1} &= 
    \begin{cases} 
      U - (U - E_t) \exp(\frac{-\Delta t}{\tau_w}) & \text{wake} \\
      L + (E_t - L) \exp(\frac{-\Delta t}{\tau_s}) & \text{sleep}
   \end{cases}
\end{aligned}
\]

Process C (circadian): 3 parameters $\mu, a, b$
\[
E(t) = \mu + a \cos(\omega t) + b \sin(\omega t)
\]

Null model: $\mu$
\[
E(t) = \mu
\]

## Genes are mostly circadian or sleep, ~200 are a mixture between both

```{r summary}
fits$model <- factor(as.character(fits$model), levels = c("flat", "sleep", "circadian", "mixed"))
ggplot(fits, aes(x = model)) + geom_bar() + theme_bw(24) + xlab("") + ylab("# genes") + 
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

## Wake-activated gene: Fos
```{r fos}
g <- "Fos"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Wake-activated gene: Egr2

```{r wake2}
g <- "Egr2"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Sleep-activated gene: Qsox1

```{r qsox1}
g <- "Qsox1"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## PCA of genes in sleep-wake driven model shows separation by sleep-wake history

```{r pca_sleep}
dat.nsd <- subset(dat.long.shift, gene %in% sleep.genes.all)
dat.nsd.mat <- dcast(dat.nsd, gene ~ time + samp + trtmnt, value.var = "exprs")
# add ZT to colnames
rownames(dat.nsd.mat) <- dat.nsd.mat$gene; dat.nsd.mat$gene <- NULL
colnames(dat.nsd.mat) <- sapply(colnames(dat.nsd.mat), function(s) strsplit(s, "_")[[1]][[1]])
PlotPcs(dat.nsd.mat, jcex = 2)
```

## Summary of sleep-wake parameters: steady state values

```{r sleepwake_summary}
sleep.hits.broad <- as.character(subset(fits.bic, model == "sleep" & weight > 0)$gene)
fits.sub <- subset(fits, model == "sleep" & gene %in% sleep.hits.broad) 

ReturnFitSleep <- function(dat){
  return(dat$fit.sleep[[1]])
}
fits.sub1 <- fits.sub %>%
  group_by(gene) %>%
  do(ReturnFitSleep(.))

sleep.hits <- as.character(subset(fits.bic, model == "sleep" & weight > 0.96)$gene)
sleep.hash <- hash(sleep.hits, sleep.hits)

fits.sub1$gene.lab <- sapply(as.character(fits.sub1$gene), function(g){
  gg <- sleep.hash[[g]]
  if (is.null(gg)){
    gg <- NA
  }
  return(gg)
})
ggplot(fits.sub1, aes(x = U, y = L, label = gene.lab)) + geom_point(alpha = 0.5) + geom_text() + theme_bw(24) + 
  xlab("Wake Steady State [log2 exprs]") + ylab("Sleep Steady State [log2 exprs]") + 
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank())
```

## Summary of sleep-wake model parameters: half-lives

```{r sleepwake_summary_hl}
ggplot(fits.sub1, aes(x = tau.w * log(2), y = tau.s * log(2))) + 
  geom_point(alpha = 0.2) + theme_bw(24) +
  xlab("Half-life wake [h]") + ylab("Half-life sleep [h]") + 
  scale_x_log10() + scale_y_log10() +
  theme(aspect.ratio=1, panel.grid.major = element_blank(), panel.grid.minor = element_blank())
  
```

## Circadian example: Fmo2
```{r fmo2}
g <- "Fmo2"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Circadian example: Sox2
```{r sox2}
g <- "Sox2"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## PCA of circadian module shows mostly clustering by ZT time, except for ZT27 samples
```{r pca_circadian}
dat.nsd <- subset(dat.long.shift, gene %in% circ.genes.all)
dat.nsd.mat <- dcast(dat.nsd, gene ~ time + samp + trtmnt, value.var = "exprs")
# add ZT to colnames
rownames(dat.nsd.mat) <- dat.nsd.mat$gene; dat.nsd.mat$gene <- NULL
# rename colnames to show only ZT
colnames(dat.nsd.mat) <- sapply(colnames(dat.nsd.mat), function(s) strsplit(s, "_")[[1]][[1]])
PlotPcs(dat.nsd.mat, jcex = 2)
```

## Some circadian genes may still be effected by sleep deprivation treatment
```{r circ_subtle}
g <- "Net1"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Amplitude and phase of top hits

```{r ampphase}
jgenes <- as.character(subset(fits, model == "circadian")$gene)
dat.fits.sub <- subset(dat.fits, gene %in% jgenes)
PlotAmpPhase(dat.fits.sub, constant.amp = 5)
```

## Mixed example: Arntl

```{r mixed1}
g <- "Arntl"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Mixed example: Nfil3

```{r mixed2}
g <- "Nfil3"
PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Genes in mixed model show a larger separation between ZT30 and ZT27 compared to sleep model
```{r pca_mixed}
dat.nsd <- subset(dat.long.shift, gene %in% mixed.genes)
dat.nsd.mat <- dcast(dat.nsd, gene ~ time + samp + trtmnt, value.var = "exprs")
# add ZT to colnames
rownames(dat.nsd.mat) <- dat.nsd.mat$gene; dat.nsd.mat$gene <- NULL
# rename colnames to show only ZT
colnames(dat.nsd.mat) <- sapply(colnames(dat.nsd.mat), function(s) strsplit(s, "_")[[1]][[1]])
PlotPcs(dat.nsd.mat, jcex = 2)
```

## Sleep genes for comparison
```{r pca_sleep2}
dat.nsd <- subset(dat.long.shift, gene %in% sleep.genes.all)
dat.nsd.mat <- dcast(dat.nsd, gene ~ time + samp + trtmnt, value.var = "exprs")
# add ZT to colnames
rownames(dat.nsd.mat) <- dat.nsd.mat$gene; dat.nsd.mat$gene <- NULL
# rename colnames to show only ZT
colnames(dat.nsd.mat) <- sapply(colnames(dat.nsd.mat), function(s) strsplit(s, "_")[[1]][[1]])
PlotPcs(dat.nsd.mat, jcex = 2)
```

# Dynamics of ATAC-Seq signal

## ATAC-Seq signal at promoters do not show clear temporal separation

```{r promoter-pca}
mat.qnorm.centered <- t(scale(t(mat.qnorm), center = TRUE, scale = TRUE))
# remove bad genes
bad.genes <- unique(rownames(which(is.na(mat.qnorm.centered), arr.ind = TRUE)))
mat.qnorm.centered <- mat.qnorm.centered[! rownames(mat.qnorm.centered) %in% bad.genes, ]

# filter for sleep genes

# mat.qnorm.centered.sleep <- mat.qnorm.centered[rownames(mat.qnorm.centered) %in% sleep.genes, ]
# mat.qnorm.centered.circ <- mat.qnorm.centered[rownames(mat.qnorm.centered) %in% circ.genes, ]
# mat.qnorm.centered.mixed <- mat.qnorm.centered[rownames(mat.qnorm.centered) %in% mixed.genes, ]
# mat.qnorm.centered.flat <- mat.qnorm.centered[rownames(mat.qnorm.centered) %in% flat.genes, ]
mat.qnorm.centered.nonflat <- as.matrix(mat.qnorm.centered[!rownames(mat.qnorm.centered) %in% flat.genes, ])
colnames(mat.qnorm.centered.nonflat) <- sapply(colnames(mat.qnorm.centered.nonflat), function(s) gsub("ZT", "", strsplit(s, "_")[[1]][[1]]))

PlotPcs(mat.qnorm.centered.nonflat, jcex = 2)

# mat.pca <- prcomp(mat.qnorm.centered.mixed, center = FALSE, scale. = FALSE)
# mat.pca <- prcomp(mat.qnorm.centered.flat, center = FALSE, scale. = FALSE)
# mat.pca <- prcomp(mat.qnorm.centered, center = FALSE, scale. = FALSE)
# mat.pca <- prcomp(mat.qnorm.centered.sleep, center = FALSE, scale. = FALSE)
# mat.pca <- prcomp(mat.qnorm.centered.circ, center = FALSE, scale. = FALSE)
# mat.pca <- prcomp(mat.qnorm.centered.nonflat, center = FALSE, scale. = FALSE)
# 
# pc1 <- 1
# pc2 <- 2
# 
# labs <- gsub("ZT", "", colnames(mat))
# phases <- as.numeric(sapply(labs, function(l) strsplit(l, "_")[[1]][[1]]))
# phases <- sapply(phases, function(p){
#   if (p > 72) return(p - 72)
#   if (p > 48) return(p - 48)
#   if (p > 24) return(p - 24)
#   return(p)
# })
# phases.rad <- phases * 2 * pi / 24
# cols <- hsv(PhaseToHsv(phases.rad, min.phase = 0, max.phase = 2 * pi), s = 1, v = 1)
# conds <- sapply(labs, function(l) strsplit(l, "_")[[1]][[2]])
# pchs <- sapply(conds, function(cond){
#   if (cond == "NSD") return(".")
#   if (cond == "SD") return("*")
# })
# 
# pc1 <- 1
# pc2 <- 2
# pc.var <- mat.pca$sdev ^ 2 / sum(mat.pca$sdev ^ 2)
# pc1.var <- round(pc.var[pc1] * 100)
# pc2.var <- round(pc.var[pc2] * 100)
# 
# wordcloud::textplot(mat.pca$rotation[, pc1], mat.pca$rotation[, pc2], words = colnames(mat), 
#                     col = cols, cex = 1.5, 
#                     xlim = c(-0.6, 0.6), ylim = c(-0.6, 0.6),
#                     pch = pchs, xlab = paste0("PC", pc1, " (", pc1.var, "%)"), 
#                     ylab = paste0("PC", pc2, " (", pc2.var, "%)"), cex.axis = 1.5, cex.lab = 1.5)
```

## Egr2 mRNA shows dynamic change (fold change of ~4) but ATAC-Seq at promoter shows minimal change

```{r rnaseq_atacseq1}
jgene <- "Egr2"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5, zt24tozt0 = TRUE)  +  ylab("log2 ATACSeq counts") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)), m.atac + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)), cols = 1)
```

## Some distal regions have significant differences between ZT3 vs ZT27 or ZT6 vs ZT30

```{r distal}
do.log2 <- TRUE
# jstr <- "chr2 136938273 136938773 Mkks 47137"
jstr <- "chr12 4201974 4202474 2410017P09Rik 31840"
jgene <- "Adcy3"

jtitle <- paste(strsplit(jstr, "\\s+")[[1]], collapse = " ")
# jgene <- ExtractChromoStartEndGene(jstr)$gene
exprs.sub <- subset(dat.long.shift, gene == jgene)
if (nrow(exprs.sub) > 0){
  m.gene <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
    # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
    AddDarkPhases(alpha = 0.1) +
    ggtitle(paste("RNASeq:", jgene))
} else {
  m.gene <- NULL
}
m.atac <- PlotPoints.atacseq(explore.sub, jstr, exprs.cname = "counts.norm", convert.log2 = !do.log2, zt24tozt0=TRUE)  + 
  # AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) +
  AddDarkPhases(alpha = 0.1) +
  ggtitle("chr12:4201974-4202474 >50kb away")
multiplot(m.gene + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)), 
          m.atac + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)), 
          cols = 1)

```

# TF motifs underlying gene exprs

## MARA

- Identify motifs underlying different temporal patterns of gene expression

- Model log2 gene expression by the motifs at the promoters to estimate "TF motif activity"


## SRF motif activity increases during sleep deprivation

```{r sleepreg}
act.long <- sleep.mara$act.long
jgene <- "SRF.p3"
m <- PlotPoints(subset(act.long, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3, zt24tozt0 = TRUE)  +  ylab("TF motif activity") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("SRF motif")
print(m + ylab("TF motif activity [A.U.]") + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)))
```

## SRF mRNA accumulation is also sleep-wake driven

```{r sleepregmrna}
# plot gene if possible
jgene.tf <- GetGenesFromMotifs(jgene, tfs)
PlotFit(c(jgene.tf), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Potential targets of SRF: Egr2 and Fos

- See on browser

## SMAD motif activity decreases during sleep deprivation

```{r sleepreg2}
act.long <- sleep.mara$act.long
jgene <- "SMAD1..7.9.p2"
m <- PlotPoints(subset(act.long, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3, zt24tozt0 = TRUE)  +  ylab("TF motif activity") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("SMAD motif")
print(m + ylab("TF motif activity [A.U.]") + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)))
```

## ATF2 (CREB2) motif is associated with circadian model 

```{r circreg}
act.long <- sleep.mara$act.long
jgene <- "ATF2.p2"
m <- PlotPoints(subset(act.long, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3, zt24tozt0 = TRUE)  +  ylab("TF motif activity") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("Atf2 motif")
print(m + ylab("TF motif activity [A.U.]") + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)))
```

## HSF motif is associated with circadian model

```{r circreg2}
act.long <- sleep.mara$act.long
jgene <- "HSF1.2.p2"
m <- PlotPoints(subset(act.long, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3, zt24tozt0 = TRUE)  +  ylab("TF motif activity") +
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("HSF motif")
print(m + ylab("TF motif activity [A.U.]") + theme(axis.text=element_text(size=20), axis.title=element_text(size=20)))
```

## Hsf1 mRNA abundance appears sleep-wake driven 
```{r hsf}
jgene.tf <- "Hsf1"
PlotFit(c(jgene.tf), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Hsf2 mRNA abundance is flat over time
```{r hsf2}
jgene.tf <- "Hsf2"
PlotFit(c(jgene.tf), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time, color.sd.time = FALSE)
```

## Conclusions

- Integrating RNA-Seq and EEG identifies sleep-wake, circadian, and sleep+circadian -driven genes. 

- ATAC-Seq signal does not show large temporal dynamics, but could be used to locate potential regulatory regions. 

- Promoter analysis identifies regulators of immediate early genes, such as SRF, associated with sleep-wake-driven genes
