---
title: 'Transcriptional responses to sleep deprivation'
author: "Jake Yeung"
date: "2016-09-01"
output: 
  beamer_presentation:
    template: /home/yeung/.pandoc/template/rmd-template/default.tex
    toc: false
    theme: "Madrid"
    colortheme: "default"
    fonttheme: "default"
    incremental: false
    slide_level: 2
---

# Introduction
```{r init, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_chunk$set(fig.width=18, fig.height=10,
                      echo=FALSE, warning=FALSE, message=FALSE)
setwd("~/projects/sleep_deprivation")
library(dplyr)
library(ggplot2)

# init
top.n <- 500
downsamp <- 0.005  # downsample fraction 

library(dplyr)
library(hash)
library(reshape2)
library(preprocessCore)

library(PhaseHSV)
library(wordcloud)

source("scripts/functions/FitFunctions.R")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/EegFunctions.R")
source("scripts/functions/RnaSeqFunctions.R")
source("scripts/functions/AtacSeqFunctions.R")
source("scripts/functions/ModelSelectionFunctions.R")
source("scripts/functions/DatabaseFunctions.R")


# Functions ---------------------------------------------------------------


# LOAD  -------------------------------------------------------------------


# fit on replicates
## GENE EXPRS
zt0tozt24 <- FALSE


load("Robjs/dat.long.htseq.0to78hrs.reps.Robj", v=T)
dat.long.shift <- dat.long.shift[order(dat.long.shift$time), ]
dat.long.shift$exprs <- dat.long.shift$log2exprs; dat.long.shift$log2exprs <- NULL
dat.long.shift <- subset(dat.long.shift, time != 78)
if (zt0tozt24){
  # change ZT0 to ZT24
  dat.long.shift$time <- sapply(dat.long.shift$time, function(tt){
    if (tt == 0){
      return(24)
    } else {
      return(tt)
    }
  })
}

# dat.orig <- WrangleDatForModelSelection(dat.long.shift, zt0tozt24 = TRUE, removezt78 = FALSE)
## WAKE
load("Robjs/dat.eeg.smooth.min_awake_5_min_intervals.Robj", verbose = T)



dat.eeg$time.shift <- dat.eeg$time - 24
jxlim <- c(-24, 78)
dat.eeg <- subset(dat.eeg, time.shift >= jxlim[1] & time.shift <= jxlim[2])
# add 72 as -24
row.to.add <- subset(dat.eeg, time.shift == -24); row.to.add$time.shift <- 72
dat.eeg <- rbind(dat.eeg, row.to.add)

wake.df <- subset(dat.eeg, time.shift >= 0 & time.shift <= 72)
# add 72 hours as 71.99889
wake.df.dup <- wake.df[wake.df$time.shift == max(wake.df$time.shift), ]
wake.df.dup$time.shift <- 72
wake.df <- rbind(wake.df, wake.df.dup)

tstep <- 4/3600  # 4seconds in units of hour

# filt.time <- unique(dat.long.shift$time)
filt.time <- seq(1, 72)
wake.collapsed <- CollapseWake(wake.df, tstep, filter.time = filt.time)


# downsample for plotting
dat.eeg.plot <- dat.eeg[seq(1, nrow(dat.eeg), length.out = downsamp * nrow(dat.eeg)), ]

# Take top hits -----------------------------------------------------------

# load("Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.Robj", v=T)
load("Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.pseudo0.Robj" ,v=T)
load("Robjs/fits.sleep.circadian.flat.mix.bic.pseudo.0.halflife.0.33h.to.24h.noIntnoWeight.pseudo0.Robj", v=T)
if (is.null(fits$bic.sleep)){
  fits$bic.sleep <- sapply(fits$fit.sleep, function(s) s[["bic"]])
  fits$bic.circadian <- sapply(fits$fit.circadian, function(s) s[["bic"]])
  fits$bic.flat <- sapply(fits$fit.flat, function(s) s[["bic"]])
  fits$bic.mix <- sapply(fits$fit.mix, function(s) s[["bic"]])
  # fits$bic.delta <- mapply(function(sl, cir) sl - cir, fits$bic.sleep, fits$bic.circadian)
  # fits$bic.mean <- mapply(function(x, y) mean(c(x, y)), fits$bic.sleep, fits$bic.circadian)
}
cnames <- c("sleep", "circadian", "flat", "mixed")
s.i <- which(colnames(fits) == "bic.sleep")
c.i <- which(colnames(fits) == "bic.circadian")
f.i <- which(colnames(fits) == "bic.flat")
m.i <- which(colnames(fits) == "bic.mix")
fits$model <- apply(fits, 1, function(row){
  bic.s <- as.numeric(row[[s.i]])
  bic.c <- as.numeric(row[[c.i]])
  bic.f <- as.numeric(row[[f.i]])
  bic.m <- as.numeric(row[[m.i]])
  bic.vec <- c(bic.s, bic.c, bic.f, bic.m)
  return(cnames[[which.min(bic.vec)]])
})


# Get exponentiated BIC ---------------------------------------------------

fits.bic <- melt(data = subset(fits, select = c(-fit.circadian, -fit.flat)), 
                 id.vars = c("gene", "model", "fit.sleep", "fit.mix"), 
                 measure.vars = c("bic.sleep", "bic.circadian", "bic.flat", "bic.mix"), 
                 variable.name = "bic.model", 
                 value.name = "bic")

fits.bic <- fits.bic %>%
  group_by(gene) %>%
  mutate(weight = exp(-0.5 * bic) / sum(exp(-0.5 * bic)))

# Load fits amp and phase
load("/home/yeung/projects/sleep_deprivation/Robjs/dat.fits.weighted_and_nonweighted.Robj", v=T)


# Load atacseq
load("Robjs/atacseq_promoters.Robj")

atacseq.qnorm <- subset(atacseq.qnorm, time != 78)

# 
# jvar <- "counts"
# inf <- "/home/shared/sql_dbs/atacseq_signal_windows500.long.motifindexed.sqlite3"
# atacseq.tbl <- LoadDatabase(inf)
# explore.query <- filter(atacseq.tbl, abs(dist) < 1000)  # take all genes near promoters and then normalize !
# 
# explore <- collect(explore.query, n = Inf) %>%
#   group_by(gene, sample) %>%
#   summarise(counts = sum(counts)) %>%
#   group_by(gene, sample) %>%
#   mutate(time = SampToTime(sample),
#          trtmnt = SampToTrtmnt(sample),
#          repl = SampToRep(sample))
# 
# genes <- as.character(subset(fits)$gene)
# mat <- dcast(subset(explore, gene %in% genes), gene ~ sample, value.var = jvar)
# mat <- log2(mat + 1)
# mat.qnorm <- normalize.quantiles(as.matrix(mat))
# rownames(mat.qnorm) <- rownames(mat)
# colnames(mat.qnorm) <- colnames(mat)
# atacseq.qnorm <- melt(mat.qnorm, varnames = c("gene", "sample"), value.name = "exprs")
# atacseq.qnorm$time <- sapply(as.character(atacseq.qnorm$sample), SampToTime)
# atacseq.qnorm$samp <- sapply(as.character(atacseq.qnorm$sample), SampToRep)
```

# Results: mRNA profiles of sleep, circadian, and mixed modules

## Mixed model explains mRNA accumulation profile of clock and clock-controlled genes

```{r Arntl}
g <- "Arntl"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

## Npas2

```{r Npas2}
g <- "Npas2"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

## Nfil3

```{r Nfil3}
g <- "Nfil3"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

## Fos

```{r Fos}
g <- "Fos"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

## Genes in mixed model concentrate around ZT22 and ZT10

```{r genomewide-mixed}
jgenes <- as.character(subset(fits, model == "mixed")$gene)
dat.fits.sub <- subset(dat.fits, gene %in% jgenes)
PlotAmpPhase(dat.fits.sub, constant.amp = 7, gene.label = c("Arntl", "Nfil3", "Npas2"))
```

## Genes in sleep model also concentrate around ZT22 and ZT10

```{r genomewide-sleep}
jgenes <- as.character(subset(fits, model == "sleep")$gene)
dat.fits.sub <- subset(dat.fits, gene %in% jgenes)
PlotAmpPhase(dat.fits.sub, constant.amp = 7, gene.label = c("Arntl", "Nfil3", "Npas2"))
```

## Sleep example: Egr2

```{r Egr2}
g <- "Egr2"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

### Genes in circadian model also concentrate around ZT24 and ZT12

```{r genomewide-circadian}
jgenes <- as.character(subset(fits, model == "circadian")$gene)
dat.fits.sub <- subset(dat.fits, gene %in% jgenes)
PlotAmpPhase(dat.fits.sub, constant.amp = 7, gene.label = c("Arntl", "Nfil3", "Npas2"))
```

## Circadian example: Fmo2

```{r Fmo2}
g <- "Fmo2"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))

g <- "Slc25a35"
print(PlotFit(c(g), dat.long.shift, fits, wake.collapsed, dat.eeg.plot, time.vec = filt.time))
```

# ATAC-Seq (promoter analysis)

## ATACseq at promoters of genes do not differ substantially between samples

```{r atacseq_pca}
# mat.qnorm.centered <- mat.qnorm.centered[apply(mat.qnorm.centered, 1, var, na.rm=TRUE) != 0,]
mat.qnorm.centered <- t(scale(t(mat.qnorm), center = TRUE, scale = TRUE))
# remove bad genes
bad.genes <- unique(rownames(which(is.na(mat.qnorm.centered), arr.ind = TRUE)))
mat.qnorm.centered <- mat.qnorm.centered[! rownames(mat.qnorm.centered) %in% bad.genes, ]
mat.pca <- prcomp(mat.qnorm.centered, center = FALSE, scale. = FALSE)

pc1 <- 1
pc2 <- 2

labs <- gsub("ZT", "", colnames(mat))
phases <- as.numeric(sapply(labs, function(l) strsplit(l, "_")[[1]][[1]]))
phases <- sapply(phases, function(p){
  if (p > 72) return(p - 72)
  if (p > 48) return(p - 48)
  if (p > 24) return(p - 24)
  return(p)
})
phases.rad <- phases * 2 * pi / 24
cols <- hsv(PhaseToHsv(phases.rad, min.phase = 0, max.phase = 2 * pi), s = 1, v = 1)
conds <- sapply(labs, function(l) strsplit(l, "_")[[1]][[2]])
pchs <- sapply(conds, function(cond){
  if (cond == "NSD") return(".")
  if (cond == "SD") return("*")
})

pc1 <- 1
pc2 <- 2
pc.var <- mat.pca$sdev ^ 2 / sum(mat.pca$sdev ^ 2)
pc1.var <- round(pc.var[pc1] * 100)
pc2.var <- round(pc.var[pc2] * 100)

wordcloud::textplot(mat.pca$rotation[, pc1], mat.pca$rotation[, pc2], words = colnames(mat), 
                    col = cols, cex = 3, 
                    pch = pchs, xlab = paste0("PC", pc1, " (", pc1.var, "%)"), 
                    ylab = paste0("PC", pc2, " (", pc2.var, "%)"), cex.axis = 1.5, cex.lab = 1.5)
```

## Egr2 mRNA accumulation correlates (slightly) with ATAC-Seq profile at promoter

```{r Egr2-atacseq}
jgene <- "Egr2"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```

## Dbp mRNA accumulation is delayed compared to chromatin accessibility at promoter

```{r Dbp-atacseq}
jgene <- "Dbp"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```

## Chromatin accessibility at Arntl promoter does not change over time, but mRNA does

```{r Arntl-atacseq}
jgene <- "Arntl"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```

## Which promoters have largest difference in chromatin accessibility between ZT3 and ZT27?

```{r top-diff}
jgene <- "Rassf3"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)

```

## Ehd1

```{r Ehd1}
jgene <- "Ehd1"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```

## Kdm3a

```{r Kdm3a}
jgene <- "Kdm3a"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```

## Il15ra

```{r Il15ra}
jgene <- "Il15ra"
m <- PlotPoints(subset(dat.long.shift, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 mRNA accumulation") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle(jgene)
m.atac <- PlotPoints(subset(atacseq.qnorm, gene == jgene), expand.zero = TRUE, colour.pts = TRUE, dotsize = 3.5)  +  ylab("log2 atacseq counts") +
  AddDarkPhases(dark.start = 24, dark.end = 30, alpha = 0.2) + 
  AddDarkPhases(alpha = 0.1) + 
  ggtitle("")
multiplot(m, m.atac, cols = 1)
```