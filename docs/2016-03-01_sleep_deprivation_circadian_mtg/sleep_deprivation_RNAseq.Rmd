---
title: 'Temporal changes in mRNA levels in cortex of sleep deprived mice'
author: "Jake Yeung"
date: "2016-03-31"
output: 
  beamer_presentation:
    template: /home/yeung/.pandoc/template/rmd-template/default.tex
    toc: false
    theme: "Madrid"
    colortheme: "default"
    fonttheme: "default"
    incremental: false
    slide_level: 2
---

# Introduction
```{r init, echo=FALSE, warning=FALSE, message=FALSE}
knitr::opts_chunk$set(fig.width=18, fig.height=10,
                      echo=FALSE, warning=FALSE, message=FALSE)
setwd("~/projects/sleep_deprivation")
source("scripts/functions/PlotFunctions.R")
source("scripts/functions/HandleRawDataFunctions.R")
source("scripts/functions/StringFunctions.R")
library(dplyr)
library(PhaseHSV)
library(stringr)
library(hash)
library(reshape2)

inf <- "/home/yeung/data/sleep_deprivation/htseq_maxime/allCountsNormalized.txt"
dat <- read.table(inf)
meta.data <- ReadProcessMetadata()

dat.long.samp <- DatToLong(dat, meta.data, contains.gene.cname = FALSE)
dat.long.samp$linear.exprs <- dat.long.samp$exprs
pseudo <- 0.01
dat.long.samp$exprs <- log2(dat.long.samp$linear.exprs + pseudo)

load("Robjs/dat.long.htseq.redo.Robj")
load("Robjs/dat.dist.htseq.Robj")
load("Robjs/dat.fits.weighted_and_nonweighted.Robj")
load("Robjs/dat.ttest.htseq.Robj", v=T)
```

## Experimental design

- Mice were sleep deprived (SD) from ZT0 to ZT3 (light onset). 
- SD: 3 cortex samples were collected at ZT3, 6, 12, 18, 24, 30, 48, 54.
- RNA-Seq was performed and compared with control (NSD).
- NSD (control): 3 cortex samples collected at ZT3, 6, 12, 18. 
     - 4 Samples collected at ZT0.

# Results

## PCA shows SD alters transcriptome at ZT3 and ZT6, and slowly returns to normal conditions.

```{r pca}
dat.nsd <- subset(dat.long.samp)

dat.nsd.mat <- dcast(dat.nsd, gene ~ time + samp + trtmnt, value.var = "exprs")

rownames(dat.nsd.mat) <- dat.nsd.mat$gene; dat.nsd.mat$gene <- NULL
dat.nsd.mat <- as.matrix(dat.nsd.mat)

# center rows
# dat.nsd.mat.cent <- sweep(dat.nsd.mat, MARGIN = 1, STATS = rowMeans(dat.nsd.mat), FUN = "-")
dat.nsd.mat.cent <- t(scale(t(dat.nsd.mat), center = TRUE, scale = FALSE))

# do PCA
dat.pca <- prcomp(dat.nsd.mat.cent, center = TRUE, scale. = FALSE)


labs <- colnames(dat.nsd.mat.cent)
phases <- as.numeric(sapply(labs, function(l) strsplit(l, "_")[[1]][[1]]))
phases <- sapply(phases, function(p){
  if (p > 48) return(p - 48)
  if (p > 24) return(p - 24)
  return(p)
})
phases.rad <- phases * 2 * pi / 24
cols <- hsv(PhaseToHsv(phases.rad, min.phase = 0, max.phase = 2 * pi), s = 1, v = 1)
conds <- sapply(labs, function(l) strsplit(l, "_")[[1]][[3]])
pchs <- sapply(conds, function(cond){
  if (cond == "NSD") return(".")
  if (cond == "SD") return("*")
})

pc1 <- 1
pc2 <- 2
plot(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], xlab = paste0("PC", pc1), ylab = paste0("PC", pc2), pch = pchs, cex = 2)
text(dat.pca$rotation[, pc1], dat.pca$rotation[, pc2], labels = colnames(dat.nsd.mat.cent), col = cols, cex = 1.5)
```

## Screeplot
```{r screeplot}
screeplot(dat.pca)
```

## Rhythmic genes (all low amplitude) are perturbed by 3 hours of sleep deprivation

```{r clockgenes}
m1 <- PlotGeneAcrossTime(subset(dat.long, gene == "Arntl"), show.zero = FALSE, jsize = 4)
m2 <- PlotGeneAcrossTime(subset(dat.long, gene == "Dbp"), show.zero = FALSE, jsize = 4)
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == "Per3"), show.zero = FALSE, jsize = 4)
m4 <- ggplot(dat.long, aes(x = exprs)) + geom_histogram(bins = 500) + theme_bw(24)
multiplot(m1, m2, m3, m4, cols = 2)
```

## Fold changes between NSD and SD are small (less than 2 fold change)

```{r ttest_global}

# take max FC
dat.ttest.max <- dat.ttest %>%
  group_by(gene) %>%
  filter(abs(nsd.minus.sd) == max(abs(nsd.minus.sd)))

m1 <- ggplot(dat.ttest, aes(x = nsd.minus.sd)) + geom_histogram(bins = 500) + theme_bw(24)
m2 <- PlotGeneAcrossTime(subset(dat.long, gene == "Egr2"), show.zero = TRUE, jsize = 4) + ylim(c(0, 7.5))
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == "Slco1b2", show.zero = TRUE), jsize = 4) + ylim(c(0, 7.5))
m4 <- PlotGeneAcrossTime(subset(dat.long, gene == "Plekhg4", show.zero = TRUE), jsize = 4) + ylim(c(0, 7.5))
multiplot(m1, m2, m3, m4, cols = 2)
```

## Volcano plot (pval from ttest) shows few statistically significant genes

```{r ttest_volcano}
m1 <- ggplot(dat.ttest.max, aes(x = nsd.minus.sd, y = -log10(pval))) + geom_point(alpha = 0.25) + theme_bw(24)
m2 <- ggplot(dat.ttest.max, aes(x = nsd.minus.sd, y = -log10(pval.adj))) + geom_point(alpha = 0.25) + theme_bw(24)
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == "Crh"), show.zero = TRUE, jsize = 4) + ylim(c(0, 7.5))
m4 <- PlotGeneAcrossTime(subset(dat.long, gene == "Pth2r", show.zero = TRUE), jsize = 4) + ylim(c(0, 7.5))
multiplot(m1, m2, m3, m4, cols = 2)
```

## What are the general patterns of NSD vs SD?

```{r clustering}
jlnorm <- 1
pval.cutoff <- 0.25
effect.size.min <- 0.5
signif.genes <- unique(as.character(subset(dat.ttest, pval.adj < pval.cutoff & abs(nsd.minus.sd) > effect.size.min)$gene))

jgene <- "Fos"
m1 <- PlotGeneAcrossTime(subset(dat.long, gene == jgene))
m2 <- PlotByDistance(subset(dat.long, gene == jgene), lnorm = 1) + geom_hline(aes(yintercept = 0))
jgene <- "1700001L05Rik"
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == jgene))
m4 <- PlotByDistance(subset(dat.long, gene == jgene), lnorm = 1) + geom_hline(aes(yintercept = 0))
multiplot(m1, m2, m3, m4, cols = 2)
```

## Clustering analysis on "significant" genes identifies early and late responding genes
```{r clustering-earlylate}
n.centers <- 5
jscale <- TRUE
jlnorm <- 1
pval.cutoff <- 0.25
effect.size.min <- 0.25
signif.genes <- unique(as.character(subset(dat.ttest, pval.adj < pval.cutoff & abs(nsd.minus.sd) > effect.size.min)$gene))

# filter by significance
dat.dist.filt <- subset(dat.dist, gene %in% signif.genes)

mat.dist <- dcast(dat.dist.filt, gene ~ time, value.var = "exprs.dist")
rownames(mat.dist) <- mat.dist$gene; mat.dist$gene <- NULL


# Normalize (optional) ----------------------------------------------------

do.normalize <- TRUE
if (do.normalize){
  mat.dist <- t(scale(t(mat.dist), scale = TRUE, center = FALSE))
}
mat.long <- melt(mat.dist, varnames = c("gene", "time"), value.name = "exprs.dist")

# Filter genes with small expression differences --------------------------
cluster.dist <- kmeans(mat.dist, centers = n.centers)

clusters.count <- table(cluster.dist$cluster)
# sort by minimum withinss
clusters.count <- clusters.count[order(cluster.dist$withinss / cluster.dist$size)]

plots.lst <- list()
i <- 1
for (clust in names(clusters.count)){  
  genes.clust <- names(cluster.dist$cluster)[which(cluster.dist$cluster == clust)]
  genes <- names(cluster.dist$cluster)[which(cluster.dist$cluster == clust)]
  m <- ggplot(subset(mat.long, gene %in% genes.clust), aes(x = time, y = exprs.dist, group = gene)) +
          xlab("Time (ZT)") + 
          ylab("Zscore (SD - NSD)") + 
          geom_line(alpha = 0.2) + ggtitle(paste("N genes:", length(genes))) + ylim(c(-2.5, 2.5)) +
          theme_bw(24)
  plots.lst[[i]] <- m
  i <- i + 1
}
multiplot(plots.lst[[1]], plots.lst[[2]], plots.lst[[3]], plots.lst[[4]], cols = 2)
```

## Examples of early-responding genes 
```{r early-phase}
m2 <- PlotGeneAcrossTime(subset(dat.long, gene == "Plekhg4"))
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == "Mybpc3"))
multiplot(plots.lst[[1]], m2, plots.lst[[2]], m3, cols = 2)
```

## Examples of late-responding genes
```{r late-phase}
m2 <- PlotGeneAcrossTime(subset(dat.long, gene == "Car12"))
m3 <- PlotGeneAcrossTime(subset(dat.long, gene == "Mttp"))
multiplot(plots.lst[[3]], m2, plots.lst[[4]], m3, cols = 2)
```

## Direction of response depends on phase of gene in NSD (early)
```{r phases-early}
scatters <- list()
for (i in seq(4)){
  cluster.i <- names(clusters.count)[[i]]
  genes <- names(cluster.dist$cluster)[which(cluster.dist$cluster == cluster.i)]
  scatters[[i]] <- PlotAmpPhase(subset(dat.fits, gene %in% genes))
}
multiplot(scatters[[1]], plots.lst[[1]], scatters[[2]], plots.lst[[2]], cols = 2)
```

## Direction of response depends on phase of gene in NSD (late)
```{r phases-late}
multiplot(scatters[[3]], plots.lst[[3]], scatters[[4]], plots.lst[[4]], cols = 2)
```

## SD vs NSD fold change depends on amplitude of gene in NSD
```{r fc-amp}
dat.ttest.max <- dat.ttest %>%
  group_by(gene) %>%
  summarise(ttest.fc.max = nsd.minus.sd[which.max(abs(nsd.minus.sd))],
            ttest.pval.min = min(pval),
            ttest.pval.adj.min = min(pval.adj))
fc.hash <- hash(as.character(dat.ttest.max$gene), dat.ttest.max$ttest.fc.max)

dat.fits$ttest.fc.max <- sapply(as.character(dat.fits$gene), function(g){
  fc <- fc.hash[[g]]
  if (is.null(fc)){
    return(NA)
  } else {
    return(fc)
  }
})
dat.fits$lab <- apply(dat.fits, 1, function(jrow){
  jamp <- as.numeric(jrow[5][[1]])
  jfc <- abs(as.numeric(jrow[10][[1]]))
  jgene <- as.character(jrow[1][[1]])
  if (is.na(jfc) | is.na(jamp)) return("")
  if (jamp > 2 | jfc > 2){
    return(jgene)  
  } else {
    return("")
  }
})

ggplot(dat.fits, aes(x = amp, y = abs(ttest.fc.max), label = lab)) + geom_point(alpha = 0.2) + theme_bw(24) + 
  xlab("NSD 24h amplitude (log2 peak to trough)") + ylab("Abs max log2 fold change between NSD vs SD") + geom_text()

```

# Conclusions

## Conclusions

- Largest changes in mRNA abundance (SD vs NSD) occur within 12 hours of SD (mostly ZT3 and ZT6)
- Four patterns of gene expression changes: early and late (upregulated and downregulated).
- Response to SD in gene expression depends on amplitude and phase of the gene in NSD.

## Further questions

- Regulators underlying upregulation and downregulation of genes due to SD?
- How do we decouple circadian and sleep-homeostatic processes?
